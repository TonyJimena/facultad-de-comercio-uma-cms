<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\ImageManagerStatic as Image;

use App\Helpers\FilesTypes;

use App\Models\FileUserData;

class OptimizeImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:optimize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Optimize user data images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $files = FileUserData::where('mime_type', 'LIKE', '%image/%')->get();

        foreach ($files as $file) {
            $type = $file->file_type_id;
            $path = $file->path;
            $name = $file->name;

            switch ($type) {
                case FilesTypes::getAvatarId():
                    $with = FilesTypes::getAvatarWidth();
                    break;
                case FilesTypes::getLogoId():
                    $with = FilesTypes::getLogoWidth();
                    break;
                default:
                    $with = FilesTypes::getCarouselImageWidth();
                    break;
            }

            $img = Image::make($file->content);

            $currentWith = $img->width();

            if ($currentWith > $with) {
                $img->resize($with, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $content = str_replace('data:image/jpeg;base64,', '', (string)$img->encode('data-url', 70));

                $decodedContent = base64_decode($content);
                if (Storage::put("$path$name", $decodedContent)) {
                    $file->content = $content;
                    $file->save();
                }
            }
        }
    }
}
