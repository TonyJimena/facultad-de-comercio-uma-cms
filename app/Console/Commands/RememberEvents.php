<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;

use App\Models\Event;

class RememberEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'events:remember';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification to remember that event is tomorro';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tomorrow = Carbon::tomorrow();
        $startDate = $tomorrow->copy()->startOfDay();
        $endDate = $tomorrow->copy()->endOfDay();

        $events = Event::whereBetween('date', [$startDate, $endDate])->get();

        foreach ($events as $event) {
            $event->notifyIsTomorrow();
        }
    }
}
