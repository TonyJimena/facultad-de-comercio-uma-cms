<?php

namespace App\Exports;

use App\Models\BaseModel;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BaseExport implements FromArray, WithHeadings
{
    protected $model;
    protected $module;
	protected $data;

    public function __construct(BaseModel $model, string $module, array $data)
    {
        $this->model = $model;
        $this->module = $module;
        $this->data = $data;
    }

    public function headings(): array
    {
        $heading = [];

        $excelFields = array_values(
            array_unique(
                array_merge($this->model->gridColumns, array_keys($this->model->relationsGridColumns))
            )
        );
        
        foreach ($excelFields as $key => $column) {
            if (!in_array($column, $this->model->notExcel?: [])) {
                $heading[] = trans($this->module . '.' . $column);
            }
        }

        return $heading;
    }

	public function array(): array
    {
        $data = [];

        foreach($this->data as $key => $value) {
            $data[] = array_values($value);
        }

        return $data;
    }
}
