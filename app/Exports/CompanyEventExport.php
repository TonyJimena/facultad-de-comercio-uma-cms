<?php

namespace App\Exports;

use App\Models\CompanyEvent;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompanyEventExport extends BaseExport
{
    public function __construct(CompanyEvent $model, string $module, array $data)
    {
        parent::__construct($model, $module, $data);
    }
}
