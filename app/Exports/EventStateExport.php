<?php

namespace App\Exports;

use App\Models\EventState;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EventStateExport extends BaseExport
{
    public function __construct(EventState $model, string $module, array $data)
    {
        parent::__construct($model, $module, $data);
    }
}
