<?php

namespace App\Exports;

use App\Models\Inscription;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InscriptionExport extends BaseExport
{
    public function __construct(Inscription $model, string $module, array $data)
    {
        parent::__construct($model, $module, $data);
    }
}
