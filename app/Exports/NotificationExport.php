<?php

namespace App\Exports;

use App\Models\Notification;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class NotificationExport extends BaseExport
{
    public function __construct(Notification $model, string $module, array $data)
    {
        parent::__construct($model, $module, $data);
    }
}
