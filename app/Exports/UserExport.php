<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport extends BaseExport
{
    public function __construct(User $model, string $module, array $data)
    {
        parent::__construct($model, $module, $data);
    }
}
