<?php

namespace App\Helpers;

class EventsStatus {

    const available = 1;
    const completed = 2;
    const canceled = 3;
    const finalized = 4;

    public static function getAvailableId(){
        return self::available;
    }

    public static function getCompletedId()
    {
        return self::completed;
    }

    public static function getCanceledId()
    {
        return self::canceled;
    }

    public static function getFinalizedId()
    {
        return self::finalized;
    }
}
