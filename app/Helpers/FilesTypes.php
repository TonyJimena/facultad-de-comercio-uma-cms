<?php

namespace App\Helpers;

class FilesTypes
{
    const avatar = 1;
    const logo = 2;
    const carouselImage = 3;

    const avatarWidth = 100;
    const logoWidth = 300;
    const carouselImageWidth = 1024;

    public static function getAvatarId()
    {
        return self::avatar;
    }

    public static function getLogoId()
    {
        return self::logo;
    }

    public static function getCarouselImageId()
    {
        return self::carouselImage;
    }
    public static function getAvatarWidth()
    {
        return self::avatarWidth;
    }

    public static function getLogoWidth()
    {
        return self::logoWidth;
    }

    public static function getCarouselImageWidth()
    {
        return self::carouselImageWidth;
    }
}
