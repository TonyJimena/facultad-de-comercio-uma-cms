<?php

namespace App\Helpers;

use DB;
use Stringy\StaticStringy as Stringy;

class Helper {

	public static function isRequired( $item, $field ){
		if(!$item) return false;
		if( array_key_exists( $field, $item::$rules) && strpos( $item::$rules[ $field ], 'required' ) !== false )
	        return true;
	    else
	        return false;
	}

    public static function isRequiredLang( $item, $field ){
        if(!$item) return false;
        $rulesLangs = $item->getRulesLangs();
        
        if( array_key_exists( $field, $rulesLangs) && strpos( $rulesLangs[ $field ], 'required' ) !== false )
            return true;
        else
            return false;
    }

    public static function dbEscapeLike($value, $escapeAll = true)
    {
        $string = preg_replace('/(\\_|\\%)/', '\\\$1', $value);

        if ($escapeAll) {
            $string = DB::getPdo()->quote($string);
            $string = Stringy::substr($string, 1, -1);
        }

        return $string;
    }

    public static function dbEscapeAs($value)
    {
        if(strpos($value, 'AS') !== false){
            $value = substr($value, 0, strpos($value, 'AS'));	
        }

        return trim($value);
    }

    public static function getTax($decimal = true)
    {
        if ($decimal) {
            return 0.21;
        }

        return 21;
    }

    public static function isCorrectDate( $timestamp ){

        $date = \DateTime::createFromFormat( 'd/m/Y', $timestamp );

        if( $date === false )
            return false;
        else
            return true;


    }
}
