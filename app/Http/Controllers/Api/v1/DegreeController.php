<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Degree;

class DegreeController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param Degree $model
     */
    public function __construct(Degree $model)
    {
        parent::__construct($model);
    }
}
