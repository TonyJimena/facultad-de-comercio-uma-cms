<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Event;
use App\Helpers\EventsStatus;

use App\Helpers\FilesTypes;

use App\Notifications\EventWithPlaces;

class EventController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param Event $model
     */
    public function __construct(Event $model)
    {
        parent::__construct($model);
    }
    /**
     * Show a event
     * 
     * @param Request $request
     * @param Int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $event = Event::with([
            'companies.user.userData.files' => function ($query) {
                $query->where('file_type_id', FilesTypes::getAvatarId());
            },
            'companies.sector',
            'state'
        ])->find($id);

        if (!$event) {
            return response()->json([
                'message' => trans('event.not_found')
            ], 404);
        }

        return response()->json($event);
    }
    /**
     * Get event for authenticate user
     * 
     * @param Request $request
     * @return Response
     */
    public function getEventsAuthenticateUser(Request $request)
    {
        $user = $request->user();
        $company = $user->company;

        if (!$company) {
            return response()->json([]);
        }

        $events = $company->events;
        if (!$events) {
            return response()->json([]);
        }

        $eventsIds = $events->map(function ($item) {
            return $item->id;
        });

        $table = $this->model->getTable();
        $objects = DB::table($table);
        $objects = $this->formatData($request, $objects, true);

        $objects->whereIn($table . '.id', $eventsIds);

        $objects = $this->generatePagination($objects);

        return response()->json($objects);
    }
    /**
     * Create a assist for a company to a event
     * 
     * @param Request $request
     * @param Int $id
     * @return Response
     */
    public function assist(Request $request, $id)
    {
        $result = DB::transaction(function () use ($request, $id) {
            $user = $request->user();
            $event = Event::find($id);

            if (!$event) {
                return response()->json([
                    'message' => trans('event.not_found')
                ], 404);
            }
            if ($event->event_state_id == EventsStatus::getCanceledId()) {
                return response()->json([
                    'message' => trans('event.canceled')
                ], 404);
            }
            if ($event->event_state_id == EventsStatus::getFinalizedId()) {
                return response()->json([
                    'message' => trans('event.finalized')
                ], 404);
            }

            $company = $user->company;
            if (!$company) {
                return response()->json([
                    'message' => trans('company.not_found')
                ], 404);
            }

            $existsCompany = $event->companies()->where('company_id', $company->id)->first();
            if ($existsCompany) {
                return response()->json([
                    'message' => trans('event.company_exists')
                ], 422);
            }

            if ($event->event_state_id == EventsStatus::getCompletedId() || $event->companies()->count() >= $event->number_attendees) {
                $event->companies()->attach($company->id, ['in_reserve' => 1]);
            } else {
                $event->companies()->attach($company->id);
                if ($event->number_attendees == $event->companies()->count()) {
                    $this->associateEventState(EventsStatus::getCompletedId(), $event);
                }
            }

            return $this->show($request, $event->id);
        });

        return $result;
    }
    /**
     * Delete a assist for a company to a event
     * 
     * @param Request $request
     * @param Int $id
     * @return Response
     */
    public function leave(Request $request, $id)
    {
        $result = DB::transaction(function () use ($request, $id) {
            $user = $request->user();
            $event = Event::find($id);

            if (!$event) {
                return response()->json([
                    'message' => trans('event.not_found')
                ], 404);
            }

            $company = $user->company;
            if (!$company) {
                return response()->json([
                    'message' => trans('company.not_found')
                ], 404);
            }

            $existsCompany = $event->companies()->where('company_id', $company->id)->first();
            if (!$existsCompany) {
                return response()->json([
                    'message' => trans('event.company_not_exists')
                ], 422);
            }

            $event->companies()->detach($company->id);
            if ($event->companies()->count() < $event->number_attendees) {
                if ($event->event_state_id == EventsStatus::getCompletedId())
                    $this->associateEventState(EventsStatus::getAvailableId(), $event);

                $reserveCount = $event->companies()->wherePivot('in_reserve', 1)->count();
                if ($reserveCount) {
                    $event->notify(new EventWithPlaces($reserveCount));
                }
            }

            return $this->show($request, $event->id);
        });

        return $result;
    }
}
