<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\EventState;

class EventStateController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param EventState $model
     */
    public function __construct(EventState $model)
    {
        parent::__construct($model);
    }
}
