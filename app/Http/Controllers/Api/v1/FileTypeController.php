<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\FileType;

class FileTypeController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param FileType $model
     */
    public function __construct(FileType $model)
    {
        parent::__construct($model);
    }
}
