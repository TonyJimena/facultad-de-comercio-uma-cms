<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use Intervention\Image\ImageManagerStatic as Image;

use App\Models\FileUserData;
use App\Models\User;

use App\Helpers\FilesTypes;

class FileUserDataController extends BaseController
{
    protected $path = 'files_user_data';
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param FileUserData $model
     */
    public function __construct(FileUserData $model)
    {
        parent::__construct($model);
    }
    /**
     * Get user data files
     * 
     * @param Request $request
     * @return Response
     */
    public function getUserDataFiles($userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return response()->json([
                'message' => trans('user.not_found')
            ], 404);
        }

        $files = [];

        if ($user->userData) {
            $files = $user->userData
                ->files()
                ->with([
                    'type'
                ])
                ->orderBy('position', 'ASC')
                ->get();
        }

        return response()->json($files);
    }
    /**
     * Get files authenticated user
     * 
     * @param Request $request
     * @return Response
     */
    public function getUserDataFilesAuthenticatedUser(Request $request)
    {
        $id = $request->user()->id;

        return $this->getUserDataFiles($id);
    }
    /**
     * Create a file
     * 
     * @param Request $request
     * @return Response
     */
    public function storeUserDataFile(Request $request)
    {
        $result = DB::transaction(function () use ($request) {
            $user = $request->user();

            $content = $request->content;
            $extension = $request->extension;
            $mimeType = $request->mime_type;
            $type = $request->file_type;

            if ($mimeType == 'image/jpeg') {
                $content = $this->optimizeImage($content, $type);
            }

            $decodedContent = base64_decode($content);

            $originalName = $request->original_name . '.' . $extension;
            $name = Str::random(70) . '.' . $extension;
            $path = $this->path . '/' . $user->id . '/';

            $data = [
                'path' => $path,
                'original_name' => $originalName,
                'name' => $name,
                'mime_type' => $mimeType,
                'content' => $content
            ];

            if (!Storage::exists($path))
                Storage::makeDirectory($path);

            if (Storage::put("$path$name", $decodedContent)) {
                $file = FileUserData::create($data);
                $this->associateFileType($type, $file);
                $this->associateUserData($user->userData->id, $file);
            } else {
                return response([
                    'message' => trans('file_user_data.no_store')
                ], 403);
            }

            return response()->json($file);
        });

        return $result;
    }
    /**
     * Update a file
     * 
     * @param Request $request
     * @param Int $id
     * @return Response
     */
    public function updateUserDataFile(Request $request, $id)
    {
        $result = DB::transaction(function () use ($request, $id) {
            $user = $request->user();
            $fileUserData = null;

            if (isset($user->userData->files))
                $fileUserData = $user->userData->files()->where('id', $id)->first();

            if (!$fileUserData) {
                return response()->json([
                    'message' => trans('file_user_data.not_found')
                ], 404);
            }

            $content = $request->content;
            $extension = $request->extension;
            $mimeType = $request->mime_type;
            $type = $request->file_type;

            if ($mimeType == 'image/jpeg') {
                $content = $this->optimizeImage($content, $type);
            }

            $decodedContent = base64_decode($content);

            $originalName = $request->original_name . '.' . $extension;
            $name = $fileUserData ? $fileUserData->name : Str::random(70) . '.' . $extension;
            $path = $this->path . '/' . $user->id . '/';

            $data = [
                'path' => $path,
                'original_name' => $originalName,
                'name' => $name,
                'mime_type' => $mimeType,
                'content' => $content
            ];

            if (!Storage::exists($path))
                Storage::makeDirectory($path);

            if (Storage::put("$path$name", $decodedContent)) {
                if ($fileUserData) {
                    $fileUserData->update($data);
                } else {
                    $fileUserData = FileUserData::create($data);
                    $this->associateUserData($user->userData->id, $fileUserData);
                }
                $this->associateFileType($type, $fileUserData);
            } else {
                return response([
                    'message' => trans('file_user_data.no_store')
                ], 403);
            }

            return $this->getUserDataFilesAuthenticatedUser($request);
        });

        return $result;
    }
    /**
     * Delete a file
     * 
     * @param Request $request
     * @param Int $id
     * @return Response
     */
    public function deleteUserDataFile(Request $request, $id)
    {
        $user = $request->user();

        $fileUserData = null;

        if (isset($user->userData->files))
            $fileUserData = $user->userData->files()->where('id', $id)->first();

        if (!$fileUserData) {
            return response()->json([
                'message' => trans('file_user_data.not_found')
            ], 404);
        }

        if (Storage::delete($fileUserData->path . $fileUserData->name)) {
            $fileUserData->delete();
            return $this->getUserDataFilesAuthenticatedUser($request);
        } else {
            return response()->json([
                'message' => trans('file_user_data.no_destroy')
            ], 403);
        }
    }
    /**
     * Return file preview
     * 
     * @param Int $id
     * @return Response
     */
    public function previewUserDataFile($id)
    {
        $file = FileUserData::find($id);

        if (!$file) {
            return response()->json([
                'message' => trans('file_user_data.not_found')
            ], 404);
        }

        $path = storage_path("app/$file->path/$file->name");

        return response()->file($path);
    }
    /**
     * Optimize images
     *
     * @param String $content
     * @param Int $type
     * @return String
     */
    private function optimizeImage($content, $type)
    {
        $img = Image::make($content);

        /*switch ($type) {
            case FilesTypes::getAvatarId():
                $with = FilesTypes::getAvatarWidth();
                break;
            case FilesTypes::getLogoId():
                $with = FilesTypes::getLogoWidth();
                break;
            default:
                $with = FilesTypes::getCarouselImageWidth();
                break;
        }

        $img->resize($with, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });*/

        $content = str_replace('data:image/jpeg;base64,', '', (string)$img->encode('data-url', 75));

        return $content;
    }
}
