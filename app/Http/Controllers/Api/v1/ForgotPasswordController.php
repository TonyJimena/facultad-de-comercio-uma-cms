<?php

namespace App\Http\Controllers\Api\v1;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Carbon\Carbon;

use App\Notifications\ResetPasswordNotification;
use App\Models\User;
use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{   
    /**
     * Send link to reset password to app user
     *
     * @params Requet $request
     * @return Response object
     */
    public function sendResetLinkEmail(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors = [
                'message' => implode(".\n", $errors->all())
            ];
            return response()->json($errors, 422);
        }

        $email = $request->email;
        $user = User::where([
            ['username', $email],
            ['active', 1]
        ])->first();

        if (!$user)
            return response()->json([
                'message' => trans('reset_password.no_user')
            ], 404);
        
        $user->name = isset($user->userData) ? $user->userData->name : '';
        $user->surname = isset($user->userData) ? $user->userData->surname : '';
        
        $passwordReset = DB::table('password_resets')->where('email', $user->username)->first();
        $token = Str::random(64);
        if($passwordReset) {
            DB::table('password_resets')
            ->where('email', $user->username)
            ->update([
                'token' => $token,
                'created_at' => Carbon::now()->toDateTimeString()
            ]);
        }else{
            DB::table('password_resets')->insert(
                [
                    'email' => $user->username,
                    'token' => $token,
                    'created_at' => Carbon::now()->toDateTimeString()
                ]
            );
        }
        
        $user->notify(
            new ResetPasswordNotification($token)
        );
        return response()->json([
            'message' => trans('passwords.sent')
        ]);
    }
}