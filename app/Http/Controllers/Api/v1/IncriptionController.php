<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\Inscription;

class IncriptionController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param Inscription $model
     */
    public function __construct(Inscription $model)
    {
        parent::__construct($model);
    }
    /**
     * Store a inscription
     * 
     * @param $request
     * @return Response
     */
    public function store(Request $request)
    {
        $result = DB::transaction(function () use ($request) {
            $errors = $this->validator($request, $this->model->getRulesApi());

            if ($errors) {
                return response()->json($errors, 422);
            }

            $inscription = Inscription::create($request->all());
            $this->associateSector($request->sector, $inscription);

            return response()->json($inscription);
        });

        return $result;
    }
}
