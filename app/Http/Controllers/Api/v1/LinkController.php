<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Link as LinkModel;
use Link;

class LinkController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param Link $model
     */
    public function __construct(LinkModel $model)
    {
        parent::__construct($model);
    }
}
