<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\News as NewModel;
use News;

class NewController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param News $model
     */
    public function __construct(NewModel $model)
    {
        parent::__construct($model);
    }
}
