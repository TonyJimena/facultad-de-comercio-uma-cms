<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Notice as NoticeModel;
use App\Models\Notice;
use App\Models\User;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;



class NoticeController extends BaseController
{
    /**
     * Creates a Notices instance of the controller with a Notices instance of the model
     *
     * @param Notice $model
     */
    public function __construct(NoticeModel $model)
    {
        parent::__construct($model);
    }

     /**
     * Delete a Notice
     *
     * @param Request $request
     * @return Response
     */
    public function deleteNotice(Request $request, $notice_id)
    {

        $object = $this->model::where('id', $notice_id);

        $object->delete();

        return response()->json("Deleted at:");
    }

    /**
     * Store a item
     *
     * @params Requet $request
     * @return Response object
     */
    public function storeNotice(Request $request)
    {
        $input = $request->only($this->model->fields);
        $user = Auth::user();
        $id = Auth::id();

        $user = User::find($id);

        $input['user_name'] = $user->username;
        $input['user_id'] = $id;

        $notice = Notice::create($input);

        //$this->associateNotice($notice->id, $user);

        return response()->json($notice, 201);
    }
}
