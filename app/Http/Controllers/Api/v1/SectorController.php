<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Sector;

class SectorController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param Sector $model
     */
    public function __construct(Sector $model)
    {
        parent::__construct($model);
    }
}
