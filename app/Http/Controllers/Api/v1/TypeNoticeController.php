<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\TypeNotice;

class TypeNoticeController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param TypeNotice $model
     */
    public function __construct(TypeNotice $model)
    {
        parent::__construct($model);
    }
}
