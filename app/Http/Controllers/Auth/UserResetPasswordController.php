<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Helpers\Application;

use Carbon\Carbon;

use App\Models\User;

class UserResetPasswordController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        Application::setCMS();
    }
    /**
     * Return view for reset password
     * 
     * @param string $token
     */
    public function showResetForm($token) {
        $tokenData = DB::table('password_resets')
        ->where('token', $token)->first();
        
        if($tokenData && !$this->tokenExpired($tokenData)) {
            return view('auth.passwords.reset', [
                'url' => route('user.password.request'),
                'token' => $token
            ]);
        }else{
            return view('auth.passwords.error-reset')->with('message', trans('reset_password.message_error_reset'));
        }
    }
    /**
     * Reset password
     * 
     * @param  Request $request
     */
    public function reset(Request $request) {
        $validatedData = $request->validate([
            'password' => 'required|min:6|regex:/[A-Z]/',
            'password_confirmation' => 'required|in:'.$request->password
        ]);

        $tokenData = DB::table('password_resets')
        ->where('token', $request->token)->first();
        
        if($tokenData && !$this->tokenExpired($tokenData)) {
            $password = $request->password;
            $user = User::where('username', $tokenData->email)->first();
            if ( !$user ) return view('auth.passwords.error-reset')->with('message', trans('reset_password.no_user'));
            $user->tokens()->delete();
            $user->password = $password;
            $user->save();

            return view('auth.passwords.success-reset')->with('message', trans('reset_password.message_success_reset'));
        }else{
            return view('auth.passwords.error-reset')->with('message', trans('reset_password.message_error_reset'));
        }
    }
    /**
     * Check if token reset is expired
     * 
     * @param string $token
     * @return bool
     */
    private function tokenExpired($tokenData) {
        return Carbon::parse($tokenData->created_at)->addSeconds(config('auth.password_timeout'))->isPast();
    }
}
