<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;

use App\Presenters\CompanyEventPresenter;

use App\Models\CompanyEvent;
use App\Models\Event;
use App\Models\EventState;
use App\Models\Company;

use App\Events\EventReserveConfirmed;
use App\Notifications\EventWithPlaces;

use App\Helpers\EventsStatus;

class CompanyEventController extends BaseController
{

     /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
      */
     public function __construct(CompanyEvent $model, CompanyEventPresenter $presenter = null)
     {
          parent::__construct($model, $presenter);

          $this->module = 'company_event';
          $this->urlBack = array(
               'store' => '',
               'update' => ''
          );
     }
     /**
      * Return date to share
      */
     private function setDataToShare()
     {
          $events = Event::select('id', 'title')
               ->where('event_state_id', '=', EventsStatus::getAvailableId())
               ->orderBy('date')
               ->get();
          $eventsAux = [];
          foreach ($events as $event) {
               $eventsAux[$event->id] = $event->title;
          }
          $events = $eventsAux;

          $companies = Company::whereHas('user', function ($query) {
               $query->where('active', 1);
          })->get();
          $companiesAux = [];
          foreach ($companies as $company) {
               $companiesAux[$company->id] = $company->name;
          }
          $companies = $companiesAux;

          View::share([
               'events' => $events,
               'companies' => $companies
          ]);
     }
     /**
      * Show the form for create the specified resource.
      *
      * @param int $id
      * @return Response
      */
     public function create()
     {
          $this->setDataToShare();

          return parent::create();
     }
     /**
      * Show the form for editing the specified resource.
      *
      * @param int $id
      * @return Response
      */
     public function edit($id = NULL)
     {
          $this->setDataToShare();

          return parent::edit($id);
     }
     /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
     public function store($id = NULL)
     {
          $result = DB::transaction(function () use ($id) {
               if ($id != null) {
                    if (!$this->model->options['can_edit']) return redirect()->route($this->module . '.index');
                    $companyEvent = CompanyEvent::find($id);
               } else {
                    if (!$this->model->options['can_create']) return redirect()->route($this->module . '.index');
                    $companyEvent = new CompanyEvent();
               }

               $rules = array_merge($this->model->getRules());

               $success = false;
               $success = $this->model->validate(array_filter(request()->all()), $rules);

               if (!$success) {
                    return $this->responseInvalid(__FUNCTION__);
               }

               $company = Company::find(request()->company_id);
               if (!$company) {
                    return $this->responseInvalid(__FUNCTION__);
               }

               $user = $company->user;
               if (!$user || !$user->active) {
                    return $this->responseInvalid(__FUNCTION__);
               }

               $event = Event::find(request()->event_id);
               if (!$event) {
                    return $this->responseInvalid(__FUNCTION__);
               }
               if ($event->event_state_id == EventsStatus::getCanceledId()) {
                    return $this->responseInvalid(__FUNCTION__, ['event_id' => trans('event.canceled')]);
               }
               if ($event->event_state_id == EventsStatus::getFinalizedId()) {
                    return $this->responseInvalid(__FUNCTION__, ['event_id' => trans('event.finalized')]);
               }

               $existsCompany = $event->companies()
                    ->where('company_id', $company->id)
                    ->first();
               if ($existsCompany) {
                    return $this->responseInvalid(__FUNCTION__, ['company_id' => 'Esta empresa ya está apuntada para asistir al evento']);
               }

               if ($event->event_state_id == EventsStatus::getCompletedId() || $event->companies()->count() >= $event->number_attendees) {
                    $event->companies()->attach($company->id, ['in_reserve' => 1]);
               } else {
                    $event->companies()->attach($company->id);
                    if ($event->number_attendees == $event->companies()->count()) {
                         $state = EventState::find(EventsStatus::getCompletedId());
                         $event->state()->associate($state);
                         $event->save();
                    }
               }

               return $this->responseSucess('store', $companyEvent->id);
          });

          return $result;
     }
     /**
      * Remove the specified resource from storage.
      *
      * @param int $id
      * @return Response
      */
     public function destroy($id = null)
     {
          if ($id) {
               if (!$this->model->options['can_delete']) return redirect()->route($this->module . '.index');
          }

          $dataError = [
               'success' => false,
               'message' => trans('buttons.delete-fail')
          ];

          $companyEvent = CompanyEvent::find($id);
          if (!$companyEvent) {
               return Response::make(json_encode($dataError), 200);
          }

          $event = Event::find($companyEvent->event_id);
          if (!$event) {
               return Response::make(json_encode($dataError), 200);
          }

          $event->companies()->detach($companyEvent->company_id);
          if ($event->companies()->count() < $event->number_attendees) {
               if ($event->event_state_id == EventsStatus::getCompletedId()) {
                    $state = EventState::find(EventsStatus::getAvailableId());
                    $event->state()->associate($state);
                    $event->save();
               }

               $reserveCount = $event->companies()->wherePivot('in_reserve', 1)->count();
               if ($reserveCount) {
                    $event->notify(new EventWithPlaces($reserveCount));
               }
          }

          return $this->responseSucess(__FUNCTION__, $id);
     }
     /**
      * Pass assist from reserve to confirmed status
      *
      * @param Int $id
      */
     public function confirm($id)
     {
          $assistance = CompanyEvent::find($id);

          if (!$assistance) {
               return $this->responseInvalid(__FUNCTION__);
          }

          $assistance->in_reserve = 0;
          $saved = $assistance->save();

          if ($saved) {
               EventReserveConfirmed::dispatch($assistance);
          }

          return $this->responseSucess(__FUNCTION__, $id);
     }
}
