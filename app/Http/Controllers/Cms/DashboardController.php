<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Helpers\Application;
use App\Models\Module;
use Illuminate\Support\Facades\DB;

use Auth;

class DashboardController extends Controller
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     */
    public function __construct()
    {
        Application::setCMS();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::where([
            ['active', '=', 1],
            ['is_dashboard', '=', 1]
        ])
            ->orderBy('data_order', 'asc')
            ->get();

        $items = [];
        foreach ($modules as $module) {
            $count = DB::table($module->table_name)->whereNull('deleted_at')->count();
            $items[] = [
                'title' => $module->description,
                'icon' => $module->type_icon . ' ' . $module->icon,
                'count' => $count,
                'slug' => $module->slug
            ];
        }

        return view('dashboard')->with('items', $items)->with('module', 'dashboard');
    }
}
