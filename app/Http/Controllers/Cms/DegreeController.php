<?php

namespace App\Http\Controllers\Cms;


use App\Presenters\DegreePresenter;

use App\Models\Degree;

class DegreeController extends BaseController
{

	/**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
	public function __construct(Degree $model, DegreePresenter $presenter = null) {
		parent::__construct($model, $presenter);

		$this->module = 'degree';
		$this->urlBack = array(
            'store' => '',
            'update' => ''
        );
	}

}
