<?php

namespace App\Http\Controllers\Cms;

use App\Presenters\EventPresenter;

use App\Models\Event;

use App\Events\EventCreated;

class EventController extends BaseController
{
     /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
      */
     public function __construct(Event $model, EventPresenter $presenter = null)
     {
          parent::__construct($model, $presenter);

          $this->module = 'event';
          $this->urlBack = array(
               'store' => '',
               'update' => ''
          );
     }
     /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
     /*public function store($id = null)
     {
          $response = parent::store($id);

          if (!$id && $response->getSession()->get('flash_message_title') == trans('global.success')) {
               $event = Event::orderBy('created_at', 'DESC')->first();
               EventCreated::dispatch($event);
          }

          return $response;
     }*/
}
