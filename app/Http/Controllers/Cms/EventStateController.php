<?php

namespace App\Http\Controllers\Cms;


use App\Presenters\EventStatePresenter;

use App\Models\EventState;

class EventStateController extends BaseController
{

	/**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
	public function __construct(EventState $model, EventStatePresenter $presenter = null) {
		parent::__construct($model, $presenter);

		$this->module = 'event_state';
		$this->urlBack = array(
            'store' => '',
            'update' => ''
        );
	}
	
}