<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\DB;

use App\Presenters\InscriptionPresenter;

use Carbon\Carbon;

use App\Models\Inscription;
use App\Models\User;
use App\Models\UserData;
use App\Models\Company;

class InscriptionController extends BaseController
{

    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
    public function __construct(Inscription $model, InscriptionPresenter $presenter = null)
    {
        parent::__construct($model, $presenter);

        $this->module = 'inscription';
        $this->urlBack = array(
            'store' => '',
            'update' => ''
        );
    }
    /**
     * Create user from inscription
     * 
     * @param Int $id
     * @return Response
     */
    public function createUser($id)
    {
        $result = DB::transaction(function () use ($id) {
            $inscription = Inscription::find($id);
            if (!$inscription) {
                return $this->responseInvalid(__FUNCTION__);
            }
            $data = $inscription->toArray();

            $email = $inscription->email;

            $existsUserData = UserData::where('email', $email)->first();
            $existsUser = User::where('username', $email)->first();

            if ($existsUserData || $existsUser) {
                return $this->responseInvalid(__FUNCTION__, ['email' => 'Ya existe en la base de datos']);
            }

            $user = [
                'username' => $email,
                'active' => 0,
                'renovation_date' => Carbon::today()->addYear()->format('d/m/Y')
            ];

            $user = User::create($user);
            if (!$user) {
                return $this->responseInvalid(__FUNCTION__);
            }

            $userData = UserData::create($data);
            if (!$userData) {
                return $this->responseInvalid(__FUNCTION__);
            }

            $data['name'] = $data['company_name'];
            $company = Company::create($data);
            if (!$company) {
                return $this->responseInvalid(__FUNCTION__);
            }

            $user->inscription()->save($inscription);
            $user->userData()->save($userData);
            $user->company()->save($company);

            $inscription->notifyHasBeenAccepted();

            return redirect()->route('user.edit', ['id' => $user->id])
                ->with('flash_message', trans('buttons.store-success'))->with('flash_message_title', trans('global.success'));
        });

        return $result;
    }
}
