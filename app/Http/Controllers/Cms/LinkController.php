<?php

namespace App\Http\Controllers\Cms;

use App\Models\Link as LinkModel;
use Link;

use App\Presenters\LinkPresenter;


class LinkController extends BaseController
{
    /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
      */
      public function __construct(LinkModel $model, LinkPresenter $presenter = null)
      {
           parent::__construct($model, $presenter);

           $this->module = 'link';
           $this->urlBack = array(
                'store' => '',
                'update' => ''
           );
      }
}
