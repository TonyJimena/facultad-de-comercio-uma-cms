<?php

namespace App\Http\Controllers\Cms;

use App\Presenters\ModulePresenter;

use App\Models\Module;

class ModuleController extends BaseController
{

     /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
      */
     public function __construct(Module $model, ModulePresenter $presenter = null)
     {
          parent::__construct($model, $presenter);

          $this->module = 'module';
          $this->urlBack = array(
               'store' => '',
               'update' => ''
          );
     }
}
