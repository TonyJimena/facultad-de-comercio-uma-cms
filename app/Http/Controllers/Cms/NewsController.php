<?php

namespace App\Http\Controllers\Cms;

use App\Models\News as NewModel;
use News;

use App\Presenters\NewsPresenter;


class NewsController extends BaseController
{
    /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
      */
      public function __construct(NewModel $model, NewsPresenter $presenter = null)
      {
           parent::__construct($model, $presenter);

           $this->module = 'NewsModel';
           $this->urlBack = array(
                'store' => '',
                'update' => ''
           );
      }
}
