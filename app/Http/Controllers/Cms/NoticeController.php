<?php

namespace App\Http\Controllers\Cms;

use App\Models\Notice;
use App\Presenters\NoticePresenter;
use App\Models\UserCms;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class NoticeController extends BaseController
{
    /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
      */
      public function __construct(Notice $model, NoticePresenter $presenter = null)
      {
           parent::__construct($model, $presenter);

           $this->module = 'notice';
           $this->urlBack = array(
                'store' => '',
                'update' => ''
           );
      }

       /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store2($id = null)
    {
        try {
            /*$id
            ? $this->authorize('update' . '_' . $this->model->getTable(), get_class($this->model))
            : $this->authorize('create' . '_' . $this->model->getTable(), get_class($this->model));*/
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        if ($id != null) {
            if (!$this->model->options['can_edit']) return redirect()->route($this->module . '.index');
        } else {
            if (!$this->model->options['can_create'] && !$this->model->options['can_send']) return redirect()->route($this->module . '.index');
        }

        \DB::enableQueryLog();

        self::log(__FUNCTION__);

        // $input = request()->except(array('_token', 'id'));

        $input = request()->only($this->model->fields);
        $userAuth = Auth::user();
        $id = Auth::id();

        $user = UserCms::find($id);

        $input['user_name'] = $user->username;
        $input['user_id'] = $id;

        dd($input);
        //unset($input['id']);
        if (count($this->storeInputExtra) > 0) {
            $input = array_merge($input, $this->storeInputExtra);
        }
        if ($id == NULL || $id < 0) {
            unset($input['id']);
        }
        $this->log(__FUNCTION__, 'info', $id);

        $only = request()->only($this->model->fields);
        $fillables = request()->all();
        //dd($fillables);
        $modelClassUses = class_uses($this->model);

        // Get the model to update
        $this->log(__FUNCTION__, 'debug', 'Begin transaction ' . get_class($this->model) . ' ' . $id);
        DB::beginTransaction();
        try {
            $success = false;
            try {
                $success = $this->preStore($this->model); //Mejorar el control de excepciones.

                if ($success) {
                    // Sets the authored fields
                    if ($this->model->isAuthoredBehaviour()) {
                        $this->model->updateAuthored();
                    }

                    $rules = $this->model->getRules();
                    $success = false;
                    $success = $this->model->validate(request()->all(), $rules);

                    if ($id != null) {
                        if (array_key_exists('Illuminate\Database\Eloquent\SoftDeletes', $modelClassUses)) {
                            $this->model = $this->model->withTrashed()->find($id);
                        } else {
                            $this->model = $this->model->find($id);
                        }

                        $this->modelToTimeline = $this->model->replicate();
                    } else {
                        //$input['created_at'] = $input['updated_at'];
                    }

                    if ($success) {
                        $success = $this->model->fill($only)->save();
                    }
                } else {
                    if (!$this->model->validationErrors)
                        $this->model->validationErrors = new MessageBag();

                    $this->model->validationErrors->add('preSaveError', 1);
                }
            } catch (MassAssignmentException $exception) {
                $this->model->validationErrors->add($exception->getMessage(), trans('validation.date', ['attribute' => $exception->getMessage()]));
            }

            if (!$success) {
                $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                DB::rollback();
                //dd('aqui1');
                return $this->responseInvalid(__FUNCTION__);
            } else {
                // Update files
                $this->updateImages();
                $this->updateFiles();
                //$this->rewriteFiles();
                $this->updatePositionsBanners();
                $this->removeCacheKeys($this->model);

                $id = $this->model->id;

                // Extract related values
                $related = $this->getRelatedInput(request()->all());
                $save_related = true;
                // Save Related
                foreach ($related as $module => $values) {

                    $relatedClass = 'App\\Models\\' . ucfirst($module);
                    $this->log(__FUNCTION__, 'debug', get_class($this->model) . ' ' . $id . ' related ' . $relatedClass);
                    if (isset($values['0'])) {
                        // Varios registros << Sin testear >>

                        foreach ($values as $key => $one_related) {
                            $class = new $relatedClass();
                            if ($class->fill($one_related)->save()) {
                                $save_related = $save_related && $this->model->$module()->save($class);
                                if (!$save_related) {
                                    $save_related = false;
                                    $this->log(__FUNCTION__, 'error', get_class($this->model) . ' ' . $id . ' related ' . get_class($class));
                                    break 2;
                                }
                            } else {
                                $save_related = false;
                                $this->log(__FUNCTION__, 'debug', 'Invalid input for ' . get_class($class));
                                // Rollback transaccion
                                $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                                DB::rollback();

                                return Redirect::back()->withInput()->withErrors($class->errors);
                            }
                        }
                    } else {

                        // Registro unico
                        $class = new $relatedClass();

                        if (!empty($values[$class->getKeyName()])) {
                            // Update
                            $this->log(__FUNCTION__, 'debug', 'Attempting to update ' . get_class($this->model) . ' ' . $id . ' related ' . get_class($class) . ' ' . $values[$class->getKeyName()]);
                            $class = $class::find($values[$class->getKeyName()]);
                        }
                        unset($values[$class->getKeyName()]);

                        if ($class->fill($values)->save()) {
                            $save_related = $this->model->$module()->save($class);
                            if (!$save_related) {
                                $this->log(__FUNCTION__, 'error', get_class($this->model) . ' ' . $id . ' related ' . get_class($class));
                                break;
                            } else {
                                $this->log(__FUNCTION__, 'debug', 'save ' . get_class($this->model) . ' ' . $id . ' related ' . get_class($class) . ' ' . $class->getKey());
                            }
                        } else {
                            if (!empty($class->validationErrors)) {
                                $errors = $class->validationErrors->toArray();

                                foreach ($errors as $key => $messages) {
                                    foreach ($messages as $message) {
                                        $this->model->validationErrors->add($key, $message);
                                    }
                                }
                            }

                            // Rollback transaccion
                            $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                            DB::rollback();
                            $save_related = false;
                            //dd('aqui3');
                            return $this->responseInvalid(__FUNCTION__, $class->errors);
                        }
                    }
                }
            }

            if ($this->model->relationsCheckboxes) {
                foreach ($this->model->relationsCheckboxes as $table => $config) {
                    $modelName = '\\App\\Models\\' . Stringy::upperCamelize($table);
                    $model = new $modelName();

                    $data = DB::table($table . ' AS t')->leftJoin($config['nm'], function ($join) use ($id, $config) {
                        $join->on($config['nm'] . '.' . $config['otherID'], '=', 't.id');
                        $join->on($config['nm'] . '.' . $config['thisID'], '=', DB::raw($id));
                    });
                    if ($model->fieldsLangs) {
                        $data = $data->leftJoin($table . '_langs AS tl', function ($join) use ($config) {
                            $join->on('tl.' . $config['otherID'], '=', 't.id')
                                ->where('tl.language_id', '=', Language::getMainLanguage()->id);
                        });
                        $data = $data->select('t.*', 'tl.*', $config['nm'] . '.' . $config['thisID']);
                    } else {
                        $data = $data->select('t.*', $config['nm'] . '.' . $config['thisID']);
                    }
                    if ($config['filtered'])
                        $data = $data->where($config['filtered'], $this->model->{$config['filtered']});

                    $data = $data->get();

                    foreach ($fillables as $key => $value) {
                        if (strpos($key, $config['nm']) === 0) {
                            $id_check = intVal(str_replace($config['nm'] . '_', '', $key));
                            if ($value == 1) {
                                if (DB::table($config['nm'])->where($config['thisID'], $id)->where($config['otherID'], $id_check)->count() == 0) {
                                    DB::table($config['nm'])->insert(
                                        [$config['otherID'] => $id_check, $config['thisID'] => $id]
                                    );
                                }
                            } else {
                                DB::table($config['nm'])->where($config['thisID'], $id)->where($config['otherID'], $id_check)->delete();
                            }
                        }
                    }
                }
            }

            if ($save_related && $this->postStore($this->model)) {
                $this->log(__FUNCTION__, 'debug', 'Commit ' . get_class($this->model) . ' ' . $id);
                DB::commit();

                return $this->responseSucess(__FUNCTION__, $id);
            } else {
                // Rollback transaccion
                $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                DB::rollback();
                throw new AppException('POSTUPDATE ERROR in ' . get_class($this) . '->' . __FUNCTION__);
            }
        } catch (Exception $e) {
            $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
            DB::rollback();
            throw $e;

            throw new AppException('POSTUPDATE ERROR in ' . get_class($this) . '->' . __FUNCTION__ . '\n' . $e->getMessage());
        }
    }

}
