<?php

namespace App\Http\Controllers\Cms;

use App\Presenters\NotificationPresenter;

use App\Models\Notification;

class NotificationController extends BaseController
{

    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
    public function __construct(Notification $model, NotificationPresenter $presenter = null)
    {
        parent::__construct($model, $presenter);

        $this->module = 'notification';
        $this->urlBack = array(
            'store' => '',
            'update' => ''
        );
    }
}
