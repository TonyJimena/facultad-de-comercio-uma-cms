<?php

namespace App\Http\Controllers\Cms;


use App\Presenters\SectorPresenter;

use App\Models\Sector;

class SectorController extends BaseController
{

	/**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
	public function __construct(Sector $model, SectorPresenter $presenter = null) {
		parent::__construct($model, $presenter);

		$this->module = 'sector';
		$this->urlBack = array(
            'store' => '',
            'update' => ''
        );
	}
	
}