<?php

namespace App\Http\Controllers\Cms;


use App\Presenters\TypeNoticePresenter;

use App\Models\TypeNotice;

class TypeNoticeController extends BaseController
{

	/**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
	public function __construct(TypeNotice $model, TypeNoticePresenter $presenter = null) {
		parent::__construct($model, $presenter);

		$this->module = 'type_notice';
		$this->urlBack = array(
            'store' => '',
            'update' => ''
        );
	}

}
