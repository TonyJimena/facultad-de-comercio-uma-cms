<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

use App\Presenters\UserPresenter;

use App\Models\User;
use App\Models\Sector;
use App\Models\UserData;
use App\Models\Company;

use App\Notifications\PasswordChanged;

class UserController extends BaseController
{

     /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
      */
     public function __construct(User $model, UserPresenter $presenter = null)
     {
          parent::__construct($model, $presenter);

          $this->module = 'user';
          $this->urlBack = array(
               'store' => '',
               'update' => ''
          );
     }
     /**
      * Set data to share in view
      */
     private function setDataToShare()
     {
        //   $sectors = Sector::select('id', 'name')->orderBy('name')->get();
        //   $sectorsAux = [];
        //   foreach ($sectors as $sector) {
        //        $sectorsAux[$sector->id] = $sector->name;
        //   }
        //   $sectors = $sectorsAux;

        //   View::share([
        //        'sectors' => $sectors,
        //   ]);
     }
     /**
      * Show the form for editing the specified resource.
      *
      * @param int $id
      * @return Response
      */
     public function edit($id = NULL)
     {
          //$this->setDataToShare();

          return parent::edit($id);
     }
     /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
     public function store($id = NULL)
     {
          $result = DB::transaction(function () use ($id) {
               if ($id != null) {
                    if (!$this->model->options['can_edit']) return redirect()->route($this->module . '.index');
                    $user = User::find($id);
               } else {
                    if (!$this->model->options['can_create']) return redirect()->route($this->module . '.index');
               }

               $rules = array_merge(
                    $this->model->getRules(),
                    (new UserData())->getRules(),
                    (new Company())->getRules(),
                    request()->password ? [
                         'password' => 'min:6|regex:/[a-z]/|regex:/[A-Z]/'
                    ] :
                         []
               );

               $success = false;
               $success = $this->model->validate(array_filter(request()->all()), $rules);

               if (!$success) {
                    return $this->responseInvalid(__FUNCTION__);
               }

               $username = request()->username;
               $email = request()->email;

               $existsUserData = UserData::where([
                    ['email', $email],
                    ['user_id', '!=', $id]
               ])->first();
               $existsUser = User::where([
                    ['username', $username],
                    ['id', '!=', $id]
               ])->first();

               if ($existsUserData || $existsUser) {
                    return $this->responseInvalid(__FUNCTION__, [
                         'email' => 'Ya existe en la base de datos',
                         'username' => 'Ya existe en la base de datos'
                    ]);
               }

               $changedPassword = request()->password ? true : false;

               $data = request()->password ? request()->all() : request()->except(['password']);

               $updatedUser = $user->update($data);
               if (!$updatedUser) {
                    return $this->responseInvalid(__FUNCTION__);
               }

               $userData = $user->userData;
               $updatedUserData = $userData->update($data);
               if (!$updatedUserData) {
                    return $this->responseInvalid(__FUNCTION__);
               }

               $company = $user->company;
               $data['name'] = $data['company_name'];
               $updatedCompany = $company->update($data);
               if (!$updatedCompany) {
                    return $this->responseInvalid(__FUNCTION__);
               }

               if ($changedPassword) {
                    $user->notify(new PasswordChanged(request()->password));
               }

               return $this->responseSucess('store', $id);
          });

          return $result;
     }
}
