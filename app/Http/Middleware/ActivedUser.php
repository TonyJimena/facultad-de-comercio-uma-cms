<?php

namespace App\Http\Middleware;

use Closure;

class ActivedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if (!$user || !$user->active) {
            $error = trans('auth.inactive');
            return response([
                'message' => $error
            ], 401);
        }

        return $next($request);
    }
}
