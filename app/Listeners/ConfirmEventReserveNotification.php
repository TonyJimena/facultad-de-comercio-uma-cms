<?php

namespace App\Listeners;

use App\Events\EventReserveConfirmed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ConfirmEventReserveNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventReserveConfirmed  $event
     * @return void
     */
    public function handle(EventReserveConfirmed $event)
    {
        $event->assistance->notifyHasBeenConfirmed();
    }
}
