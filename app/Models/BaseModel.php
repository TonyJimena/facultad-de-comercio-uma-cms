<?php

namespace App\Models;

use Doctrine\Common\Collections\Criteria;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Models\TraitAuthored;
use App\Models\InterfaceAuthored;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Database\Eloquent\MassAssignmentException;

use Auth;
use Str;

class BaseModel extends Model
{

    use TraitAuthored;

    /**
     * List of attribute names which has multilanguage
     *
     * @var array
     */
    public static $i18nFields = array();

    /**
     * The model's i18n attributes.
     *
     * @var array
     */
    protected $i18nAttributes = array();

    /**
     * The model's i18n date attributes.
     *
     * @var array
     */
    protected $i18nDateAttributes = [];

    /**
     * The model i18n attribute's original state.
     *
     * @var array
     */
    protected $i18nOriginal = [];
    /**
     * Dates to convert to ISO 8601 format.
     *
     * @var array
     */
    public $datesToIso8601 = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * List of attribute names which are currency values
     *
     * @var array
     */
    public static $currencyFields = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $schema = '';

    /**
     * The validation rules associated with the model
     *
     * @var array
     */
    static $rules = [];
    static $rulesApi = [];

    /**
     * Array of relations for grid
     *
     * @var array
     */
    public $relationsGrid;

    public $relationsCheckboxes;

    public $relationsCombo;

    public $relationsGridColumns;
    /**
     * Sort default
     */
    public $sortDefaultColumn = 'created_at';
    public $sortDefaultDir = 'desc';

    public $fieldsForNoChecking = [];
    /**
     * Relations to delete
     */
    public $relationsToDelete = [];
    /**
     * The array of custom error messages.
     *
     * @var array
     */
    public static $customMessages = array();
    /**
     * The array of custom attributes.
     *
     * @var array
     */
    public static $customAttributes = array();
    /**
     * The validator object in case you need it externally (say, for a form builder).
     *
     * @see getValidator()
     * @var \Illuminate\Validation\Validator
     */
    protected $validator;
    /**
     * The message bag instance containing validation error messages
     *
     * @var \Illuminate\Support\MessageBag
     */
    public $validationErrors;


    /**
     * Indicates if the model should be private.
     *
     * @var bool
     */
    protected $privateBehaviour = false;

    /**
     * Grid columns for the model
     *
     * @var array
     */
    public $gridColumns = [];
    /**
     * Columns to return in api
     */
    public $columnsApi = [];
    /**
     * Related columns to return in api
     */
    public $relationsApi = [];

    /**
     * Grid columns to make the query condition in the grid
     *
     * @var array
     */
    public $gridColumnsToQuery = [];

    const IMG_PATH = '/uploads/';
    /**
     * Width for uploaded images
     */
    public $widthImages = 1920;
    /**
     * Quality for uploaded images
     */
    public $qualityImages = 60;
    public $relationsUploadMassive = [];
    public $fieldsLangs = [];

    /**
     * Grid columns for the model
     *
     * @var array
     */
    public $options = [
        'seo' => false,
        'can_create' => false,
        'can_edit' => false,
        'can_delete' => true,
        'can_inactive' => true,
        'can_see' => true,
        'can_cancel' => false,
        'can_ok' => true,
        'can_send' => false
    ];
    public $modelOptions = [];
    /**
     * Determine if a get mutator exists for an attribute.
     *
     * @param  string  $key
     * @return bool
     */
    public function hasGetMutator($key)
    {
        return (in_array($key, $this->i18nDateAttributes) || in_array($key, $this->getCurrencyFields()) || method_exists($this, 'get' . Str::studly($key) . 'Attribute'));
    }


    /**
     * Get the value of an attribute using its mutator.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return mixed
     */
    protected function mutateAttribute($key, $value)
    {
        if (method_exists($this, 'get' . Str::studly($key) . 'Attribute')) {
            return parent::mutateAttribute($key, $value);
        } else if (in_array($key, $this->i18nDateAttributes)) {
            $format = trans('format.date');
            // TODO Get TZ from users profile
            $timezone = "Europe/Madrid";
            return $this->dateAccessor($value, $format, $timezone);
        } elseif (in_array($key, $this->getCurrencyFields())) {
            // TODO Create get for mutate Currency

            // manage currency, base values
            // Get currency value from user profile
            $currency_base = 'USD';
            // Value to display
            // The _equivalence attribute has the currency base value
            if (array_key_exists($key . '_equivalence', $this->attributes)) {

                $value = $this->attributes[$key . '_equivalence'];

                // Currency settings
                $base = 100;
                $symbol = '$';
                $display_decimals = 2;

                // Locale settings
                $decimal = trans('format.decimal');
                $thousands = trans('format.thousands');

                return ($value)
                    ? (number_format(($value / $base), $display_decimals, $decimal, $thousands))
                    : null;
            } else {
                return null;
            }
        }
        return parent::getAttribute($key);
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return void
     */
    public function setAttribute($key, $value)
    {
        // First we will check for the presence of a mutator for the set operation
        // which simply lets the developers tweak the attribute as it is set on
        // the model, such as "json_encoding" an listing of data for storage.
        if ($this->hasSetMutator($key)) {
            return parent::setAttribute($key, $value);
        }
        // If an attribute is listed as a "date", we'll convert it from a DateTime
        // instance into a form proper for storage on the database tables using
        // the connection grammar's date format. We will auto set the values.
        elseif (in_array($key, $this->i18nDateAttributes)) {
            $format = trans('format.dateTime');
            return $this->setDateTimeMutator($key, $value, $format);
        } else {
            $this->attributes[$key] = $value;
        }
    }

    /**
     * Dynamically retrieve attributes on the model.
     * Overrided from {@link Model} to implement recognition of the {@link $i18nFields} array.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->isI18nField($key)
            ? $this->getI18nAttribute($key)
            : $this->getAttribute($key);
    }

    /**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return void
     */
    public function __set($key, $value)
    {
        if ($this->isI18nField($key)) {
            $this->setI18nAttribute($key, $value);
        } else {
            $this->setAttribute($key, $value);
        }
    }

    /**
     * Setter de Schema
     *
     * @param String $value
     */
    public function setSchema($value)
    {
        $this->schema = $value;
    }

    /**
     * Getter de Schema
     *
     * @return string
     */
    public function getSchema()
    {
        if ($this->isPrivateReal() && Auth::check()) {
            $this->setSchema(Auth::getUser()->getProfileSchema());
        }
        return $this->schema;
    }

    /**
     * Returns if the model is private
     *
     * @return boolean
     */
    public function isPrivate()
    {
        return false;
        return $this->privateBehaviour;
    }

    public function isPrivateReal()
    {
        return $this->privateBehaviour;
    }

    public function hasSEO()
    {
        return isset($this->options['seo']) ? $this->options['seo'] : false;
    }
    /**
     * Get the table associated with the model with the schema prefix
     *
     * @return string
     */
    public function getTable()
    {
        return (($this->getSchema() == '') ? parent::getTable() : $this->getSchema() . '.' . parent::getTable());
    }

    public function getTableWithoutSchema()
    {
        return parent::getTable();
    }

    public static function getTableName()
    {
        $className = get_called_class();
        $model = new $className;

        return ($model->getTableWithoutSchema());
    }

    /**
     * Return the Validation Rules
     *
     * @return array
     */
    public function getRules()
    {
        return $this->mergeRules();
    }
    /**
     * Return the Validation Rules for API
     *
     * @return array
     */
    public function getRulesApi()
    {
        return static::$rulesApi;
    }
    /**
     * Merge Rules
     *
     * Merge the rules arrays to form one set of rules
     */
    private function mergeRules()
    {
        $output = [];
        if (isset(static::$rules['save'])) {
            if ($this->exists) {
                $merged = array_merge_recursive(static::$rules['save'], static::$rules['update']);
            } else {
                $merged = array_merge_recursive(static::$rules['save'], static::$rules['create']);
            }

            foreach ($merged as $field => $rules) {
                if (is_array($rules)) {
                    $output[$field] = implode("|", $rules);
                } else {
                    $output[$field] = $rules;
                }
            }
        } else {
            $output = static::$rules;
        }

        // Add unique except :id
        //         $replace = ($this->getKey() > 0) ? ',' . $this->getKey() : '';
        //         foreach ($output as $key => $rule) {
        //             $output[$key] = str_replace(',:' . $this->getKeyName(), $replace, $rule);
        //         }

        $output = $this->buildUniqueExclusionRules($output);

        $output = $this->buildLangsRules($output);

        return $output;
    }


    /**
     * COPIED FROM ARDENT SOURCE
     * https://github.com/laravel-ardent/ardent/blob/master/src/Ardent/Ardent.php
     *
     * When given an ID and a Laravel validation rules array, this function
     * appends the ID to the 'unique' rules given. The resulting array can
     * then be fed to a Ardent save so that unchanged values
     * don't flag a validation issue. Rules can be in either strings
     * with pipes or arrays, but the returned rules are in arrays.
     *
     * @param int   $id
     * @param array $rules
     *
     * @return array Rules with exclusions applied
     */
    protected function buildUniqueExclusionRules(array $rules = array())
    {

        if (!count($rules))
            $rules = static::$rules;
        foreach ($rules as $field => &$ruleset) {
            // If $ruleset is a pipe-separated string, switch it to array
            $ruleset = (is_string($ruleset)) ? explode('|', $ruleset) : $ruleset;
            foreach ($ruleset as &$rule) {
                if (strpos($rule, 'unique:') === 0) {

                    // Stop splitting at 4 so final param will hold optional where clause
                    $params = explode(',', $rule, 4);
                    $uniqueRules = array();
                    // Append table name if needed
                    $table = explode(':', $params[0]);
                    if (count($table) == 1)
                        $uniqueRules[1] = $this->getTable();
                    else
                        $uniqueRules[1] = $table[1];

                    // Append field name if needed
                    if (count($params) == 1)
                        $uniqueRules[2] = $field;
                    else
                        $uniqueRules[2] = $params[1];
                    if (isset($this->primaryKey)) {
                        $uniqueRules[3] = $this->{$this->primaryKey};

                        // If optional where rules are passed, append them otherwise use primary key
                        if (isset($params[3])) {
                            $uniqueRules[4] = $params[3];
                        }
                    } else {
                        $uniqueRules[3] = $this->id;
                    }

                    $rule = 'unique:' . implode(',', $uniqueRules);
                } // end if strpos unique

            } // end foreach ruleset
        }

        return $rules;
    }
    /**
     * Create the rules for validator with fields with languages
     */
    protected function buildLangsRules(array $rules = array())
    {
        $fieldsLangs = $this->fieldsLangs;
        $requiredLangsRules = [];

        if (count($fieldsLangs)) {
            foreach ($rules as $field => $rule) {
                if (in_array($field, $fieldsLangs)) {
                    array_pull($rules, $field);
                    foreach (Language::getAvailable() as $l) {
                        $locale = $l->code;
                        $fieldLang = $field . '_' . $locale;
                        $requiredLangsRules[$fieldLang] = $rule;
                    }
                }
            }
            $rules = array_merge($rules, $requiredLangsRules);
        }

        return $rules;
    }


    /**
     * Returns if the model has i18n support
     *
     * @return bool
     */
    public static function isI18n()
    {
        return !empty(static::$i18nFields);
    }

    /**
     * Return if the field is a i18n attribute
     *
     * @param unknown $field
     * @return bool
     */
    public static function isI18nField($field)
    {
        return in_array($field, static::$i18nFields);
    }


    /**
     * Returns the currency fields
     *
     * @return array
     */
    public static function getCurrencyFields()
    {
        return static::$currencyFields;
    }

    /**
     * Validate the model instance
     *
     * @param array $rules            Validation rules
     * @param array $customMessages   Custom error messages
     * @param array $customAttributes Custom attributes
     * @return bool
     * @throws InvalidModelException
     */
    public function validate(array $data, array $rules = array(), array $customMessages = array(), array $customAttributes = array())
    {

        // check for overrides, then remove any empty rules
        $rules = (empty($rules)) ? static::$rules : $rules;
        foreach ($rules as $field => $rls) {
            if ($rls == '') {
                unset($rules[$field]);
            }
        }
        if (empty($rules)) {
            $success = true;
        } else {
            $customMessages = (empty($customMessages)) ? static::$customMessages : $customMessages;
            $customAttributes = (empty($customAttributes)) ? static::$customAttributes : $customAttributes;

            /*
             * Esto lo comentamos porque ya se hidrata en el BaseController.
            if ($this->forceEntityHydrationFromInput || (empty($this->attributes) && $this->autoHydrateEntityFromInput)) {
                $this->fill(Input::all());
            }
            */
            //$data = $this->getAttributes(); // the data under validation
            // perform validation
            $this->validator = Validator::make($data, $rules, $customMessages, $customAttributes);
            $success = $this->validator->passes();
            if ($success) {
                // if the model is valid, unset old errors
                if ($this->validationErrors === null || $this->validationErrors->count() > 0) {
                    $this->validationErrors = new MessageBag;
                }
            } else {
                // otherwise set the new ones
                $this->validationErrors = $this->validator->messages();
                // stash the input to the current session
                if (request()->session()) {
                    request()->session()->flash('errors', $this->validationErrors);
                }
            }
        }

        return $success;
    }

    /**
     * Save the model to the database.
     *
     * @param array $rules
     * @param array $customMessages
     * @param array $options
     * @param Closure $beforeSave
     * @param Closure $afterSave
     *
     * @return bool
     * @see Ardent::forceSave()
     */
    public function save(array $rules = array(), array $customMessages = array(), array $options = array(), Closure $beforeSave = null, Closure $afterSave = null)
    {
        $success = parent::save($rules, $customMessages, $options, $beforeSave, $afterSave);

        if ($success && $this->isI18n()) {
            $success &= $this->savei18n($rules, $customMessages, $options, $beforeSave, $afterSave);
        }

        return $success;
    }

    /**
     * Save the i18n attributes model to the database.
     *
     * @param array $rules
     * @param array $customMessages
     * @param array $options
     * @param Closure $beforeSave
     * @param Closure $afterSave
     *
     * @return bool
     */
    protected function savei18n(array $rules = array(), array $customMessages = array(), array $options = array(), Closure $beforeSave = null, Closure $afterSave = null)
    {
        $success = true;
        if ($this->isI18n()) {
            // TODO Save i18n Fields
            //             foreach (Config::get('app.available_languages') as $locale) {
            //                 // echo $locale;
            //                 $success = true;
            //             }
        }
        return $success;
    }

    /**
     * Preprocess input data before fill-in the model
     *
     * @param array $input  Input data
     * @return array        Processed data
     */
    protected function preProcessData($input)
    {
        /*if(isset($input['priority']))
            $input['active'] = isset($input['active']);*/

        if ($this->isPrivateReal() && !$this->exists && Auth::check() && in_array('user_id', static::getFillable())) {
            $input['user_id'] = (!isset($input['user_id']))
                ? Auth::getUser()->getScopeKey()
                : $input['user_id'];
        } else {
            unset($input['user_id']);
        }

        // If has related_type
        if (in_array('related_type', $this->fillable)) {
            if (isset($input['related_type']) && isset($input['related_id'])) {

                $namespace = (new \ReflectionClass(get_class($this)))->getNamespaceName();

                $input['related_type'] = strpos($input['related_type'], '\\')
                    ? $input['related_type']
                    : $namespace . '\\' . $input['related_type'];
            }
        }

        return $input;
    }

    /**
     * Fill the model with an array of attributes.
     *
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model static
     * @throws MassAssignmentException
     */
    public function fill(array $attributes)
    {
        if ($this->isI18n()) {
            $this->fillI18n($attributes);
            $attributes = array_diff($attributes, static::$i18nFields);
        }
        return parent::fill($this->preProcessData($attributes));
    }

    /**
     * Get the fillable attributes for the model.
     *
     * @return array
     */
    public function getFillable()
    {
        $fillables = parent::getFillable();

        if ($this->isAuthoredBehaviour()) {
            // TODO get the columns values
            if (!in_array('created_by', $fillables)) {
                array_push($fillables, 'created_by');
            }
            if (!in_array('updated_by', $fillables)) {
                array_push($fillables, 'updated_by');
            }
        }
        return $fillables;
    }

    /**
     * Fill the i18n attribute's model with an array of attributes.
     *
     * @param array $attributes
     * @return null
     */
    public function fillI18n(array $attributes)
    {
        if ($this->isI18n()) {
            foreach (static::$i18nFields as $field) {
                foreach (Language::getAvailable() as $l) {
                    $locale = $l->code;
                    $i18nField = $field . '_i18n_' . $locale;
                    if ($this->isFillable($field) && array_key_exists($i18nField, $attributes)) {
                        $this->setI18nAttribute($field, $attributes[$i18nField], $locale);
                    }
                }
            }
        }
    }

    /**
     * i18n Translation model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translate()
    {
        return $this->isI18n() ? $this->hasMany('Translates', 'id') : null;
    }

    /**
     * Accessor for dateTime fields
     *
     * @param string $value
     * @param string $timezone
     * @param string $format
     * @return string;
     */
    protected function dateTimeAccessor($value, $format = null, $timezone = "Europe/Madrid")
    {
        $format = ($format) ? $format : trans('format.dateTime');
        $carbon = new \Carbon\Carbon($value);
        return ($value)
            ? $carbon->copy()->tz($timezone)->format($format)
            : $value;
    }

    /**
     * Mutator for dateTime fields
     *
     * @param string $field
     * @param string $value
     * @param string $format
     * @return void
     * @throws MassAssignmentException
     */
    protected function setDateTimeMutator($field, $value, $format = null)
    {
        try {
            if (!empty($value)) {
                $format = ($format) ? $format : trans('format.dateTime');
                $value = (strrpos($value, ':') === false) ? $value . ' 00:00:00' : $value;
                $carbon = \Carbon\Carbon::createFromFormat($format, $value);
                $this->attributes[$field] = $carbon;
            } else {
                $this->attributes[$field] = null;
            }
        } catch (\InvalidArgumentException $e) {
            throw new MassAssignmentException($field);
        }
    }

    /**
     * Accesor for date fields
     *
     * @param string $value
     * @param string $timezone
     * @param string $format
     * @return \Carbon\Carbon
     */
    protected function dateAccessor($value, $format = null, $timezone = "Europe/Madrid")
    {
        $format = ($format) ? $format : trans('format.date');
        return $this->dateTimeAccessor($value, $format, $timezone);
    }

    /**
     * Mutator for date fields
     *
     * @param string $field
     * @param string $value
     * @param string $format
     * @return void
     */
    protected function setDateMutator($field, $value, $format = null)
    {
        $format = ($format) ? $format : trans('format.date');
        $this->setDateTimeMutator($field, $value, $format);
    }

    /**
     * Mutator for i18n fields
     *
     * @param string $field
     * @param string $value
     * @param string $locale
     * @return void
     */
    public function setI18nAttribute($field, $value, $locale = null)
    {
        $locale = ($locale) ? $locale : \App::getLocale();
        if (!array_key_exists($locale, $this->i18nAttributes))
            $this->i18nAttributes[$locale] = [];

        $this->i18nAttributes[$locale] = array_merge($this->i18nAttributes[$locale], [
            $field => $value
        ]);
    }

    /**
     * Accesor for i18n fields
     *
     * @param string $field
     * @param string $locale
     * @return string
     */
    public function getI18nAttribute($field, $locale = null)
    {
        $locale = $locale ?: \App::getLocale();
        if (!array_key_exists($locale, $this->i18nAttributes)) {
            $languages = Language::getAvailable();
            $locale = array_column('code', $languages->toArray());
            $locale = array_shift($locale);

            if (!array_key_exists($locale, $this->i18nAttributes))
                return null;
        }

        if (!array_key_exists($field, $this->i18nAttributes[$locale]))
            return null;

        return $this->i18nAttributes[$locale][$field];
    }
    public function getCreatedAtAttribute($value)
    {
        if (request()->is('api/*')) {
            return to_iso_8601_string($value);
        } else {
            return $this->dateTimeAccessor($value);
        }
    }
    /**
     * Accesor for i18n fields
     *
     * @param string $field
     * @return array
     */
    public function getI18nAttributes($field = null)
    {
        if (is_null($field)) {
            return $this->i18nAttributes;
        } elseif (!$this->isI18nField($field)) {
            return null;
        } else {
            $languages = Language::getAvailable();
            $locales = array_column('code', $languages);
            $result = [];
            foreach ($locales as $locale) {
                if (!array_key_exists($locale, $result))
                    $result[$locale] = [];
                $result[$locale] = array_merge($result[$locale], [$field => $this->getI18nAttribute($field, $locale)]);
            }
        }

        return $result;
    }

    /**
     * Get the attributes that have been changed since last sync.
     *
     * @return array
     */
    public function getDirty()
    {
        $dirty = array();

        foreach ($this->i18nAttributes as $locale => $values) {
            foreach ($values as $key => $value) {
                if (array_key_exists($key, $dirty))
                    continue;
                if (array_key_exists($locale, $this->i18nOriginal) && array_key_exists($key, $this->i18nOriginal[$locale])) {
                    if ($value !== $this->i18nOriginal[$locale][$key]) {
                        $dirty[$key] = $value;
                    }
                } else {
                    $dirty[$key] = $value;
                }
            }
        }

        $dirty = array_merge($dirty, parent::getDirty());

        return $dirty;
    }

    public function getFillables()
    {
        return $this->fillable;
    }

    public function isNotUsingDBView()
    {

        return (strpos('_view', $this->getTable()) === false) ? true : false;
    }

    public static function comboFromCollection($collection, $select_none = true, $module_trans = false)
    {

        if ($select_none) {
            //            $collection = $collection->put( 0, trans( 'global.select-none' ) )->toArray();
            $collection = array_prepend($collection->toArray(), trans('global.select-none'), 0);
        } else
            $collection = $collection->toArray();
        //        ksort( $collection );

        if ($module_trans) {
            foreach ($collection as &$coll)
                $coll = trans($module_trans . '.' . $coll);
        }
        return $collection;
    }

    public static function combo($displayName = 'name', $id = 'id', $select_none = true, $module_trans = false, $where = array())
    {
        $collection = null;

        $addCode = false;
        $columns = DB::getSchemaBuilder()->getColumnListing(self::getTableName());
        $querySelect = [$id, $displayName];
        if (in_array('code', $columns)) {
            $addCode = true;
            $querySelect[] = 'code';
        }

        if ($where) {
            $column = key($where);
            $value = array_shift($where);
            $collection = self::where($column, $value);

            foreach ($where as $column => $value) {
                $collection = $collection->where($column, $value);
            }

            $collection = $collection->select($querySelect)->get();
        } else {
            $collection = self::select($querySelect)->get();
        }

        $result = \Illuminate\Database\Eloquent\Collection::make();
        foreach ($collection as $row) {
            if ($addCode) {
                $rowValue = $row->code . ' - ' . $row->$displayName;
            } else {
                $rowValue = $row->$displayName;
            }

            $result->put($row->id, $rowValue);
        }

        return self::comboFromCollection($result, $select_none, $module_trans);
    }
    public function getURI()
    {

        return self::IMG_PATH . $this->path;
    }

    public function getFullPath()
    {

        return public_path() . $this->getURI();
    }
    /**
     * Return all columns of the model's table.
     *
     * @return array Array which contains columns for this model's table.
     */
    public function getTableColumns()
    {
        return DB::getSchemaBuilder()->getColumnListing($this->getTableName());
    }

    public function scopeByCompany($query, $company = null)
    {
        $columns = $this->getTableColumns();

        if (in_array('company_id', $columns)) {
            if (empty($company)) {
                $company = Auth::user()->company_id;
            }

            $query->whereCompanyId($company);
        }

        return $query;
    }

    public function getLang()
    {
        if ($this->fieldsLangs) {
            foreach (Language::getAvailable() as $l) {
                $lang = $l->id;
                $langcode = $l->code;
                $langData = DB::table($this->table . '_langs')->select($this->fieldsLangs)
                    ->where($this->table . '_id', $this->id)->where('language_id', $lang)->first();
                if ($langData) {
                    foreach ($this->fieldsLangs as $fL) {
                        $l = $fL . '_' . $langcode;
                        $this->$l = $langData->$fL;
                    }
                }
            }
        }
    }
    public function getSEO()
    {
        \DB::enableQueryLog();
        if ($this->options['seo']) {

            $seoModel = new \App\Models\SeoData();

            $select = array();
            $select[] = $seoModel->getTable() . '.*';
            if ($seoModel->fieldsLangs) {
                foreach (Language::getAvailable() as $lang) {
                    $l = $lang->code;
                    foreach ($seoModel->fieldsLangs as $fl) {
                        $select[] = "l$l.$fl as {$fl}_$l";
                    }
                }

                $seoData = $seoModel->select($select);
                $left = '';
                foreach (Language::getAvailable() as $lang) {
                    $l = $lang->id;
                    $lcode = $lang->code;
                    $seoData = $seoData->leftJoin($seoModel->getTable() . "_langs AS l$lcode", function ($q) use ($l, $lcode, $seoModel) {
                        $q->on("l$lcode." . $seoModel->getTable() . "_id", '=', $seoModel->getTable() . ".id");
                        $q->where("l$lcode.language_id", '=', $l);
                    });
                }
            }
            $seoData = $seoData->where([
                ['external_id', '=', $this->id],
                ['module_id', '=', $this->module_id]
            ])->first();

            if ($seoData) {
                foreach ($seoData->getAttributes() as $key => $d) {
                    if ($key != 'id') $this->$key = $d;
                }
            }

            $baseSeoFields = json_decode($seoModel->seoFields);
            $seoFields = json_decode($this->meta_data);
            $baseTabs = array_column($baseSeoFields, 'tab');
            if ($seoFields) {
                foreach ($seoFields as $seoField) {
                    $tab = $seoField->tab;
                    $key = array_search($tab, $baseTabs);
                    $contentBase = array_column($baseSeoFields[$key]->content, 'field');
                    $contents = $seoField->content ?: $baseSeoFields[$key]->content;
                    foreach ($contents as $content) {
                        $index = array_search($content->field, $contentBase);
                        if ($index !== false) {
                            foreach ($content as $i => $value) {
                                $baseSeoFields[$key]->content[$index]->$i = $value;
                            }
                        }
                    }
                }
            }
            $this->meta_data = $baseSeoFields;

            foreach (Language::getAvailable() as $lang) {
                $l = $lang->code;
                $baseSeoFieldsLangs = json_decode($seoModel->seoFields);
                $attribute = 'meta_data_lang_' . $l;
                $seoFields = json_decode($this->$attribute);
                $baseTabs = array_column($baseSeoFieldsLangs, 'tab');
                if ($seoFields) {
                    foreach ($seoFields as $seoField) {
                        $tab = $seoField->tab;
                        $key = array_search($tab, $baseTabs);
                        $contentBase = array_column($baseSeoFieldsLangs[$key]->content, 'field');
                        $contents = $seoField->content ?: $baseSeoFieldsLangs[$key]->content;
                        foreach ($contents as $content) {
                            $index = array_search($content->field, $contentBase);
                            foreach ($content as $i => $value) {
                                $baseSeoFieldsLangs[$key]->content[$index]->$i = $value;
                            }
                        }
                    }
                }
                foreach ($baseSeoFieldsLangs as $key => $baseSeoField) {
                    foreach ($baseSeoField->content as $index => $content) {
                        $newObject = new \StdClass();
                        foreach ($content as $i => $value) {
                            $attr = $i . '_' . $l;
                            $newObject->$attr = $value;
                        }
                        $baseSeoFieldsLangs[$key]->content[$index] = $newObject;
                    }
                }
                $this->$attribute = $baseSeoFieldsLangs;
            }
        }
    }
    /**
     * Get rules for fields with langs
     */
    public function getRulesLangs()
    {
        $fieldsLangs = $this->fieldsLangs;
        $rules = static::$rules;
        $requiredLangsRules = [];

        if (count($fieldsLangs)) {
            foreach ($rules as $field => $rule) {
                if (in_array($field, $fieldsLangs)) {
                    foreach (Language::getAvailable() as $lang) {
                        $locale = $lang->code;
                        $fieldLang = $field . '_' . $locale;
                        $requiredLangsRules[$fieldLang] = $rule;
                    }
                }
            }
        }

        return $requiredLangsRules;
    }
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        foreach ($this->modelOptions as $key => $value) {
            $this->options[$key] = $value;
        }
    }
    /**
     * Get the model's updated at.
     *
     * @param  string  $value
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        if (request()->is('api/*')) return to_iso_8601_string($value);

        return $value;
    }
}
