<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = null;
	protected $fillable = [
		'sector_id',
		'name',
		'address',
		'google_maps_url',
		'web',
		'number_centers',
		'number_employees',
		'services',
		'annual_billing',
	];
	public $fields = [
		'user_id',
		'sector_id',
		'name',
		'address',
		'google_maps_url',
		'web',
		'number_centers',
		'number_employees',
		'services',
		'annual_billing',
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'company';
	protected $dates = ['deleted_at'];
	static $rules = [
		'company_name' => 'required',
		'web' => 'required',
		'address' => 'required',
		'sector_id' => 'required',
	];
	static $rulesApi = [
		'company_name' => 'required',
		'web' => 'required',
		'address' => 'required'
	];
	/**
	 * Get the user that owns the company.
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id');
	}
	/**
	 * Get the sector that owns the company.
	 */
	public function sector()
	{
		return $this->belongsTo('App\Models\Sector', 'sector_id');
	}
	/**
	 * The events that belong to the company.
	 */
	public function events()
	{
		return $this->belongsToMany('App\Models\Event')
			->withPivot('in_reserve')
			->withTimestamps();
	}
}
