<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

use OneSignal;

class CompanyEvent extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $module_id = 6;
    protected $fillable = [
        'event_id',
        'company_id',
        'in_reserve'
    ];
    public $fields = [
        'event_id',
        'company_id',
        'in_reserve'
    ];
    public $gridColumns = [
        'id',
        'in_reserve'
    ];
    public $relationsGridColumns = [
        'title' => [
            'table' => 'event',
            'field' => 'title',
            'fk' => [
                'field' => 'event_id'
            ]
        ],
        'state' => [
            'table' => 'event_state',
            'field' => 'label',
            'fk' => [
                'field' => 'event_state_id',
                'table' => 'event'
            ]
        ],
        'name' => [
            'table' => 'company',
            'field' => 'name',
            'fk' => [
                'field' => 'company_id'
            ]
        ],
    ];
    public $filters = [
        'event' => [
            'table' => 'event',
            'field' => 'id',
            'display_fields' => ['title'],
            'type' => 'multiple',
            'relation' => 'fk',
            'fk' => [
                'field' => 'event_id',
            ]
        ],
        'company' => [
            'table' => 'company',
            'field' => 'id',
            'display_fields' => ['name'],
            'type' => 'multiple',
            'relation' => 'fk',
            'fk' => [
                'field' => 'company_id',
            ]
        ],
    ];
    static $rules = [
        'event_id' => 'required',
        'company_id' => 'required',
    ];
    public $options = [
        'seo' => false,
        'can_create' => true,
        'can_edit' => true,
        'can_delete' => true,
        'can_inactive' => false,
        'can_see' => true,
        'can_cancel' => true,
        'can_ok' => false,
        'can_send' => false
    ];
    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'company_event';
    protected $dates = ['deleted_at'];
    /**
     * Notify to user that his event reserve has been accepted
     */
    public function notifyHasBeenConfirmed()
    {
        $company = Company::find($this->company_id);
        $event = Event::find($this->event_id);

        $data = [
            'notification_type' => 'event_reserve_confirmed',
            'type_object' => 'event',
            'object_id' => $event->id,
            'title' => '¡Te esperamos en el evento!',
            'description' => 'Ya no estás en la reserva del evento "' . $event->title . '" y podrás asistir el ' . $event->date
        ];

        $users = [
            $company->user->where('active', 1)->first()
        ];
        foreach ($users as $user) {
            OneSignal::sendNotifications(
                $user->devices,
                null,
                $data
            );
        }
    }
}
