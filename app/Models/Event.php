<?php

namespace App\Models;

use App\Events\EventCreated;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

use Carbon\Carbon;

use OneSignal;

use App\Models\UserCms;

class Event extends BaseModel
{
	use SoftDeletes, Notifiable;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = 4;
	public $datesToIso8601 = [
		'created_at',
		'updated_at',
		'deleted_at',
		'date'
	];
	protected $fillable = [
		'event_state_id',
		'title',
		'description',
		'address',
		'google_maps_url',
		'date',
		'number_attendees',
		'image1',
		'image1_url',
		'file1',
		'file1_url'
	];
	public $fields = [
		'event_state_id',
		'title',
		'description',
		'address',
		'google_maps_url',
		'date',
		'number_attendees',
		'image1',
		'image1_url',
		'file1',
		'file1_url'
	];
	public $relationsGrid = [
		'company_event',
	];
	public $relatedField = 'event_id';
	public $relationsCombo = [
		'event_state' => ['label']
	];
	public $gridColumns = [
		'id',
		'image1',
		'title',
		'description',
		'address',
		'date',
		'number_attendees'
	];
	public $relationsGridColumns = [
		'event_state' => [
			'table' => 'event_state',
			'field' => 'label',
			'fk' => [
				'field' => 'event_state_id'
			]
		],
	];
	public $filters = [
		'title' => [
			'field' => 'title',
			'type' => 'text',
			'relation' => 'this'
		],
		'address' => [
			'field' => 'address',
			'type' => 'text',
			'relation' => 'this'
		],
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'event';
	public $numImages = 1;
	public $numFiles = 1;
	public $fieldImageName1 = 'title';
	public $fieldFileName1 = 'title';
	protected $dates = ['deleted_at'];
	static $rules = [
		'title' => 'required',
		'description' => 'required',
		'address' => 'required',
		'date' => 'required',
		'number_attendees' => 'required|numeric',
		'event_state_id' => 'required',
	];
	public $options = [
		'seo' => false,
		'can_create' => false,
		'can_edit' => false,
		'can_delete' => false,
		'can_inactive' => false,
		'can_see' => false,
		'can_cancel' => false,
		'can_ok' => false,
		'can_send' => false
	];
	/**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => EventCreated::class,
    ];
	/**
	 * Get the model's renovation date.
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getDateAttribute($value)
	{
		if (!$value) return $value;

		if (request()->is('api/*')) return to_iso_8601_string($value);

		return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i:s');
	}
	/**
	 * Set the model's renovation date.
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function setDateAttribute($value)
	{
		$this->attributes['date'] = Carbon::createFromFormat('d/m/Y H:i:s', $value)->toDateTimeString();
	}
	/**
	 * Route notifications for the mail channel.
	 *
	 * @param  \Illuminate\Notifications\Notification  $notification
	 * @return array|string
	 */
	public function routeNotificationForMail($notification)
	{
		$admins = UserCms::where([
			['user_cms_profile_id', 1],
			['active', 1]
		])->get();

		$emails = [];
		foreach ($admins as $admin) {
			$emails[$admin->email] = $admin->name;
		}

		return $emails;
	}
	/**
	 * The companies that belong to the event.
	 */
	public function companies()
	{
		return $this->belongsToMany('App\Models\Company', 'company_event')
			->withPivot('in_reserve')
			->withTimestamps();
	}
	/**
	 * The event that belong to the status.
	 */
	public function state()
	{
		return $this->belongsTo('App\Models\EventState', 'event_state_id');
	}
	/**
	 * Send notification to remember that event is tomorrow
	 */
	public function notifyIsTomorrow()
	{
		$companies = $this->companies()
			->with('user')
			->wherePivot('in_reserve', 0)
			->whereHas('user', function (Builder $query) {
				$query->where('active', 1);
			})
			->get();

		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->date)->format('H:i');

		$data = [
			'notification_type' => 'event_is_tomorrow',
			'type_object' => 'event',
			'object_id' => $this->id,
			'title' => '¡Te esperamos mañana!',
			'description' => 'Te recordamos que mañana a las ' . $date . ' tienes confirmada la asistencia al evento "' . $this->title . '".'
		];

		foreach ($companies as $company) {
			$users = [
				$company->user
			];
			foreach ($users as $user) {
				OneSignal::sendNotifications(
					$user->devices,
					null,
					$data
				);
			}
		}
	}
	/**
	 * Send notification when event is created
	 */
	public function notifyHasBeenCreated()
	{
		$users = User::has('devices')
			->where('active', 1)
			->get();

		$data = [
			'notification_type' => 'event_created',
			'type_object' => 'event',
			'object_id' => $this->id,
			'title' => 'Nuevo evento',
			'description' => 'Se acaba de crear el evento "' . $this->title . '" para el día ' . $this->date . '. Entra y, ¡No te quedes sin tu plaza!'
		];

		foreach ($users as $user) {
			OneSignal::sendNotifications(
				$user->devices,
				null,
				$data
			);
		}
	}
}
