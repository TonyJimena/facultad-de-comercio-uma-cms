<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class EventState extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = null;
	protected $fillable = [
		'name',
		'label',
		'description',
	];
	public $fields = [
		'name',
		'label',
		'description',
	];
	/**
	 * Grid columns for the model
	 *
	 * @var array
	 */
	public $gridColumns = [
		'id',
		'name',
		'label',
		'description'
	];
	public $modelOptions = [
		'can_create' => true,
		'can_edit' => true,
		'can_delete' => true,
		'can_inactive' => false,
		'can_see' => true,
		'can_cancel' => true,
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'event_state';
	protected $dates = ['deleted_at'];
	/**
	 * Get the events for the status.
	 */
	public function events()
	{
		return $this->hasMany('App\Models\Event', 'event_state_id');
	}
}
