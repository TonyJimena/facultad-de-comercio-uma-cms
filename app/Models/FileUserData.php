<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class FileUserData extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $module_id = null;
    protected $fillable = [
        'path',
        'original_name',
        'name',
        'mime_type',
        'content',
        'position'
    ];
    public $fields = [
        'user_data_id',
        'file_type_id',
        'path',
        'original_name',
        'name',
        'mime_type',
        'content',
        'position'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_data_id',
        'path',
        'original_name',
        'name',
        'mime_type',
        'content',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_preview'];
    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'file_user_data';
    protected $dates = ['deleted_at'];
    /**
     * Determine if the user is an administrator.
     *
     * @return bool
     */
    public function getUrlPreviewAttribute()
    {
        return $this->attributes['url_preview'] = route('api.files_user_data.preview', ['id' => $this->attributes['id']]);
    }
    /**
     * Get the user data that owns the file user data.
     */
    public function userData()
    {
        return $this->belongsTo('App\Models\UserData', 'user_data_id');
    }
    /**
     * Get the file user data type that owns the file user data.
     */
    public function type()
    {
        return $this->belongsTo('App\Models\FileType', 'file_type_id');
    }
}
