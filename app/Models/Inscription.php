<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;

use App\Notifications\InscriptionAccepted;

class Inscription extends BaseModel
{
	use SoftDeletes, Notifiable;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = 2;
	protected $fillable = [
		'sector_id',
		'name',
		'phone_number',
		'telephone_number',
		'email',
		'company_name',
		'web',
		'address',
		'google_maps_url',
		'position',
		'number_centers',
		'number_employees',
		'reason_registration',
		'contribution',
		'partner',
		'services',
		'annual_billing',
		'linkedin_url',
		'facebook_url',
		'instagram_url',
		'twitter_url'
	];
	public $fields = [
		'user_id',
		'sector_id',
		'name',
		'phone_number',
		'telephone_number',
		'email',
		'company_name',
		'web',
		'address',
		'google_maps_url',
		'position',
		'number_centers',
		'number_employees',
		'reason_registration',
		'contribution',
		'partner',
		'services',
		'annual_billing',
		'linkedin_url',
		'facebook_url',
		'instagram_url',
		'twitter_url'
	];
	public $relationsCombo = [
		'sector' => ['label']
	];
	/**
	 * Grid columns for the model
	 *
	 * @var array
	 */
	public $gridColumns = [
		'id',
		'user_id',
		'name',
		'email',
		'company_name',
		'number_centers',
		'number_employees',
	];
	public $relationsGridColumns = [
		'sector' => [
			'table' => 'sector',
			'field' => 'label',
			'fk' => [
				'field' => 'sector_id',
			]
		],
	];
	public $filters = [
		'name' => [
			'field' => 'name',
			'type' => 'text',
			'relation' => 'this'
		],
		'email' => [
			'field' => 'email',
			'type' => 'text',
			'relation' => 'this'
		],
		'company_name' => [
			'field' => 'company_name',
			'type' => 'text',
			'relation' => 'this'
		],
		'address' => [
			'field' => 'address',
			'type' => 'text',
			'relation' => 'this'
		],
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */

	protected $table = 'inscription';

	protected $dates = ['deleted_at'];
	static $rules = [
		'sector_id' => 'required',
		'name' => 'required',
		'position' => 'required',
		'company_name' => 'required',
		'number_centers' => 'required',
		'number_employees' => 'required',
		'web' => 'required',
		'telephone_number' => 'required',
		'address' => 'required',
		'email' => 'required|email',
		'reason_registration' => 'required',
		'contribution' => 'required',
		'partner' => 'required'
	];
	static $rulesApi = [
		'sector' => 'required',
		'name' => 'required',
		'position' => 'required',
		'company_name' => 'required',
		'number_centers' => 'required',
		'number_employees' => 'required',
		'web' => 'required',
		'telephone_number' => 'required',
		'address' => 'required',
		'email' => 'required|email|unique:inscription',
		'reason_registration' => 'required',
		'contribution' => 'required',
		'partner' => 'required'
	];
	public $options = [
		'seo' => false,
		'can_create' => true,
		'can_edit' => true,
		'can_delete' => true,
		'can_inactive' => false,
		'can_see' => true,
		'can_cancel' => true,
		'can_ok' => false,
		'can_send' => false
	];

    /*
    static $rulesApi = [
		'name' => 'required',
		'surname' => 'required',
		'username' => 'required',
		'password' => 'required',
		'student_code' => 'required',
		'student_degree_id' => 'required'
	];
    */
	/**
	 * Get the user that owns the inscription.
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id');
	}
	/**
	 * Get the sector that owns the inscription.
	 */
	public function sector()
	{
		return $this->belongsTo('App\Models\Sector', 'sector_id');
	}
	/**
	 * Send email to set account
	 */
	public function notifyHasBeenAccepted()
	{
		$token = Str::random(64);
		DB::table('set_account_tokens')->insert([
			'email' => $this->email,
			'token' => $token
		]);

		$this->notify(new InscriptionAccepted($token));
	}
}
