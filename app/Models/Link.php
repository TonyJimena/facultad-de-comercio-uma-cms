<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Link extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $module_id = 10;

    protected $fillable = [
        'active',
        'created_at',
        'data',
        'deleted_at',
        'id',
        'title',
        'updated_at',
        'url'
    ];

    public $gridColumns = [
		'id',
        'title',
        'url',
        'active'
	];

    public $options = [
		'seo' => false,
		'can_create' => true,
		'can_edit' => true,
		'can_delete' => true,
		'can_inactive' => false,
		'can_see' => true,
		'can_cancel' => true,
		'can_ok' => false,
		'can_send' => false
	];

        /**
     * The schema associated with the model.
     *
     * @var string
     */
        protected $table = 'link';

        /**
	 * The validation rules associated with the model
	 *
	 * @var array
	 */
        static $rules= [];
        // static $rulesApi = [
        //     'link_id' => 'required|numeric',
        //     'is_price_total' => 'required|boolean',
        //     'price_per_unit' => 'required|numeric',
        //     'price_total' => 'required|numeric',
        // ];
}
