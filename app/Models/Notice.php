<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Notice extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $module_id = 9;

    protected $fillable = [
        'active',
        //'created_at',
        'data',
        //'deleted_at',
        'description',
        'end_date',
        'id',
        'short_description',
        'title',
        'type',
        'type_id',
        //'updated_at',
        'user_id',
        'user_name'
    ];

    public $fields = [
        'active',
        //'created_at',
        'data',
        'deleted_at',
        'description',
        'end_date',
        'id',
        'short_description',
        'title',
        'type',
        'type_id',
        //'updated_at',
        'user_id',
        'user_name'
	];

    public $gridColumns = [
		'id',
        'user_name',
        'title',
        'short_description',
        'description',
        'type',
        'active'
	];

	// public $relationsGridColumns = [
	// 	'title' => [
	// 		'table' => 'notice',
	// 		'field' => 'title',

	// 	],
	// 	'description' => [
	// 		'table' => 'notice',
	// 		'field' => 'description',
	// 	]
	// ];

	// public $filters = [
	// 	'title' => [
	// 		'table' => 'notice',
	// 		'field' => 'title',
	// 		'type' => 'text',
	// 		'relation' => 'fk',
	// 		'mul' => [
	// 			'field' => 'user_id',
	// 		]
	// 	]
	// ];


	// public $columnsApi = [
	// 	'id',
	// 	'title'
	// ];

	// public $relationsApi = [
	// 	'title' => [
	// 		'table' => 'notice',
	// 		'field' => 'title',
	// 		'mul' => [
	// 			'field' => 'user_id'
	// 		]
	// 	],
	// ];


    public $options = [
		'seo' => false,
		'can_create' => true,
		'can_edit' => true,
		'can_delete' => true,
		'can_inactive' => false,
		'can_see' => true,
		'can_cancel' => true,
		'can_ok' => false,
		'can_send' => false
	];

        /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'notice';

        /**
	 * The validation rules associated with the model
	 *
	 * @var array
	 */

    static $rules= [];
    // static $rulesApi = [
    //     'link_id' => 'required|numeric',
    //     'is_price_total' => 'required|boolean',
    //     'price_per_unit' => 'required|numeric',
    //     'price_total' => 'required|numeric',
    // ];

    protected function preProcessData($input)
    {
        return $input;
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'user_id');
    }
}
