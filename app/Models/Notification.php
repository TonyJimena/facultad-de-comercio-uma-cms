<?php

namespace App\Models;

use App\Events\NotificationCreated;
use Illuminate\Database\Eloquent\SoftDeletes;

use OneSignal;

class Notification extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $module_id = 8;
    protected $fillable = [
        'title',
        'description'
    ];
    public $fields = [
        'title',
        'description'
    ];
    /**
     * Grid columns for the model
     *
     * @var array
     */
    public $gridColumns = [
        'id',
        'title',
        'description',
        'created_at',
    ];
    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'notification';
    /**
     * Filter columns for the model
     *
     * @var array
     */
    public $filters = [
        'title' => [
            'field' => 'title',
            'type' => 'text',
            'relation' => 'this'
        ],
        'description' => [
            'field' => 'description',
            'type' => 'text',
            'relation' => 'this'
        ],
        'created_at' => [
            'field' => 'created_at',
            'type' => 'date',
            'relation' => 'this'
        ],
    ];
    protected $dates = ['deleted_at'];
    /**
     * The validation rules associated with the model
     *
     * @var array
     */
    static $rules = [
        'title' => 'required|max:200',
        'description' => 'required'
    ];
    public $modelOptions = [
        'can_create' => false,
        'can_inactive' => false,
        'can_cancel' => true,
        'can_edit' => false,
        'can_delete' => false,
        'can_see' => false,
        'can_send' => true
    ];
    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => NotificationCreated::class,
    ];
    /**
     * Send notification
     * 
     * @param Array $users
     * @param Array $data
     */
    public function send()
    {
        $users = User::has('devices')->get();

        foreach ($users as $user) {
            OneSignal::sendNotifications(
                $user->devices,
                $this,
                [
                    'notification_type' => 'cms_notification',
                    'type_object' => 'notification',
                    'object_id' => $this->id,
                ]
            );
        }
    }
}
