<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Platform extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $module_id = null;
    protected $fillable = [
        'name'
    ];
    public $fields = [
        'name'
    ];
    /**
     * Grid columns for the model
     *
     * @var array
     */
    public $gridColumns = [
        'id',
        'name'
    ];
    public $modelOptions = [
        'can_create' => true,
        'can_edit' => true,
        'can_delete' => true,
        'can_inactive' => false,
        'can_see' => true,
        'can_cancel' => true,
    ];
    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'platform';
    protected $dates = ['deleted_at'];
    /**
     * The validation rules associated with the model
     *
     * @var array
     */
    static $rules = [
        'name' => 'required'
    ];
    /**
     * Get the devices for the platform.
     */
    public function devices()
    {
        return $this->hasMany('App\Models\Device', 'platform_id');
    }
}
