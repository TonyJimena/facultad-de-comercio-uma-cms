<?php
/**
 * Trait for authored default behaviour
 *
 * @author fmgonzalez
 *
 */
namespace App\Models;

use Illuminate\Support\Facades\Auth;

/**
 * Trait for authored default behaviour
 */
trait TraitAuthored
{
    /**
     * The name of the "created by" column.
     *
     * @var string
     */
	static $createdByColumn = 'created_by';
    
    /**
     * The name of the "updated by" column.
     *
     * @var string
     */
    static $updatedByColumn = 'updated_by';
    
    /**
     * The default anonymous user id.
     *
     * @var integer
     */
    static $annonymousUserId = 0;
    
    /**
     * Indicates if the model is currently authored
     *
     * @var bool
     */
    static $authoredBehaviour = false;
    
    /**
     * Update the authored fields.
     *
     * @return void
     */
    public function updateAuthored()
    {
        $author = static::getCurrentUser();
    
        if (! $this->isDirty($this->getUpdatedByColumn())) {
            $this->setUpdatedBy($author);
        }
    
        if (! $this->exists && ! $this->isDirty($this->getCreatedByColumn())) {
            $this->setCreatedBy($author);
        }
    }
    
    /**
     * Set the value of the "created by" attribute.
     *
     * @param mixed $value
     * @return void
     */
    public function setCreatedBy($value)
    {
        $this->{$this->getCreatedByColumn()} = $value;
    }
    
    /**
     * Returns the current user's id
     *
     * @return integer
     */
    static function getCurrentUser(){
        $user = Auth::check() ? Auth::user()->getKey() : static::$annonymousUserId;
        return $user;
    }
    
    /**
     * Set the value of the "updated by" attribute.
     *
     * @param mixed $value
     * @return void
     */
    public function setUpdatedBy($value)
    {
        $this->{$this->getUpdatedByColumn()} = $value;
    }
        
    /**
     * Return the column name for created by
     *
     * @return string
     */
    protected function getCreatedByColumn()
    {        
        return property_exists(__CLASS__, 'createdByColumn') ? $this->createdByColumn : null;
    }
    
    /**
     * Return the column name for updated by
     *
     * @return string
     */
    protected function getUpdatedByColumn()
    {
        return property_exists(__CLASS__, 'updatedByColumn') ? $this->updatedByColumn : null;
    }    

    /**
     * Return if class is authored
     *
     * @return bool
     */
    static function isAuthoredBehaviour()
    {        
        return ( property_exists( __CLASS__ , 'authoredBehaviour' ) && static::$authoredBehaviour );
    }
}