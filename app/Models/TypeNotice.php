<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeNotice extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = 13;
	protected $fillable = [
		'name',
		'label',
		'description',
	];
	public $fields = [
		'name',
		'label',
		'description',
	];
	public $gridColumns = [
		'id',
		'name',
		'label',
		'description',
	];
	public $modelOptions = [
		'can_create' => true,
		'can_edit' => true,
		'can_delete' => true,
		'can_inactive' => false,
		'can_see' => true,
		'can_cancel' => true,
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'type_notice';
	protected $dates = ['deleted_at'];
	// /**
	//  * Get the companies for the sector.
	//  */
	// public function companies()
	// {
	// 	return $this->hasMany('App\Models\Company', 'sector_id');
	// }
	// /**
	//  * Get the inscription for the sector.
	//  */
	// public function inscription()
	// {
	// 	return $this->hasMany('App\Models\Inscription', 'sector_id');
	// }
}
