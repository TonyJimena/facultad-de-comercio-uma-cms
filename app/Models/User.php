<?php

namespace App\Models;

use App\Events\UserChanged;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Notifications\InscriptionAccepted;

use Carbon\Carbon;

class User extends BaseModel implements AuthenticatableContract, CanResetPasswordContract
{
	use HasApiTokens, Authenticatable, CanResetPassword, SoftDeletes, Notifiable;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = 3;
	public $datesToIso8601 = [
		'created_at',
		'updated_at',
		'deleted_at',
	];
	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
	];
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = [
        'created_at',
        'deleted_at',
        'id',
        'updated_at',
		'username',
		'password',
		'active',
	];

	public $fields = [
        'created_at',
        'deleted_at',
        'id',
        'updated_at',
		'username',
		'password',
		'active',
        'access_token',
        'user_data',
        'user_data_id'
	];
	public $gridColumns = [
		'id',
	];
	public $relationsGridColumns = [
		'name' => [
			'table' => 'user_data',
			'field' => 'name',
			'mul' => [
				'field' => 'user_id'
			]
		],
		'email' => [
			'table' => 'user_data',
			'field' => 'email',
			'mul' => [
				'field' => 'user_id'
			]
		]
	];
	public $filters = [
		'name' => [
			'table' => 'user_data',
			'field' => 'name',
			'type' => 'text',
			'relation' => 'fk',
			'mul' => [
				'field' => 'user_id',
			]
		],
		'email' => [
			'table' => 'user_data',
			'field' => 'email',
			'type' => 'text',
			'relation' => 'fk',
			'mul' => [
				'field' => 'user_data_id',
			]
		]
	];
	public $columnsApi = [
		'id',
		'renovation_date'
	];
	public $relationsApi = [
		'name' => [
			'table' => 'user_data',
			'field' => 'name',
			'mul' => [
				'field' => 'user_id'
			]
		],
		'company_name' => [
			'table' => 'company',
			'field' => 'name',
			'mul' => [
				'field' => 'user_id'
			]
		],
		'sector' => [
			'table' => 'sector',
			'field' => 'label',
			'fk' => [
				'table' => 'company',
				'field' => 'sector_id'
			]
		],
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'user';
	protected $dates = ['deleted_at'];
	static $rules = [
		'username' => 'required|email',
		'renovation_date' => 'required',
	];
	public $options = [
		'seo' => false,
		'can_create' => false,
		'can_edit' => true,
		'can_delete' => true,
		'can_inactive' => false,
		'can_see' => true,
		'can_cancel' => true,
		'can_ok' => false,
		'can_send' => false
	];
	/**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
		'saved' => UserChanged::class,
		'deleted' => UserChanged::class,
    ];
	/**
	 * Set the user's password.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setPasswordAttribute($value)
	{
		$this->attributes['password'] = Hash::make($value);
	}
	/**
	 * Get the model's renovation date.
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getRenovationDateAttribute($value)
	{
		if (request()->is('api/*')) return to_iso_8601_string($value);

		return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y');
	}
	/**
	 * Set the model's renovation date.
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function setRenovationDateAttribute($value)
	{
		$this->attributes['renovation_date'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
	}
	/**
     * Get the user data record associated with the user.
     */
    public function userData()
    {
        return $this->belongsTo('App\Models\UserData', 'user_data_id');
    }

	/**
	 * Get the notices associated with the user.
	 */
	public function notices()
	{
		return $this->belongsTo('App\Models\Notice', 'id');
	}
	/**
	 * Route notifications for the mail channel.
	 *
	 * @return string
	 */
	public function routeNotificationForMail()
	{
		return $this->username;
	}
	/**
	 * Send the password reset notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendPasswordResetNotification($token)
	{
		$this->notify(new ResetPasswordNotification($token));
	}

    /**
	 * Send email to set account
	 */
	public function notifyHasBeenAccepted()
	{
		$token = Str::random(64);
		DB::table('set_account_tokens')->insert([
			'email' => $this->username,
			'token' => $token
		]);

		$this->notify(new InscriptionAccepted($token));
	}
}
