<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserCmsProfile extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = null;
	protected $fillable = [
		'name',
		'label',
		'description',
		'role'
	];
	public $fields = [
		'name',
		'label',
		'description',
		'role'
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'user_cms_profile';
	protected $dates = ['deleted_at'];
	/**
	 * Get the users for the profile.
	 */
	public function users()
	{
		return $this->hasMany('App\Models\UserCms', 'user_cms_profile_id');
	}
}
