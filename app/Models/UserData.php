<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserData extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = null;
	public $datesToIso8601 = [
		'created_at',
		'updated_at',
		'deleted_at'
    ];
	protected $fillable = [
		'id',
		'user_id',
		'name',
        'surname',
		'email',
        'student_code',
        'student_degree',
        'student_degree_id',
        'created_at',
		'updated_at',
		'deleted_at'
	];
	public $fields = [
        'id',
		'user_id',
		'name',
        'surname',
		'email',
        'student_code',
        'student_degree',
        'student_degree_id',
        'created_at',
		'updated_at',
		'deleted_at'
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'user_data';
	protected $dates = ['deleted_at'];
	static $rules = [
		'name' => 'required',

		'email' => 'required|email',
	];
	static $rulesApi = [
		'name' => 'required',

		'email' => 'required|email',
	];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'user_data_id');
    }
}
