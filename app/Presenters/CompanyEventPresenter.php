<?php

namespace App\Presenters;

use App\Models\CompanyEvent;

/**
 * Actions Presenter layer class 
 *
 */
class CompanyEventPresenter extends AbstractPresenter
{
    public function state()
    {
        switch ($this->object->{__FUNCTION__}) {
            case 'Abierto':
                $label = '<span class="label label-success">' . $this->object->{__FUNCTION__} . '</span>';
                break;
            case 'Cancelado':
                $label = '<span class="label label-danger">' . $this->object->{__FUNCTION__} . '</span>';
                break;
            case 'Finalizado':
                $label = '<span class="label label-primary">' . $this->object->{__FUNCTION__} . '</span>';
                break;
            default:
                $label = '<span class="label label-default">' . $this->object->{__FUNCTION__} . '</span>';
        }

        return $label;
    }
    public function in_reserve()
    {
        $faClass = $this->object->{__FUNCTION__} ? 'fa-check text-success' : 'fa-times text-danger';
        return '<div class="text-center"><i class="fa ' . $faClass . '"></i></div>';
    }
}
