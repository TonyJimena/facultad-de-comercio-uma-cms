<?php

namespace App\Presenters;

use App\Models\Event;
use Illuminate\Support\Str;

/**
 * Actions Presenter layer class 
 *
 */
class EventPresenter extends AbstractPresenter
{
    public function event_state()
    {
        switch ($this->object->{__FUNCTION__}) {
            case 'Abierto':
                $label = '<span class="label label-success">' . $this->object->{__FUNCTION__} . '</span>';
                break;
            case 'Cancelado':
                $label = '<span class="label label-danger">' . $this->object->{__FUNCTION__} . '</span>';
                break;
            case 'Finalizado':
                $label = '<span class="label label-primary">' . $this->object->{__FUNCTION__} . '</span>';
                break;
            default:
                $label = '<span class="label label-default">' . $this->object->{__FUNCTION__} . '</span>';
        }

        return $label;
    }
    public function description()
    {
        return Str::limit($this->object->{__FUNCTION__}, 100);
    }
    public function image1()
    {
        $url = env('APP_URL') . '/uploads/event/' . $this->object->{__FUNCTION__};

        return '<img src="' . $url . '" alt="' . $this->object->title . '" width="200"/>';
    }
}
