<?php

namespace App\Presenters;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

/**
 * Actions Presenter layer class 
 *
 */
class ExcelPdfPresenter extends AbstractPresenter
{
    public function image1()
    {
        $url = env('APP_URL') . '/uploads/event/' . $this->object->{__FUNCTION__};

        return '<img src="' . $url . '" alt="' . $this->object->title . '" width="100"/>';
    }

    public function active() {
        return $this->object->{__FUNCTION__}? '1' : '0';
	}
	
	public function invalids() {
		return $this->object->{__FUNCTION__}? '1' : '0';
	}

	public function failed() {
		return $this->object->{__FUNCTION__}? '1' : '0';
	}

	public function sended() {
		return $this->object->{__FUNCTION__}? '1' : '0';
	}

	public function sent() {
		return $this->object->{__FUNCTION__}? '1' : '0';
	}

	public function recipients() {
		return $this->object->{__FUNCTION__}? '1' : '0';
	}

	public function is_dashboard() {
		return $this->object->{__FUNCTION__}? '1' : '0';
	}

	public function is_menu() {
		return $this->object->{__FUNCTION__}? '1' : '0';
	}

	public function cost() {
		return $this->object->{__FUNCTION__}? $this->object->{__FUNCTION__} : '0';
	}

	public function cost_per_km() {
		return $this->object->{__FUNCTION__}? $this->object->{__FUNCTION__} : '0';
	}

	public function coefficient() {
		return $this->object->{__FUNCTION__}? $this->object->{__FUNCTION__} : '0';
	}

	public function unsubscribe() {
		return $this->object->{__FUNCTION__}? '1' : '0';
	}
    
}