<?php

namespace App\Presenters;

use App\Models\Inscription;

/**
 * Actions Presenter layer class 
 *
 */
class InscriptionPresenter extends AbstractPresenter
{
    public function user_id()
    {
        $element = $this->object->{__FUNCTION__} ?
            '<div class="text-center"><i class="fa fa-check text-success"></i></div>' :
            '<a class="btn btn-sm bg-theme-primary" href="' . route('inscription.user_create', ['id' => $this->object->id]) . '">' . trans('inscription.create_user') . '</a>';
        return $element;
    }
}
