<?php

/**
 * Presentable interface 
 * 
 * @author fmgonzalez
 *
 */

namespace App\Presenters;

use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Presentable interface 
 */
interface Presentable
{

    /**
     * Return an instance of a Model wrapped in a presenter object
     *
     * @param Model $model
     * @param Presentable $presenter
     * @return Model
     */
    public function model(Model $model, Presentable $presenter);

    /**
     * Return an instance of a Collection with each value wrapped in a presenter 
     * object
     *
     * @param Collection $collection
     * @param Presentable $presenter
     * @return Collection
     */
    public function collection(Collection $collection, Presentable $presenter);

    /**
     * Return an instance of a Paginator with each value wrapped in a presenter 
     * object
     *
     * @param Paginator $paginator
     * @param Presentable $presenter
     * @return Paginator
     */
    public function paginator(Paginator $paginator, Presentable $presenter);

    /**
     * Get DataTypes
     *
     * @return multitype:
     */
    public function getDataTypes();

    /**
     * Set DataTypes
     * @param array $data
     */
    public function setDataTypes($data = array());
}
