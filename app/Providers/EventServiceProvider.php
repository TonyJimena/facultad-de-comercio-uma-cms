<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use App\Events\EventReserveConfirmed;
use App\Listeners\ConfirmEventReserveNotification;

use App\Events\EventCreated;
use App\Listeners\SendCreateEventNotification;

use App\Events\NotificationCreated;
use App\Listeners\SendNotification;

use App\Events\UserChanged;
use App\Listeners\ClearUsersCache;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        EventReserveConfirmed::class => [
            ConfirmEventReserveNotification::class
        ],
        EventCreated::class => [
            SendCreateEventNotification::class
        ],
        NotificationCreated::class => [
            SendNotification::class
        ],
        UserChanged::class => [
            ClearUsersCache::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
