<?php

namespace App\Services;

use Illuminate\Support\Arr;

use Symfony\Component\HttpClient\Psr18Client;
use Nyholm\Psr7\Factory\Psr17Factory;
use OneSignal\Config;
use OneSignal\OneSignal;
use OneSignal\Devices;
use OneSignal\Exception\OneSignalException;

use App\Models\Notification;
use App\Models\Device;
use App\Models\DeviceOneSignal;

use Log;

class OneSignalService
{

    const USER_SUBSCRIBED = 1;
    const USER_UNSUBSCRIBED = -2;

    private $applicationId;
    private $notifications;
    private $devices;

    public function __construct()
    {
        $this->applicationId = env('ONESIGNAL_APPLICATION_ID');

        $config = new Config(
            env('ONESIGNAL_APPLICATION_ID'),
            env('ONESIGNAL_APPLICATION_AUTH_KEY'),
            env('ONESIGNAL_USER_AUTH_KEY')
        );
        $httpClient = new Psr18Client();
        $requestFactory = $streamFactory = new Psr17Factory();

        $api = new OneSignal($config, $httpClient, $requestFactory, $streamFactory);
        $this->notifications = $api->notifications();
        $this->devices = $api->devices();
    }
    /**
     * Send notification to devices
     * 
     * @param Collection $devices
     * @param Notification $notification
     * @param Array $data
     * @return Array
     */
    public function sendNotifications($devices, Notification $notification = null, $data)
    {
        $results = [];

        if ($devices) {
            foreach ($devices as $device) {
                $results[] = $this->sendNotification($device, $notification, $data);
            }
        }

        return $results;
    }
    /**
     * Add notification to OneSignal
     * 
     * @param Device $device
     * @param Notification $notification
     * @param Array $data
     * @return Array
     */
    public function sendNotification(Device $device, Notification $notification = null, $data)
    {
        $description = $notification ? $notification->description : Arr::pull($data, 'description');
        $title = $notification ? $notification->title : Arr::pull($data, 'title');
        $description = substr(strip_tags($description), 0, 200);
        $title = substr(strip_tags($title), 0, 200);

        if ($device->player_id) {
            try {
                $dt =
                    [
                        'include_player_ids' => [$device->player_id],
                        'contents' => [
                            'en' => $description,
                            'es' => $description
                        ],
                        'headings' => [
                            'en' => $title,
                            'es' => $title
                        ],
                        'content_available' => true,
                        'data' => $data,
                        'ios_badgeType' => 'Increase',
                        'ios_badgeCount' => 1
                    ];
                $result = $this->notifications->add($dt);
            } catch (OneSignalException $e) {
                $result = [
                    'errors' => [
                        $e->getMessage()
                    ],
                    'recipients' => 0
                ];
            }
        } else {
            $result = [
                'errors' => [
                    __('one_signal.no_player_id')
                ],
                'recipients' => 0
            ];
        }

        return $result;
    }
    /**
     * Return device type
     * 
     * @param Device $device
     * @return Int
     */
    private function getDeviceType(Device $device)
    {
        $platform = $device->platform;

        return $platform->id == 2 ? Devices::IOS : Devices::ANDROID;
    }
    /**
     * Return test type for One Signal
     * 
     * @return Int
     */
    private function getTestType()
    {
        return env('APP_ENV') == 'local' || env('APP_ENV') == 'dev' || env('APP_ENV') == 'preproduction' ? 1 : 2;
    }
    /**
     * Return segments 
     * 
     * @return Int
     */
    private function getSegments()
    {
        return ['environment' => env('APP_ENV')];
    }
    /**
     * Create a device in One Signal
     * 
     * @param Device $device
     */
    public function createDevice(Device $device)
    {
        try {
            $newDevice = $this->devices->add([
                'app_id' => $this->applicationId,
                'device_type' => $this->getDeviceType($device),
                'test_type' => $this->getTestType(),
                'identifier' => $device->push_token,
                'language' => $device->language,
                'game_version' => $device->app_version,
                'device_model' => $device->model_device,
                'device_os' => $device->os_version,
                'notification_types' => self::USER_SUBSCRIBED,
                'tags' => $this->getSegments()
            ]);

            if ($newDevice['success']) {
                $deviceOneSignalData = [
                    'type' => 'success',
                ];
                $device->player_id = $newDevice['id'];
                $device->save();
            } else {
                $deviceOneSignalData = [
                    'type' => 'error',
                    'message_error' => 'OneSignal ha devuelto el campo success con valor false. Error de API OneSignal.',
                ];
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $deviceOneSignalData = [
                'type' => 'error',
                'message_error' => substr($e->getMessage(), 0, 300),
            ];
        }

        $deviceOneSignalData['body'] = json_encode($device->toArray());
        $deviceOneSignalData['function'] = get_class($this) . '"\"' . __FUNCTION__;

        $deviceOneSignal = DeviceOneSignal::create($deviceOneSignalData);
        $deviceOneSignal->device()->associate($device);
        $deviceOneSignal->save();
    }
    /**
     * Update a device in One Signal
     * 
     * @param Device $device
     */
    public function updateDevice(Device $device)
    {
        try {
            $playerId = $device->player_id;

            $this->devices->update($playerId, [
                'app_id' => $this->applicationId,
                'identifier' => $device->push_token ?: '',
                'notification_types' => self::USER_SUBSCRIBED
            ]);
            $deviceOneSignalData = [
                'type' => 'success',
            ];
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $deviceOneSignalData = [
                'type' => 'error',
                'message_error' => substr($e->getMessage(), 0, 600),
            ];
        }

        $deviceOneSignalData['body'] = json_encode($device->toArray());
        $deviceOneSignalData['function'] = get_class($this) . '"\"' . __FUNCTION__;

        $deviceOneSignal = DeviceOneSignal::create($deviceOneSignalData);
        $deviceOneSignal->device()->associate($device);
        $deviceOneSignal->save();
    }
}
