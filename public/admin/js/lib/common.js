var oTable = [];
currentAjax = null;
xhrWorking = false;
var searching = false;

function underscoreToCamelcase(string) {
    return string.replace(/(\_\w)/g, function(m){return m[1].toUpperCase();});
};

$(document).ready(function(){
	$( document ).ajaxSend(function( event, request, settings ) {
		if (settings.url.match(/^\//)) {
	 		settings.url = BASE_URL + settings.url;
		}
	});

	$('li[data-module="'+ js_module +'"]').addClass('active');
	$('select').select2({
		minimumResultsForSearch: 10,
		placeholder: 'Elije una opción'
	});

	$("#model-edit-form").on('submit', function(e) {
		serializeMetaData($(this));
	
		return true;
	});
	
	$('.gridSearchFilter').bind('change', function(){
		if (! $(this).hasClass('searchDateInput')) {
			searching = true;
			$('table[id='+$(this).attr('rel')+']').dataTable().fnDraw();
		}
	});

	$('.gridSearchFilter.searchDateInput').bind('dp.hide', function(){
		searching = true;
		$('table[id='+$(this).attr('rel')+']').dataTable().fnDraw();
	});

	$('.operators-list li').click(function(event) {
		let triggerChange = true;
		const element = $(this);
		const fsearch = element.attr('fsearch');
		const title = element.attr('title');
		const text = element.text();
		
		const field = element.parent().data('field');
		$('input[name=' + field + '_operator]').val(text);

		const parentDiv = element.parent().closest('div');
		const spanElement = parentDiv.find( "span" );
		spanElement.text(text);
		spanElement.attr('fsearch', fsearch);
		spanElement.attr('title', title);

		if ( fsearch == 'between') {
			$('input[name="'+ field +'_max"]').css('display', '');
			$('input[name="'+ field +'_max"]').val($('input[name="'+ field +'"]').val());
		}else if(field.indexOf('date')){
			$('input[name="'+ field +'_max"]').css('display', 'none');
		}

		$('.gridSearchFilter').trigger('change');
	});

	$('table.dataTable').each(function(index, value) {
		var id = $(this).attr('id');
		if ($('#'+id+'Search .customSearchAdvanced').is(':empty')) {
			$('#'+id+'Search .btn-advanced-search').addClass('hide');
			$('#'+id+'Search .customSearchAdvanced').addClass('hide');
		} else {
			// Check some with data
			var advancedSeachSetted = false;
			$('#'+id+'Search .customSearchAdvanced .gridSearchFilter').each(function(){
				if (!advancedSeachSetted){
					if ($(this).is(':checkbox')) {
						if (! $(this).is(':checked')) {
							advancedSeachSetted = true;
						}
					} else {
						if ($(this).val() != '') {
							advancedSeachSetted = true;
						}
					}
				}
			});

			/*if (!advancedSeachSetted){
				$('#'+id+'Search .customSearchAdvanced').addClass('hyde');
			}*/
		}

		$('#'+id+'Search .btn-advanced-search').click(function(){
			id = $(this).attr('rel');
			if (id){
				$('#'+id+'Search .customSearchAdvanced').toggle('blind',500);
			}
		});
	});


	//	$('html').on('keyup','.gridSearchFilter', function(){
//		$('table[id='+$(this).attr('rel')+']').dataTable().fnDraw();
//	});


	//Init jquery Date Picker
//	$('.datepicker').blur( function () {
//		$('.datepicker').change();
//        $('.datepicker.dropdown-menu').hide(0);
//	});

	handlerDatepickers();

	selectMultipleHandler();

	autocompleteHandler();

	$("#owner_type").change(function(){
		$( "div[id^='owner_type_']" ).hide();
		// $( '#' + '^owner_type_' ).hide() ;
		$( '#' + 'owner_type_' + $(this).val() ).show();
		new_owner_id = $( '#' + 'owner_type_' + $(this).val() + '_id' ).val();
		$( '#' + 'owner_id' ).val(new_owner_id);
	});

	$( "select[id^='owner_type_id_']" ).change( function() {
		$( 'input[name=owner_id]' ).val( $(this).val() );
	});

	allGrid();

	/* handlerOptions(); */

    $('.styled').uniform();

    

	$( "body" ).on('click touchend','.editRow', function() {
		related = $(this).attr('related');

		if (related){
			if (xhrWorking) {
				currentAjax.abort();
			}
			url = $(this).attr('rel');
			/*
			currentAjax = $.ajax({
				url: 		url,
				type:		'get',
				dataType:	'json',
				data:		{'_token' : $("input[name='_token']").val()},
				beforeSend	:	function (jqXHR, settings){
					jQuery('#formModal').html('');
					showLoading("Cargando...");
				},
				success	:function (data, textStatus, jqXHR) {
					jQuery('#formModal').html(data.view);
					handlerDatepickers();
					autocompleteHandler();
					hideLoading();
					initImageFields();
				},
				error :function( jqXHR, textStatus, errorThrown ){
					hideLoading();
					showMessage('Error', textStatus + ': '  + errorThrown, 0);
				}
			});*/
			window.open(url, '_blank');
		}else{
			document.location=$(this).attr('rel');
		}
	});

	$( "body" ).on('click touchend','.deleteRow, .deleteRowContact', function() {
		var id = $(this).parents('tr').attr('rel');
		var module = $(this).parents('table').attr('id').replace('Grid','');
		$('.delRow').attr("rel",id);
		$('.delRow').attr("module",module);
	});

	$( "body" ).on('click touchend','.addRelated', function() {
		var action = $(this).attr('action');
		// var module = $(this).attr('module');

		$.ajax({
			url: 		action,
			type:		'get',
			dataType:	'json',
			data:		{'_token' : $("input[name='_token']").val()},
			beforeSend	:	function (jqXHR, settings){
				// alert('Buscando');
				jQuery('#formModal').html('Cargando');
				showLoading("Cargando");
			},
			success	:function (data, textStatus, jqXHR) {
				jQuery('#formModal').html(data.view);
				handlerDatepickers();
				setTimeout(function() {
					autocompleteHandler();
				}, 100);
				hideLoading();
			},
			error :function( jqXHR, textStatus, errorThrown ){
				hideLoading();
				showMessage('Error', textStatus + ': '  + errorThrown, 0);
			}
		});

	});

	$( "body" ).on('click touchend','.saveRelated', function() {
		var relatedModule = $(this).attr('module');
		var form = $(this).parents('form');
		var form_method = $('#'+	form.attr('id')+' input[name=_method]').val();
		if (!form_method) form_method = 'POST';
		if(!$('#related_field').val())
			related_field=js_module+'_id';
		else
			related_field=$('#related_field').val();

		addInputHidden(form, related_field , $('#'+related_field).val());
		serializeMetaData(form);
		/*addInputHidden(form, 'related_id', $('#'+js_module+'_id').val());
		addInputHidden(form, 'related_type', ''+ underscoreToCamelcase('_' + js_module));*/

		$.ajax({
			url: 		form.attr('action'),
			type:		form_method,
			dataType:	'json',
			data:		form.serialize(),

			beforeSend	:	function (jqXHR, settings){
				// alert('Buscando');
				// jQuery('#formModal').html('Cargando');
				showLoading("Cargando");
			},
			success	:function (data, textStatus, jqXHR) {
				if (data.success){
					showMessage('Éxito', data.message, 5000);
					oTable = $('#'+relatedModule+'Grid').dataTable();
					dataTableReload(oTable);
					$('#formModal').html('');
					$('#formModal').modal('hide');
				}else{
					showErrors(form, data.errors);
					showMessage('Error', data.message, 0);
				}
				hideLoading();
			},
			error :function( jqXHR, textStatus, errorThrown ){
				hideLoading();
				showMessage('Error', textStatus + ': '  + errorThrown, 0);
			}
		});

	});

	$( "body" ).on('click touchend','.saveRelatedWithFiles', function() {
		var relatedModule = $(this).attr('module');
		var form = $(this).parents('form');
		var form_method = $('#'+	form.attr('id')+' input[name=_method]').val();
		if (!form_method) form_method = 'POST';
		addInputHidden(form, js_module+'_id', $('#'+js_module+'_id').val());
		addInputHidden(form, 'related_id', $('#'+js_module+'_id').val());
		addInputHidden(form, 'related_type', ''+ underscoreToCamelcase('_'+js_module));
		serializeMetaData(form);
		var formData = new FormData($(this).closest( 'form' )[0]);
		//console.log( formData );

		$.ajax({
			url: 		form.attr('action'),
			type:		form_method,
			dataType:	'json',
			data:		formData,

			//Options to tell jQuery not to process data or worry about content-type.
			async: false,
		    cache: false,
		    contentType: false,
		    processData: false,

			beforeSend	:	function (jqXHR, settings){
				// alert('Buscando');
				// jQuery('#formModal').html('Cargando');
				showLoading("Cargando");
			},
			success	:function (data, textStatus, jqXHR) {
				if (data.success){
					showMessage('Éxito', data.message, 5000);
					oTable = $('#'+relatedModule+'Grid').dataTable();
					dataTableReload(oTable);
					$('#formModal').html('');
					$('#formModal').modal('hide');
				}else{
					showErrors(form, data.errors);
					showMessage('Error', data.message, 0);
				}
				hideLoading();
			},
			error :function( jqXHR, textStatus, errorThrown ){
				hideLoading();
				showMessage('Error', textStatus + ': '  + errorThrown, 0);
			}
		});

	});

	$( "body" ).on('click touchend','.delRow', function() {
		var action = $(this).attr('action');
		var successAction = $(this).data('success-action');
		var module = $(this).attr('module');
		url = action.replace(/\/i($|\/)/g, '/' + $(this).attr('rel') + '/').replace(js_module, module);

		if (url.endsWith('/')) {
			url = url.substr(0, (url.length - 1));
		}

		var _this = $(this);

		$.ajax({
			url: 		url,
			type:		'post',
			dataType:	'json',
			data:		{'_token' : $("input[name='_token']").val(), '_method' : 'delete'},
			beforeSend	:	function (jqXHR, settings){
				// alert('Buscando');
				showLoading("Borrando");
			},
			success	:function (data, textStatus, jqXHR) {
				if (data.success)
				{
					showMessage('Éxito', data.message, 5000);

					oTable = $('#'+module+'Grid').dataTable();
					dataTableReload(oTable);

					if (_this.closest('#alertModalContact').size()) {
						if ($('#chatsGrid').size() == 1) {
							dataTableReload($('#chatsGrid').dataTable());
						}
						if ($('#actionsGrid').size() == 1) {
							dataTableReload($('#actionsGrid').dataTable());
						}
					}
				}
				else
				{
					showMessage('Error', data.message, 0);
				}
				hideLoading();
			},
			error :function( jqXHR, textStatus, errorThrown ){
				hideLoading();
				showMessage('Error', textStatus + ': '  + errorThrown, 0);
			}
		});

	});

	$( "body" ).on('click touchend','.module', function() {
		window.location = $(this).attr('rel');
	});

	// Funcionamiento del radio button
	$('div.radio-group div.radio').click(function() {
		alert($(this).parents('div.radio-group').attr('id'));
		$(this).parents('div.radio-group').children('div.radio > span').removeClass('checked');
		$(this).parents('div.radio-group').children('div.radio > span > input').removeAttr('checked');
		$(this).children('span').addClass('checked');
		$(this).children('span input').attr('checked','true');
	});

	$( 'label.required' ).append( '<span>*</span>' );

	// Show accordions which contain errors
	$('label.error').closest('.panel-collapse.collapse').parent().find('.collapsed').removeClass('collapsed');
	$('label.error').closest('.panel-collapse.collapse').collapse('show');

	if($('#meta_data').length>0)
		setMetaDataFields();

	$(document).on('click','[data-bindaction="collapse"]',function(e){
		e.stopPropagation();
		$(this).find('.heading-elements').find('[data-action="collapse"]')[0].click();
	});

	$('#btnInactivate').click(function(e) {
		e.preventDefault();
		inactivateItem();
		return false;
	});
	$('#btnActivate').click(function(e) {
		e.preventDefault();
		activateItem();
		return false;
	});
});


function allGrid(){
	$(".dataTable").each(function(){
		grid(this);
	});
}

function grid(obj){

	related = $(obj).attr('related');
	
	module = '';
	if (related){
        relatedField = related;
		relatedValue = $('#'+relatedField).val();
		module = $(obj).attr('id').replace('Grid','');

		$( obj ).closest( '.panel-body' ).css( 'padding', '0px 15px' );

	} else {
		module = js_module;
	}
	ajaxUrl =  BASE_URL+'/'+module+'/grid';
	editUrl = ajaxUrl.substring(0,ajaxUrl.lastIndexOf('/')+1);
/*
	console.log ($(obj).attr('related'));
	console.log ($(obj).attr('id'));
	console.log (ajaxUrl);
	if (related){
		console.log (relatedField);
		console.log (relatedValue);
	}
*/

	oTable[module] = $(obj).dataTable({
		"sDom": '<"datatable-header"i><"datatable-scroll"t><"datatable-footer length-left"lp>',
		"autoWidth": false,
		"oLanguage" : {"sUrl" : BASE_URL+"/js/locale/"+js_lang+".lang" },
		"iDisplayLength": 25,
		"aLengthMenu": [[ 25, 50, -1], [ 25, 50, "Todos"]],
		"bStateSave": true,
		"bPaginate": $(obj).data('paginate') === false ? false : true,
		"bServerSide": true,
		"sAjaxSource": ajaxUrl,
		"sServerMethod": 'POST',
		"type": 'POST',
		"responsive": true,
        "aoColumnDefs": [
            {
                className: 'control',
                orderable: false,
                targets:   0,
                //"sClass": "hide_me"
            },
            { "sClass": "hide_me", "aTargets": [ 0 ] }
        ],
		"fnServerParams": function ( aoData ) {
			// aoData.push( { "name": "columns", "value": (related)?eval('js_'+module+'_columns'):js_columns } );
			aoData.push( { "name": "columns", "value": $(obj).attr('data-colums') } );

			/*if (related){
				aoData.push( { "name": relatedField, "value": relatedValue } );
				aoData.push( { "name": 'related_type', "value": ''+ underscoreToCamelcase('_'+$(obj).attr('related')) } );
				aoData.push( { "name": 'related_id', "value": relatedValue } );
			}*/
			if (related){
				aoData.push( { "name": 'related_type', "value": relatedField } );
				aoData.push( { "name": 'related_id', "value": relatedValue } );
			}
			$('#'+$(obj).attr('id')+'Search .gridSearchFilter').each(function(index, value) {
				if ($(this).is(':checkbox')) {
					aoData.push( { name : $(this).attr('id'), value :  ($(this).is(':checked') ? 'yes' : 'no') } );
				} else {
					if ($(this).val() != '') {
						if( !$( this ).hasClass( 'skipNormalBehaviourSearch' ) ){
							aoData.push( { name : $(this).attr('id'), value :  $(this).val() } );

							if( $( this ).hasClass( 'searchWithOperator' ) ) {
								var field = $(this).data('field');

								if(typeof field == 'undefined') {
									field = $(this).attr('id');
								}
								
								aoData.push( { 	name : $(this).attr('id') + '_operator',
												value :  $('#filter_' + field).find('.opSign').attr('fsearch')
											} );

								aoData.push({
									name: $(this).attr('id') + '_field',
									value: field
								});
							}

							if( $( this ).hasClass( 'date' ) )
								aoData.push( { 	name : $(this).attr('id') + '_date',
												value :  $(this).val()
											} );

							if( $( this ).hasClass( 'boolean' ) )
								aoData.push( { 	name : $(this).attr('id') + '_boolean',
												value :  $(this).val()
											} );
						}
						else{
							var callback = $( this ).data( 'callback' );
							var $value = window[ callback ]( this );

							if( $value != -1 ){
								aoData.push( { name : $(this).data( 'field' ), value :  $value } );
								aoData.push( { name : $(this).data( 'field' ) + '_operator', value :  $(this).next( '.opSign' ).text() } );
							}
						}
					}
				}
        	});
			var query = $.param(aoData);
			// boton exportar
			setSearchToExport($(obj).attr('data-module') + 'btnExportar', query);
			if($('#' + $(obj).attr('data-module') + 'btnExportarEntryExitStart').length) {
				setSearchToExport($(obj).attr('data-module') + 'btnExportarEntryExitStart', query);
			}
			// boton pdf
			setSearchToExport($(obj).attr('data-module') + 'btnPdf', query);

		},
		"fnStateSaveParams": function (oSettings, oData) {
			$('#'+$(obj).attr('id')+'Search .gridSearchFilter').each(function(index, value) {
				switch($(obj).attr('id')) {
					case 'facturasGrid':
						switch($(this).attr('id')) {
							case 'year':
								if ($(this).val() == '')
									$(this).val(new Date().getFullYear());
								break;
							default:
								break;
						}
						break;
					default:
						break;
				}

				eval('oData.'+$(this).attr('id')+' = "' + $(this).val() + '"' );

			});
		},
		"fnStateLoadParams": function (oSettings, oData) {
			var showAdvanced = false,
				noLoadSearchItems = $('#noLoadSearchItems').val();

			if (noLoadSearchItems !== '1') {
				$('#'+$(obj).attr('id')+'Search .gridSearchFilter').each(function(index, value) {
					var id = $(this).attr('id');
					if (eval('oData.'+ id + ' != "" ') ){
						var value = eval('oData.'+ id );
						$(this).val(value);
						showAdvanced = true;
					} else {
						$(this).val('');
					}
				});
				if (showAdvanced) {
					//$('#'+ $(obj).attr('id') +'Search .customSearchAdvanced').toggle('blind',500);
				}
			}
		},
		"fnPreDrawCallback": function( oSettings ) {
			id = $(obj).attr('id');
			if ( ! $('#'+id+'_wrapper .dataTables_filter .customSearchAdvanced').length ) {
	        	$('#'+$(obj).attr('id')+'_wrapper .dataTables_filter').html('');
			}
		},
        "fnDrawCallback": function( oSettings ) {
			$(obj).trigger("myLoadedDrawEvent");
            if ($(".dataTable thead tr th:last-child").text()!="")
                $(".dataTable thead tr").append('<th style="width:110px" class="text-center"><i class="fa fa-gear"></i></th>');
            handlerOptions(obj);
        },
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var html = '';
			var canDelete = $(obj).data('delete');
			var canEdit = $(obj).data('edit');
			var canSee = $(obj).data('see');
			
			if ($(obj).attr('related')) {
				if(canEdit){
					html = '<button class="pull-left btn bg-theme-secondary btn-icon editRow" data-toggle="modal" data-target="#formModal" related="'+js_module+'" rel="'+(BASE_URL+'/'+$(obj).attr('id').replace('Grid','')+'/')+aData[0]+'/edit/?related='+$(obj).attr('related')+'" type="button"><span class="fa fa-pencil"></span></button>';
				}else if(canSee){
					html = '<a class="pull-left btn bg-theme-secondary" data-toggle="modal" related="'+js_module+'" href="'+(BASE_URL+'/'+$(obj).attr('id').replace('Grid','')+'/')+aData[0]+'/edit/" ><span class="fa fa-eye"></span></a>';
				}
				if ($(obj).closest('.dataTable').attr('id') == 'contactsGrid') {
					html += '<button class="pull-left btn bg-danger btn-icon deleteRowContact" data-target="#alertModalContact" data-toggle="modal" type="button"><span class="fa fa-trash-o"></span></button>';
				} else {
					html += '<button class="pull-left btn bg-danger btn-icon deleteRow" data-target="#alertModal" data-toggle="modal" type="button"><span class="fa fa-trash-o"></span></button>';
				}
			} else {
				if(canEdit){
					html = '<button class="btn bg-theme-secondary btn-icon editRow" rel="'+(BASE_URL+'/'+$(obj).attr('id').replace('Grid','')+'/')+aData[0]+'/edit'+'" type="button"><span class="fa fa-pencil"></span></button>';
				}else if(canSee){
					html = '<a class="pull-left btn bg-theme-secondary" data-toggle="modal" related="'+js_module+'" href="'+(BASE_URL+'/'+$(obj).attr('id').replace('Grid','')+'/')+aData[0]+'/edit/" ><span class="fa fa-eye"></span></a>';
				}
				if(canDelete){
					html += '<button class="btn btn-danger btn-icon deleteRow" data-target="#alertModal" data-toggle="modal" type="button"><span class="fa fa-trash-o"></span></button>';
				}
			}
			$(nRow).append('<td>' + html + '</td>');
            $(nRow).attr('rel', aData[0]);
            //Añadiendo plugin personal para llamarlo según el módulo.
            $(obj).trigger("myLoadedRowEvent", {nRow: nRow, data: aData});

            $('.dataTables_length select').select2({
		        minimumResultsForSearch: Infinity,
		        width: 'auto'
		    });

            return nRow;
        },
        //"aoColumnDefs": [ { "sClass": "hide_me", "aTargets": [ 0 ] } ], // first column in visible columns array gets class "hide_me"
	});
/*
	setTimeout(function() {
		console.log('quitar');
		$('#'+$(obj).attr('id')+'_wrapper .dataTables_filter').append('<input id="code" type="text" class="customSearchFilter" value="30087" />');
	}, 1000);
*/
	$('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
}

function setSearchToExport(id, query) {
	if ($('#' + id).size() == 1) {
	    var dataUrl = $('#' + id).attr('data-url');
	    if (dataUrl === undefined) {
	    	var url = $('#' + id).attr('href');
	    	$('#' + id).attr('data-url', url);
	    	dataUrl = url;
	    }
	    $('#' + id).attr('href', dataUrl + '?' + query);
	}
}

function handleAdvancedSearch(obj){
	id = $(obj).attr('id');
	capa = $('div[id='+id+']');
	console.log(capa);
	if ($(capa).hasClass('hide')){
		$(capa).removeClass('hide');
	}
}

function gridHandlers(){
	/*$('.dataTables_filter .gridSearchFilter').each(function(index, value) {
		console.log($(this));
		$(this).change( function(){
			console.log($(this).val());
			$(this).parent('.dataTables_wrapper').child('table .dataTable').fnDraw();
		});
	});*/
}


/** =========================================
 *  Funciones auxiliares
 * ============================================
 */

/**
 * Añade un input para los campos maestro-detalle
 */
function addInputHidden(form, input_name, value){
	console.log(form,input_name,value);
	related_input = $('input[name='+input_name+']',form);
	if (typeof related_input){
		var input = document.createElement('input');
		input.type = 'hidden';
		input.name = input_name;
		input.value = value;
		form.prepend(input);
	}
}

/**
 * Recarga una dataTable
 *
 * @param oTable dataTable a recargar
 */
function dataTableReload(oTable){
	var oSettings = oTable.fnSettings();
	var current_page = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1 ;
	var max_page = Math.ceil((oSettings.fnRecordsDisplay()-1) / oSettings._iDisplayLength);
	if (current_page > max_page){
		oTable.fnPageChange(max_page-1);
	}else {
		oTable.fnPageChange(current_page-1);
	}
}

/**
 * datepickers handler
 */
function handlerDatepickers() {
	/*$('.datepicker').datetimepicker({
		locale: 'es',
		format: 'DD/MM/YYYY'
	});

	$('.datetimepicker').datetimepicker({
		locale: 'es',
		format: 'DD/MM/YYYY HH:mm:ss'
	});

	$('.datepickerGrid').datetimepicker({
		locale: 'es',
		format:'DD/MM/YYYY',
		useCurrent: false,
	});*/

}


/**
 * Muestra los errores en el formulario
 *
 * @param form
 * @param errors
 */
function showErrors(form, errors){
	$('#'+form.attr('id')+' label[class=error]').remove();
	$.each(errors, function(index, value){
		var parent_div = $('#'+form.attr('id')).find('[name='+index+']').parents('div[class*=col-sm-]');
		var label = parent_div.children('label[class=error]');
		if (typeof label){
			parent_div.append('<label for="'+index+'" class="error">'+value+'</label>');
		}else{
			label.html(value);
		}

		/* alert(index +': ' +value); */
	});
}

/**
 * ------------------- Desechar desde aqui ------------------
 */

function search(){

	xhrWorking = false;
	currentSearch = '';
	currentAjax = null;

	jQuery('input.auto-search').keyup(function(){

		var url    = jQuery(this).attr('action');
		// var params = jQuery('input.auto-search').serialize();

		console.log(jQuery(this).val());
		minlength = 0;

		if (jQuery(this).val().length >= minlength){
			// alert ('> 3 ' +  jQuery(this).val())
			if (jQuery(this).val() != currentSearch) {

				if (xhrWorking) {
					currentAjax.abort();
				}

				currentSearch = jQuery(this).val();

				currentAjax = jQuery.ajax({
					url: 		url,
					type:		'get',
					dataType:	'json',
					data:		{'name' : jQuery(this).val() },
					beforeSend	:	function (jqXHR, settings){
						// alert('Buscando');
						xhrWorking = true;
						showLoading("Buscando");
					},
					success	:function (data, textStatus, jqXHR) {
						xhrWorking = false ;
						hideLoading();
						showMessage('Éxito', data.message, 2000);
						jQuery('div[class*=list]').html(data.view);
					},
					error :function( jqXHR, textStatus, errorThrown ){
						xhrWorking = false;
						hideLoading();
						if (errorThrown != 'abort'){
						    var contentType = jqXHR.getResponseHeader("Content-Type");
						    if (contentType.toLowerCase().indexOf("text/html") >= 0){
						    	// asume that the login has expired - reload the current page
						    	window.location.reload();
						    }else {
							    showMessage('Error', textStatus + ': '  + errorThrown, 0);
						    }
						}
					}
				});
			}
		}
	});

}

function showLoading(text)
{
	if (jQuery('#cargando_div'))
	{
		//parent.parent.gb_changecolor('#599401');

		jQuery('#cargando_texto').html(text);
		document.body.className = 'loadin';//
		jQuery('html').css('height','100%'); //
		jQuery('html').css('overflow', 'hidden'); //
	}
}

function hideLoading()
{
	if (jQuery('#cargando_div'))
	{
		//parent.parent.gb_changecolor('#000000');

		jQuery('#cargando_texto').html('');
		document.body.className = '';
		jQuery('html').css('height','auto'); //
		jQuery('html').css('overflow', 'auto'); //
	}
}

function showMessage(level, message, time, classes)
{
	if (level == 'info' || level == '')
	{
		level = 'notice';
	}
	console.log(classes);

    new PNotify({
        // (string | mandatory) the heading of the notification
        title: level,
        // (string | mandatory) the text inside the notification
        text: message,
        addclass: classes,
        // (string | optional) the image to display on the left
        // image: 'http://a0.twimg.com/profile_images/59268975/jquery_avatar_bigger.png',
        // (bool | optional) if you want it to fade out on its own or just sit there
//        sticky: (level == 'error'),
		time: time
    });

    // console.log((level == 'error'));
    return false;

}

/** =======================================================
 *  Grid Actions
 * ===================================================== */

$(document).ready(function() {
	$('body').on('myLoadedRowEvent', function(ev, options) {
		var nRow = options.nRow;

		$(nRow).find('td').dblclick(function(ev) {
			$(this).parent().find('td:last').find('button.editRow').trigger('click');
		});
	});

	disableContextMenu();
});

function disableContextMenu() {
	$('.dataTable').contextmenu(function(ev) {
		var tag = $(ev.target).prop('tagName');

		if(tag == 'TD') {
			$(ev.target).parent().find('td:last').find('button.editRow').trigger('click');
			return false;
		}
	});
}

function handlerDelete(){

	jQuery('a.action-delete').click(function(event){
		event.preventDefault();
		if (confirm($(this).attr("confirm-msg"))) {
			$.ajax({
				url: 		$(this).attr("href"),
				type:		'delete',
				dataType:	'json',
				data:		{'_token' : $(this).attr("data")},
				beforeSend	:	function (jqXHR, settings){
					// alert('Buscando');
					showLoading("Borrando");
				},
				success	:function (data, textStatus, jqXHR) {
					console.log(jqXHR.status);
					hideLoading();
					if (data.success)
					{
						$('a.action-delete[href="'+this.url+'"]').closest('tr').remove();
						showMessage('Éxito', data.message, 5000);
					}
					else
					{
						showMessage('Error', data.message, 0);
					}
				},
				error :function( jqXHR, textStatus, errorThrown ){
					hideLoading();
					showMessage('Error', textStatus + ': '  + errorThrown, 0);
				}
			});

		}
	});
}

function handlerOptions(dataTable){

	/*jQuery('#'+dataTable.id+' th > i.fa-gear').unbind('click').bind('click', function(event){
		event.preventDefault();
		var parent_module = $(this).parents('table').attr('id').replace('Grid','');
		url = BASE_URL + '/users/profile/'+parent_module;
		currentAjax = $.ajax({
			url: 		url,
			type:		'get',
			dataType:	'json',
			data:		{'module' : parent_module,
						 '_token' : $("input[name='_token']").val()
						},
						},
			beforeSend	:	function (jqXHR, settings){
				jQuery('#formModal').html('');
				showLoading("Cargando...");
			},
			success	:function (data, textStatus, jqXHR) {
				jQuery('#formModal').html(data.view);
				hideLoading();
			},
			error :function( jqXHR, textStatus, errorThrown ){
				hideLoading();
				showMessage('error', textStatus + ': '  + errorThrown, 0);
			}
		});

	});*/
}

function selectMultipleHandler(){

	/*$('select[multiple].selectMultiple').multiselect({
        columns: 2,
        placeholder: 'Select options',
        search: true,
        selectAll: true,
        selectGroup: true,
        showChecBox: true

    });*/

}

function autocompleteHandler(){

	/*$( 'select.chosen' ).chosen({

  	});*/

}

function toFloat(number)
{
    var numberFloat = number;

    if (isNaN(numberFloat)) {
        if (numberFloat.match(/\s+/)) {
            numberFloat = numberFloat.split(' ')[0];
        }

        if (numberFloat.match(/^(\-)?[0-9]+,[0-9]+\.[0-9]+$/)) {
            numberFloat = numberFloat.replace(/,/g, '');
        } else if (numberFloat.match(/^(\-)?([0-9]+\.)?[0-9]+,[0-9]+$/)) {
            numberFloat = numberFloat.replace(/\./g, '').replace(/\,/g, '.');
        }
    }

    return parseFloat(numberFloat);
}

function setMetaDataFields()
{
	console.log($('#meta_data').val());
	if($('#meta_data').val()!=''){
	    fields=JSON.parse($('#meta_data').val());
	    for (var i = 0; i < fields.length; i++) {
	    	
	    	fields[i];
	    }
    }
}

function serializeMetaData(form) {
	let elementsMeta = form.serializeArray().filter((element) => {
		return element.name.startsWith('meta_data_');
	});
	
	let tabs = [];
	
	form.find('[id^=tab_name_]').each(function() {
		tabs.push($(this).val());
	});

	let meta_data_lang = [];
	elementsMeta.forEach((value) => {
		let name = value.name.replace('meta_data_','');
		let post = name.lastIndexOf('_');
		let lang = name.substring(post + 1, name.length);
		let newName = name.substring(0, post);

		newValue = {
			field: newName,
			value: value.value
		};

		if (!meta_data_lang[lang]) meta_data_lang[lang] = [];
		meta_data_lang[lang].push(newValue);
	});

	let metaDataLang = [];
	for(let key in meta_data_lang){
		metaDataLang[key] = [];
		for(let t in tabs) {
			let object = {
				tab: tabs[t],
				content: []
			}
			for(let k in meta_data_lang[key]) {
				let value = meta_data_lang[key][k];
				let element = document.getElementById('meta_data_' + value.field + '_' + key);
				let tab = element.getAttribute('data-tab');
				if (object.tab == tab) object.content.push(value);
			}
			metaDataLang[key].push(object);
		}
	}
	
	for(let key in metaDataLang) {
		let input = $("<input>")
			.attr("type", "hidden")
			.attr("name", "meta_data_lang_" + key)
			.val(JSON.stringify(metaDataLang[key]));
		form.append($(input));
	}
}

function inactivateItem() {
	var module = js_module;
	var id = $('input[name=id]').val();
	var ajaxUrl =  BASE_URL + '/' + module + '/' + id + '/inactivate';

	$.ajax({
		type: 'PUT',
		url: ajaxUrl,
		contentType: 'application/json'
	}).done(function (response) {
		if(response.success) {
			showMessage('Éxito', response.message, 5000);
			$('#btnInactivate').hide();
			$('#btnActivate').show();
			$('#status').text('Cerrada');
		}else{
			showMessage('Error', response.message, 0);
		}
	});
}

function activateItem() {
	var module = js_module;
	var id = $('input[name=id]').val();
	var ajaxUrl =  BASE_URL + '/' + module + '/' + id + '/activate';

	$.ajax({
		type: 'PUT',
		url: ajaxUrl,
		contentType: 'application/json'
	}).done(function (response) {
		if(response.success) {
			showMessage('Éxito', response.message, 5000);
			$('#btnActivate').hide();
			$('#btnInactivate').show();
			$('#status').text('Abierto');
		}else{
			showMessage('Error', response.message, 0);
		}
	});
}
