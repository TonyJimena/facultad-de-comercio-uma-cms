//  Author: Louis Holladay
//  Website: AdminDesigns.com
//  Last Updated: 01/01/14 
// 
//  This file is reserved for changes made by the user 
//  as it's often a good idea to seperate your work from 
//  the theme. It makes modifications, and future theme
//  updates much easier 
// 

//  Place custom styles below this line 
///////////////////////////////////////

$(document).ready(function(){
    
    var login_exists = $('#username').length;

    if( login_exists )
    {
        $('#username').focus();
    }

    /*$('.pickadate').pickadate({
        format: 'dd/mm/yyyy',
        selectYears: true,
        selectMonths: true,
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Cerrar',
    });*/

    $(document).on('click','[data-action="changelang"]',function(event) {
        event.stopPropagation();
        var lang=$(this).attr('data-lang');
        $('[data-action="changelang"]').each(function(){
            $(this).removeClass('active');
            if($(this).attr('data-lang')==lang)
                $(this).addClass('active');
                        
        });
        
        //$('.haslang').hide();
        //$('[data-locale="'+lang+'"]').show();
        $('.input-group').each(function(){
            if($(this).attr('data-locale')){
                if($(this).attr('data-locale')!=lang)
                    $(this).hide();
                else
                    $(this).show();
            }
        });
        $('textarea').each(function(){
            if($(this).attr('data-locale')){
                if($(this).attr('data-locale')!=lang)
                    $(this).addClass('hide');
                else
                    $(this).removeClass('hide');

                $(this).parent().find('.langlbl').text(lang.toUpperCase());
            }
        });

        hideErrorLabels();
    });
    //
    // Define variables
    //

    // Modal template
    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header">\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n';

    // Buttons inside zoom modal
    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
        fullscreen: 'btn btn-default btn-icon btn-xs',
        borderless: 'btn btn-default btn-icon btn-xs',
        close: 'btn btn-default btn-icon btn-xs'
    };

    // Icons inside zoom modal classes
    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross3"></i>'
    };

    // File actions
    var fileActionSettings = {
        zoomClass: 'btn btn-link btn-xs btn-icon',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        dragClass: 'btn btn-link btn-xs btn-icon',
        dragIcon: '<i class="icon-three-bars"></i>',
        removeClass: 'btn btn-link btn-icon btn-xs',
        removeIcon: '<i class="icon-trash"></i>',
        indicatorNew: '<i class="icon-file-plus text-slate"></i>',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    };
    $('.file-input.image').each(function(index, el) {
        $(el).fileinput({
            browseLabel: 'Examinar',
            browseIcon: '<i class="icon-file-plus"></i>',
            uploadIcon: '<i class="icon-file-upload2"></i>',
            removeIcon: '<i class="icon-cross3"></i>',
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                modal: modalTemplate
            },
            initialPreviewConfig: [
                {caption: $(el).attr('data-caption'), size: $(el).attr('data-size'), key: 1, showDrag: false, showDelete: false}
            ],
            initialPreviewAsData: true,
            overwriteInitial: true,
            initialCaption: "No file selected",
            fileActionSettings: fileActionSettings
        });
    });
    $('.file-input.files').each(function(index, el) {
        $(el).fileinput({
            browseLabel: 'Examinar',
            removeLabel: '',
            removeIcon: '<i class="icon-cross3"></i>',
            overwriteInitial: true,
            initialCaption: $(el).attr('data-caption'),
            fileActionSettings: fileActionSettings
        });
    });
    // Add class on init
    $('.tokenfield-primary').on('tokenfield:initialize', function (e) {
        $(this).parent().find('.token').addClass('bg-primary')
    });

    // Initialize plugin
    $('.tokenfield-primary').tokenfield();

    // Add class when token is created
    $('.tokenfield-primary').on('tokenfield:createdtoken', function (e) {
        $(e.relatedTarget).addClass('bg-primary')
    });

    // hide label error for other languages
    hideErrorLabels();
});

function hideErrorLabels(lang)
{
    var lang = $('[data-action="changelang"].active:first').attr('data-lang');
    $('label.error').each(function(){
        if($(this).attr('data-locale')){
            if($(this).attr('data-locale')!=lang)
                $(this).hide();
            else
                $(this).show();
        }
    });
}

function friendly(str)
{
    str = str.replace(/[ÀÁÂÃÄÅ]/g,"A");
    str = str.replace(/[àáâãäå]/g,"a");
    str = str.replace(/[ÈÉÊË]/g,"E");
    str = str.replace(/[èéêë]/g,"e");
    str = str.replace(/[ÌÍÎÏ]/g,"I");
    str = str.replace(/[ìíîï]/g,"i");
    str = str.replace(/[ÒÓÔÖ]/g,"O");
    str = str.replace(/[òóôö]/g,"o");
    str = str.replace(/[ÙÚÛÜ]/g,"U");
    str = str.replace(/[ùúûü]/g,"u");
    str = str.replace(/[^a-z0-9]/gi,'-');
    str = str.replace(/-+$/,'');
    str = str.replace(/--/,'-');
     return str.toLowerCase();
}

function initInputs(block) {
    $('[type="checkbox"]',block).uniform();

    $('[type="checkbox"][name^="checkbox_"]',block).change(function () {
        $(this).parent().parent().siblings('input').val($(this).prop('checked') ? 1 : 0);
    });

        // Collapse elements
    // -------------------------

    //
    // Sidebar categories
    //

    // Hide if collapsed by default
    $('.category-collapsed',block).children('.category-content').hide();


    // Rotate icon if collapsed by default
    $('.category-collapsed',block).find('[data-action=collapse]').addClass('rotate-180');


    // Collapse on click
    $('.category-title [data-action=collapse]',block).click(function (e) {
        e.preventDefault();
        var $categoryCollapse = $(this).parent().parent().parent().nextAll();
        $(this).parents('.category-title').toggleClass('category-collapsed');
        $(this).toggleClass('rotate-180');

        containerHeight(); // adjust page height

        $categoryCollapse.slideToggle(150);
    });


    //
    // Panels
    //

    // Hide if collapsed by default
    $('.panel-collapsed',block).children('.panel-heading').nextAll().hide();


    // Rotate icon if collapsed by default
    $('.panel-collapsed',block).find('[data-action=collapse]').addClass('rotate-180');


    // Collapse on click
    $('.panel [data-action=collapse]',block).click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $panelCollapse = $(this).parent().parent().parent().parent().nextAll();
        $(this).parents('.panel').toggleClass('panel-collapsed');
        $(this).toggleClass('rotate-180');

        containerHeight(); // recalculate page height

        $panelCollapse.slideToggle(150);
    });
}
// Calculate min height
    function containerHeight() {
        var availableHeight = $(window).height() - $('.page-container').offset().top - $('.navbar-fixed-bottom').outerHeight();

        $('.page-container').attr('style', 'min-height:' + availableHeight + 'px');
    }