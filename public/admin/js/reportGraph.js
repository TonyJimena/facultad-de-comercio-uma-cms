require.config({
    paths: {
        echarts: 'admin/assets/js/plugins/visualization/echarts'
    }
});
var echarts=false;
var limitless=false;
// Configuration
// ------------------------------
require(
    [
        'echarts',
        'echarts/theme/limitless',
        'echarts/chart/bar',
        'echarts/chart/pie'
    ],
    // Charts setup
    function (ec, limitless) {
        echarts=ec;
        limitless=limitless;
    }
);

$('#show-more-graph-4').click(function() {
    var element = $('tr.column-hidden');
    if(element.hasClass('hide')){
        element.removeClass('hide');
        $(this).text('Mostrar menos');
    }else{
        element.addClass('hide');
        $(this).text('Mostrar más');
    }
});

$('div[id^="graph"]').each(function() {
    var element = $(this);
    var id = element.attr('id');
    var func = element.data('func');

    requestGraph(id, func);

});

$('.graph-year').change(function() {
    var element = $(this);
    var func = element.data('func');   
    var year = element.val();
    var graph = $('#graph-' + func);
    var id = graph.attr('id');
    hideMessageNoData(func);
    requestGraph(id, func, {year: year});
});

function requestGraph(id, func, params = {}) {
    ajaxUrl =  BASE_URL+'/report/graph/'+ func;

    $.post(ajaxUrl, params, function(response) {
        switch(func) {
            case 1:
                drawGraph1(id, response);
                break;
            case 2:
                drawGraph2(id, response);
                break;
            case 3:
                //drawGraph3(id, response);
                break;
            case 4:
                drawGraph4(id, response);
                break;
        }
    });
}

function drawGraph1(id, data) {
    // Initialize charts
    // ------------------------------
    if(Object.keys(data.data).length > 0){
        var myChart = echarts.init(document.getElementById(id));
        var jobsOffer = [];
        var laborers = [];
        var managers = [];
        var colours = data.colours;
        var legends = data.legends;
    
        data = data.data;
        for(key in data) {
            jobsOffer.push(data[key]['jobs_offer']);
            laborers.push(data[key]['laborers']);
            managers.push(data[key]['managers']);
        }
    
        var numbers = [jobsOffer, laborers, managers];
        var series = [];
        for(key in numbers) {
            var serie = {
                name: legends[key],
                type: 'bar',
                data: numbers[key],
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            textStyle: {
                                fontWeight: 500
                            }
                        }
                    }
                }
            }
            series.push(serie);
        }
        option = {
            color: colours,
            // Add tooltip
            tooltip: {
                trigger: 'axis'
            },
            // Add legend
            legend: {
                data: legends
            },
            toolbox: {
                show: true,
                feature: {
                    saveAsImage: {
                        show: true,
                        name: $("#title-graph-1").text(),
                        title: 'Guardar como imagen',
                        lang: ['Guardar']
                    }
                }
            },
            // Enable drag recalculate
            calculable: true,
            // Horizontal axis
            xAxis: {
                type: 'category',
                data: Object.keys(data)
            },
            // Vertical axis
            yAxis: {
                type: 'value'
            },
            // Add series
            series: series
        };
        // Apply options
        // ------------------------------
        myChart.setOption(option);
        // Resize charts
        // ------------------------------
        myChart.resize();
    }else{
        showMessageNoData(id);
    }
}
function drawGraph2(id, data) {
    // Initialize charts
    // ------------------------------
    if(Object.keys(data.data).length > 0){
        var myChart = echarts.init(document.getElementById(id));
        var openJobsOffer = [];
        var closedJobsOffer = [];
        var finishedJobsOffer = [];
        var colours = data.colours;
        var legends = data.legends;
    
        data = data.data;
        for(key in data) {
            openJobsOffer.push(data[key]['open_jobs_offer']);
            closedJobsOffer.push(data[key]['closed_jobs_offer']);
            finishedJobsOffer.push(data[key]['finished_jobs_offer']);
        }
    
        var numbers = [openJobsOffer, closedJobsOffer, finishedJobsOffer];
        var series = [];
        for(key in numbers){
            var serie = {
                name: legends[key],
                type: 'bar',
                data: numbers[key],
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            textStyle: {
                                fontWeight: 500
                            }
                        }
                    }
                }
            }
            series.push(serie);
        }
        option = {
            color: colours,
            // Add tooltip
            tooltip: {
                trigger: 'axis'
            },
            // Add legend
            legend: {
                data: legends
            },
            toolbox: {
                show: true,
                feature: {
                    saveAsImage: {
                        show: true,
                        name: $("#title-graph-2").text(),
                        title: 'Guardar como imagen',
                        lang: ['Guardar']
                    }
                }
            },
            // Enable drag recalculate
            calculable: true,
            // Horizontal axis
            xAxis: {
                type: 'category',
                data: Object.keys(data)
            },
            // Vertical axis
            yAxis: {
                type: 'value'
            },
            // Add series
            series: series
        };
        // Apply options
        // ------------------------------
        myChart.setOption(option);
        // Resize charts
        // ------------------------------
        myChart.resize();
    }else{
        showMessageNoData(id);
    }
}
function drawGraph3(id, data) {
    //
    // Multiple donuts options
    //
    if(Object.keys(data.data).length > 0){
        var positions = data.positions;
        var colours = data.colours;
        data = data.data;
        var chartHeight = 850;
        $('#' + id).css({'height': chartHeight});
        var myChart = echarts.init(document.getElementById(id));
    
        // Top text label
        var labelTop = {
            normal: {
                label: {
                    show: true,
                    position: 'center',
                    formatter: '{b}\n',
                    textStyle: {
                        baseline: 'middle',
                        fontWeight: 300,
                        fontSize: 15
                    }
                },
                labelLine: {
                    show: false
                }
            }
        };
        // Format bottom label
        var labelFromatter = {
            normal: {
                label: {
                    formatter: function (params) {
                        return '\n\n' + (100 - params.value).toFixed(2) + '%'
                    }
                }
            }
        }
        // Bottom text label
        var labelBottom = {
            normal: {
                color: '#eee',
                label: {
                    show: true,
                    position: 'center',
                    textStyle: {
                        baseline: 'middle'
                    }
                },
                labelLine: {
                    show: false
                }
            },
            emphasis: {
                color: 'rgba(0,0,0,0)'
            }
        };
        // Set inner and outer radius
        var radius = [45, 60];
        
        var series = [];
        var i = 0;
        for(key in data) {
            var serie = {
                type: 'pie',
                center: positions[i],
                radius: radius,
                itemStyle: labelFromatter,
                data: [
                    {name: 'other', value: 100 - data[key], itemStyle: labelBottom},
                    {name: key, value: data[key],itemStyle: labelTop}
                ]
            }
            series.push(serie);
            i++;
        }
        // Add options
        option = {
            color: colours,
            // Add title
            /*title: {
                text: 'The Application World',
                subtext: 'from global web index',
                x: 'center'
            },*/
            // Add legend
            legend: {
                x: 'center',
                y: '0%',
                data: Object.keys(data)
            },
            toolbox: {
                show: true,
                feature: {
                    saveAsImage: {
                        show: true,
                        name: $("#title-graph-3").text(),
                        title: 'Guardar como imagen',
                        lang: ['Guardar']
                    }
                }
            },
            // Add series
            series: series
        };
        // Apply options
        // ------------------------------
        myChart.setOption(option);
        // Resize charts
        // ------------------------------
        myChart.resize();
    }else{
        showMessageNoData(id);
    }
}
function drawGraph4(id, data) {
    if(Object.keys(data.data).length > 0){
        var columnShow = data.column_show;
        var totalLaborers = data.total_laborers;
        var totalManagers = data.total_managers;
        var i = 1;
        data = data.data;
        var table = '<table class="table">';
        // Head
        table += '<thead><tr><th></th><th>Jornaleros</th><th>Responsables</th><th></th></tr></thead>';
        // Body
        table += '<tbody>';
        for(key in data) {
            var classTr = i > columnShow? 'column-hidden hide' : '';
            var numLaborers = data[key]['laborers'];
            var numManagers = data[key]['managers'];
            var percentageLaborers = totalLaborers > 0? ((numLaborers / totalLaborers)*100).toFixed(2) : 0.00;
            var percentageManagers = totalManagers > 0? ((numManagers / totalManagers)*100).toFixed(2) : 0.00;
            table += '<tr class="'+ classTr +'">';
            table += '<td>' + key + '</td>';
            table += '<td>'+ numLaborers +'</td>';
            table += '<td>'+ numManagers +'</td>';
            table += '<td><div class="progress"><div class="progress-bar bg-green-1000" style="width: '+ percentageLaborers +'%"></div></div><span>'+ percentageLaborers +'% Jornaleros</span>'
            table += '<div class="progress"><div class="progress-bar bg-yellow-200" style="width: '+ percentageManagers +'%"></div></div><span>'+ percentageManagers +'% Responsables</span>'
            table += '</tr>';
            i++;
        }
        table += '</tbody>';
        table += '</table>';

        $('#'+id).append(table);
    }else{
        showMessageNoData(id);
    }
}

function showMessageNoData(id) {
    $('#' + id).empty().append('<div class="message-graph-no-data"><h3><i class="fa fa-bar-chart"></i>Sin datos disponibles</h3></div>');
    $('#' + id).closest('.chart-container').addClass('chart-container-reduce');
}
function hideMessageNoData(id) {
    $('#graph-' + id).find('.message-graph-no-data').remove();
    $('#graph-' + id).closest('.chart-container').removeClass('chart-container-reduce');
}