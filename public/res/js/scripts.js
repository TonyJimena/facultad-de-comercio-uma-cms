$.fn.checkForm = function (selectors,myForm) {

//VARS
var error = false;

//RESET
$('label.error').remove();
$('.require').removeClass('require');

	//VALIDATION
	for(var i=0; i<selectors.length; i++) {
	               
	   var selector = selectors[i];
	   
	   switch(selector.type) {
	                   
	   case 'empty':                
	   		if($('#'+selector.name).val().length<1){
				$('#'+selector.name).addClass('require')
				$('#'+selector.name).after('<label class="error">'+selector.message+'</label>');
				error = true;
			}                                                                             
		break;
	                   
		case 'alphanumeric':
			var patt=/^[0-9a-zA-Z. ]+$/;
			var val = $('#'+selector.name).val();
			if(!patt.test(val)){
				$('#'+selector.name).addClass('require')
				$('#'+selector.name).after('<label class="error">'+selector.message+'</label>');
				error = true;
			}                                                                             
		break;
	                   
		case 'numeric':
			var val = $('#'+selector.name).val();
	   if(isNaN(val) || val.length<1){
				$('#'+selector.name).addClass('require')
				$('#'+selector.name).after('<label class="error">'+selector.message+'</label>');
				error = true;
			}                                                                             
		break;
	                                  
		case 'phone':
			var patt=/^[0-9+ ]+$/;
			var val = $('#'+selector.name).val();
			if(!patt.test(val) || val.length<9){
				$('#'+selector.name).addClass('require')
				$('#'+selector.name).after('<label class="error">'+selector.message+'</label>');
				error = true;
			}                                                                             
		break;
	                   
		case 'email':
			var patt= /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;;
			var val = $('#'+selector.name).val();
			if(val!="" && !patt.test(val)){
				$('#'+selector.name).addClass('require');
				$('#'+selector.name).after('<label class="error">'+selector.message+'</label>');
				error = true;
			}
		break;   
	                                  
		case 'check':
			if(!$('#'+selector.name).is(':checked')) {  
				$('#'+selector.name).addClass('require');
				$('#'+selector.name).closest('.condiciones').after('<label class="error">'+selector.message+'</label>');
				error = true;
			}
		break;   
	                                  
		case 'interval':
			var val = $('#'+selector.name).val();
			if(isNaN(val) || val.length<1 || val<selector.min || val>selector.max ){
				$('#'+selector.name).addClass('require');
				$('#'+selector.name).after('<label class="error">'+selector.message+'</label>');
				error = true;
			} 
		break;

		case 'password':
			var patt=/^[0-9a-zA-Z ]+$/;
			var val = $('#'+selector.name).val();
			if(!patt.test(val)){
				$('#'+selector.name).addClass('require');
				$('#'+selector.name).after('<label class="error">'+selector.message+'</label>');
				error = true;
			}  
			var val2 = $('#'+selector.name2).val();
			if(!patt.test(val2)){
				$('#'+selector.name2).addClass('require');
				$('#'+selector.name2).after('<label class="error">'+selector.message2+'</label>');
				error = true;
			} 
			if(val!=val2){
				$('#'+selector.name).addClass('require');
				$('#'+selector.name).after('<label class="error">'+selector.message3+'</label>');
				error = true;
			}  
			break; 
		case 'remote':
			var val = $('#'+selector.name).val();
			if(val!=""){
				$.ajax({
					type: "POST",
					url: selector.url,
					data: {data:val},
					async: false,
					dataType: 'json'
				})
				.done(function( msg ) {
					if (msg.result=="true"){
						$('#'+selector.name).after('<label class="error">'+selector.message+'</label>');
						error = true;
					}						
				});
			}                                                                         
		break;              
	                                  
	   }

	   
	               
	}

	if(error){
	   $('label.error').fadeIn(1000);
	   return false;
	}else{
	   return true;
	}
}



$.fn.goTop = function () {
	
	$('html,body').animate({scrollTop: 0}, 1000);

}


$.fn.setHeight = function () {
	
	var heightLeftColum = $(this).height();
	var heightScreen = $(window).height();
	var heightFooter = $('footer').height();
	if(heightScreen > heightLeftColum) {	
		var alto = heightScreen - heightFooter;
		$('#columLeft').height(alto);
	} 
}

$.fn.cleanForm = function() {
            $(this).find('input').val('');
            $(this).find('select').val('');
            $(this).find('input[type="checkbox"]').prop('checked', false);
            $(this).find('textarea').val('');
        };

$.fn.getCookie = function(name) {
  match = document.cookie.match(new RegExp(name + '=([^;]+)'));
  if (match) return match[1]; else return false;
}