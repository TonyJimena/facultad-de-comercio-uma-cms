<?php

return [

    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */

    'title' => 'Título',
    'new' => 'Nuevo',
    'save' => 'Guardar',
    'download' => 'Descargar',
    'cancel' => 'Cancelar',
    'ok' => 'OK',
    'close' => 'Cancelar',
    'accept' => 'Aceptar',
    'delete' => 'Borrar',
    'delete-confirmation' => '¿Est&aacute; seguro de que desea eliminar este elemento?',
    'delete-title' => 'Eliminar',
    'delete-success' => 'Registro eliminado correctamente',
    'delete-fail' => 'Error inesperado al intentar eliminar el elemento',
    'store-success' => 'Registro guardado correctamente',
    'send-success' => 'Registro enviado correctamente',
    'store-fail' => 'No se pudo guardar el elemento',
    'edit-title' => 'Editar',
    'update-success' => 'Registro actualizado correctamente',
    'update-fail' => 'No se pudo actualizar el elemento',
    'destroy-success' => 'Registro borrado correctamente',
    'destroy-fail' => 'No se pudo borrar el elemento',
    'create-factura-title' => 'Crear Factura',
    'create-factura-confirmation' => '¿Est&aacute; seguro de que desea crear una factura a partir de este presupuesto?',
    'cancel-success' => 'Registro eliminado correctamente',
    'activate-success' => 'Registro activado correctamente',
    'activate-fail' => 'No se pudo activar el registro',
    'inactivate-success' => 'Registro desactivado correctamente',
    'inactivate-fail' => 'No se pudo desactivar el registro',
    'export' => 'Exportar',
    'pdf' => 'PDF',
    'return' => 'Volver',
    'forgot_password' => 'Recuperar contraseña',
    'reset_password' => 'Cambiar contraseña',
    'print' => 'Imprimir',
    'apply' => 'Aplicar',
    'save_new' => 'Guardar y Crear',
    'inactivate' => 'Desactivar',
    'activate' => 'Activar',
    'send' => 'Enviar',
    'send_new' => 'Enviar y Crear'
];
