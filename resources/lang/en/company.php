<?php
return [
    'mtitle' => 'Empresas',
    'not_found' => 'No existe la empresa',
    'company_name' => 'Nombre Empresa',
    'number_centers' => 'Nº Centros',
    'number_employees' => 'Nº Empleados',
    'activity' => 'Actividad',
    'name' => 'Nombre Empresa',
    'web' => 'Web',
    'address' => 'Dirección',
    'sector_id' => 'Sector',
    'services' => 'Servicios',
    'annual_billing' => 'Facturación Anual',
    'google_maps_url' => 'URL Google Maps'
];