<?php
return [
    'mtitle' => 'Eventos',
    'not_found' => 'No existe el evento',
    'available' => 'El evento está disponible',
    'completed' => 'El evento tiene el aforo completado',
    'canceled' => 'El evento está cancelado',
    'finalized' => 'El evento está finalizado',
    'title' => 'Título',
    'description' => 'Descripción',
    'Address' => 'Dirección',
    'Date' => 'Fecha',
    'number_attendees' => 'Aforo',
    'date' => 'Fecha',
    'address' => 'Dirección',
    'event_state_id' => 'Estado',
    'image1' => 'Imagen',
    'id' => 'ID',
    'company_exists' => 'Ya tienes confirmada la asistencia a este evento',
    'company_not_exists' => 'Aún no has confirmado la asistencia a este evento',
    'event_state' => 'Estado Evento',
    'file1' => 'Fichero',
    'google_maps_url' => 'URL Google Maps'
];
