<?php
return array(
    
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'forbidden_download' => 'No tienes permisos para descargar el archivo',
    'forbidden_remove' => 'No tienes permisos para borrar el archivo',
    'not_found' => 'No existe el fichero',
    'destroy' => 'Fichero eliminado correctamente',
    'no_destroy' => 'No se ha podido eliminar el fichero',
    'no_store' => 'Error al crear el fichero'
);