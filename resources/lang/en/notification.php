<?php

return [
    
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Notificaciones',
    'invalids' => 'Inválidos',
    'failed' => 'Fallidos',
    'sended' => 'Enviados',
    'sended_at' => 'Enviado',
    'notification_template' => 'Plantilla',
    'description' => 'Descripción',
    'notification_template_type' => 'Tipo Plantilla',
    'template' => 'Plantilla',
    'not_found' => 'No existe la notificación',
    'title' => 'Título',
    'description' => 'Descripción',
    'created_at' => 'Creada'
];