<?php

return [
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'reset_password' => 'Cambiar Contraseña',
    'send_link' => 'Enviar',
    'password' => 'Contraseña',
    'confirm_password' => 'Confirma Contraseña',
    'message_success_reset' => 'La contraseña ha sido cambiada.',
    'message_error_reset' => 'El cambio de contraseña ha expirado.',
    'no_user' => 'No se ha localizado el usuario.'
];