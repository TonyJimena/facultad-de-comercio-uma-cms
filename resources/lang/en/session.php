<?php


return [
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'login' => 'Iniciar sesión',
    'logout' => 'Cerrar sesión',
    'forgotten' => '¿Has olvidado la contraseña?',
    'username' => 'Usuario',
    'password' => 'Contraseña',
    'too_much_attemps' => 'Demasiados intentos de inicio de sesión',
    'signin' => 'Entrar',
    'change_password' => 'Cambiar contraseña',
    'old_password' => 'Contraseña actual',
    'new_password' => 'Nueva contraseña',
    'new_password_confirmation' => 'Confirmación nueva contraseña',
    'readbefore' => 'Por favor lea nuetras :attribute antes de entrar',
    'terms' => 'Términos de uso',
    'flash_invalid' => 'Credenciales inválidas',
    'flash_logged' => 'Sesión iniciada correctamente',
    'fash_logout' => 'Sesión cerrada correctamente',
    'my_profile' => 'Mi perfil'
];