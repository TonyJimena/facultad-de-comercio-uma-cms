<?php


return [
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Datos de ajustes generales',
    'field' => 'Campo',
    'value' => 'Valor'
];