<?php
return [
    'not_found' => 'No existe el usuario',
    'not_is_laborer' => 'Tienes que ser un jornalero para realizar esta acción',
    'not_is_manager' => 'Tienes que ser un responsable de empresa para realizar esta acción',
    'username' => 'Usuario',
    'password' => 'Contraseña',
];