<?php
return array(
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'message_success_verify' => 'Hemos verificado tu email correctamente',
    'message_error_verify' => 'No hemos podido verificar tu email. Vuelve a iniciar sesión desde la App.',
    'no_user' => 'No se ha localizado el usuario.',
    'sent' => 'Se ha enviado el email de verificación'
);