<?php

return [
    'mtitle' => 'Asistencias Eventos',
    'id' => 'ID',
    'title' => 'Título',
    'state' => 'Estado',
    'name' => 'Empresa',
    'event' => 'Evento',
    'company' => 'Empresa',
    'event_id' => 'Evento',
    'company_id' => 'Empresa',
    'in_reserve' => 'En Reserva'
];