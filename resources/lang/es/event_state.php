<?php

return [
    'mtitle' => 'Estados Eventos',
    'id' => 'ID',
    'event_state' => 'Estado',
    'name' => 'Nombre',
    'label' => 'Etiqueta',
    'description' => 'Descripción',
    'state' => 'Estado',
];