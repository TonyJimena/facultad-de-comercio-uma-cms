<?php

return [
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Plantillas Notificaciones',
    'notification_template_type_id' => 'Tipo Plantilla Notificación',
    'type' => 'Tipo Plantilla',
    'not_found' => 'No existe la plantilla de notificación'
];