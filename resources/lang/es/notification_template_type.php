<?php

return [
    
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Tipos Plantillas Notificaciones',
    'type' => 'Tipo Plantilla',
    'not_found' => 'No existe el tipo de plantilla de notificación',
    'send_to_type' => 'Enviar a'
];