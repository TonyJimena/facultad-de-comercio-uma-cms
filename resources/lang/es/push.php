<?php

return [
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Pushes',
    'sended_at' => 'Fecha de envío',
    'devices' => 'Dispositivos',
    'warning' => 'Inválidos',
    'errors' => 'Fallidos',
    'sent' => 'Enviado',
    'device_id' => 'Dispositivo',
    'notification_id' => 'Notificación',
    'recipients' => 'Recibidos',
    'device' => 'Dispositivo',
    'not_found' => 'No existe la push'
];