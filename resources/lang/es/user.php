<?php
return [
    'mtitle' => 'Usuarios',
    'id' => 'ID',
    'not_found' => 'No existe el usuario',
    'not_is_laborer' => 'Tienes que ser un jornalero para realizar esta acción',
    'not_is_manager' => 'Tienes que ser un responsable de empresa para realizar esta acción',
    'username' => 'Usuario',
    'password' => 'Contraseña',
    'renovation_date' => 'Fecha Renovación',
    'name' => 'Nombre',
    'surname' => 'Apellidos',
    'email' => 'Correo Electrónico',
    'company_name' => 'Nombre Empresa',
    'address' => 'Dirección',
    'active' => 'Activo',
    'number_centers' => 'Nº Centros',
    'number_employees' => 'Nº Empleados',
    'sector' => 'Sector'
];