<?php
return [
    'name' => 'Nombre',
    'surname' => 'Apellidos',
    'email' => 'Correo Electrónico',
    'position' => 'Puesto en la Empresa',
    'phone_number' => 'Teléfono Móvil',
    'telephone_number' => 'Teléfono',
    'reason_registration' => 'Razón del Registro',
    'contribution' => 'Aportación',
    'partner' => 'Socio',
    'linkedin_url' => 'URL LinkedIn',
    'facebook_url' => 'URL Facebook',
    'instagram_url' => 'URL Instagram',
    'twitter_url' => 'URL Twitter',
];
