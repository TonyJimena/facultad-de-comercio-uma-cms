@extends('edit') 
<!-- Page content -->
@section('edit-master')
@if($item->in_reserve)
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info">
            Esta asistencia se encuentra en la lista de reserva del evento. Si quieres confirmarla pulsa <a href="{{route('company_event.confirm', ['id' => $item->id])}}" class="btn btn-sm bg-theme-primary">Confirmar</a>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang('global.company_event')</legend>
            <div class="form-group">
            {!! Form::mySelectField($item, 'event_id', $module, $events, $errors, array('id' => 'event_id', 'class' => 'form-control error', 'cols' => 6 ) ) !!}
            {!! Form::mySelectField($item, 'company_id', $module, $companies, $errors, array('id' => 'company_id', 'class' => 'form-control error', 'cols' => 6 ) ) !!}
            </div>
        </fieldset>
    </div>
</div>
@stop