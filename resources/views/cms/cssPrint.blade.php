<style type="text/css">
    @page {
      margin: 0;
    }
    @media print {
        * {
            box-shadow: none !important;
        }
        html, body {
            background: #fff;
            font-size: 0.8em !important;
        }
        #main {
            padding-top: 0 !important;
        }
        header, button, .input-group-addon, input[type="button"], .btn {
            display: none !important;
        }
        input[type="text"], textarea {
            border: 0 !important;
            padding: 0 !important;
            font-size: 1em !important;
        }
        .form-control, .chosen-container {
            display: inline-block !important;
            width: auto !important;
            margin-left: 10px !important;
        }
        textarea.form-control {
            display: block !important;
            margin-left: 0 !important;
        }
        .input-group {
            display: inline-block !important;
        }
        .panel-title {
            font-size: 1em !important;
        }
        legend {
            font-size: 1.2em !important;
            font-weight: bold;
            margin-top: 10px;
            margin-bottom: 5px;
        }
        label:after {
            content: ":";
        }
        .panel {
            border: 0 !important;
        }
        .panel .fa {
            display: none !important;
        }
        .chosen-container, .chosen-single {
            border: 0 !important;
            padding: 0 !important;
            font-size: 1em !important;
        }
        .chosen-single > div {
            display: none !important;
        }
        .chosen {
            display: none !important;
        }
        .form-group {
            margin-bottom: 0 !important;
        }
        form label > span {
            display: none;
        }
        #gritter-notice-wrapper {
            display: none !important;
        }
        ::-webkit-input-placeholder {
            color: transparent !important;
        }
        ::-moz-placeholder {
            color: transparent !important;
        }
        :-ms-input-placeholder {
            color: transparent !important;
        }
        ::placeholder {
            color: transparent !important;
        }
    }
</style>
