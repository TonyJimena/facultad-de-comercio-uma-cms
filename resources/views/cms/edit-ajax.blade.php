@extends('layouts.default') 

@section('content')
<div class="modal-dialog modal-lg">

    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">
                @if (empty($item->id))
                <i class="fa fa-plus"></i> @lang($module.'.new')
                @else
                <i class="fa fa-pencil"></i> @lang($module.'.edit')
                @endif
            </h4>
            <div class="heading-elements">
                <ul class="icons-list">
                  @foreach (App\Models\Language::getAvailable() as $key=>$lang)
                      <li><button type="button" data-lang="{!! $lang->code !!}" data-action="changelang" class="btn bg-slate  @if ($key==0)active @endif">{!! strtoupper($lang->code) !!}</button></li>
                  @endforeach
                  </ul>                
              </div>
        </div>         	
    	{!! Form::model($item, $blade_form_params) !!}
        <div class="modal-body">

        @yield('edit-ajax-master')
    	</div>
    	<div class="modal-footer">
    		<button type="button" class="btn btn-primary btn-gradient saveRelated" module="{!! $module !!}">@lang('buttons.save')</button>
    		<button type="button" class="btn btn-default btn-gradient" data-dismiss="modal">@lang('buttons.close')</button>
        </div>
        @if ($item->options['seo'])
            <div class="row">
              <div class="col-md-12">
                  <div id="accordionSeo" class="panel-group accordion">
                 <?php echo view()->make('seo_tags.edit')
                            ->with('module', $module)
                            ->with('item', $item)
                            ->render();?>
                  @yield('edit-seo','fail')
                  </div>
              </div>
            </div>
            @endif
    	{!! Form::close() !!} 
    </div>
</div>

@stop