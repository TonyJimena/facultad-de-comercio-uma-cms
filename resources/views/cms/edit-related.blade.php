@section('edit-related')
@if (!empty($item->id))
    <?php $cont = 0; ?>
    {!! Form::hidden($related_field, $item->id, array('id' => $related_field)) !!}    
    {!! Form::hidden('related_field', $related_field, array('id' => 'related_field')) !!}
    @if (!empty($relations))
        @foreach ($relations as $relatedModule)       
            @php
                $camelizeModule = Stringy\StaticStringy::upperCamelize($relatedModule); 
                $class = '\\App\\Models\\' . $camelizeModule;
                $mod = new $class();
            @endphp  
			<div class="panel panel-flat panel-collapsed">
                <div class="panel-heading"  data-bindaction="collapse">
                    <h5 class="panel-title">@lang($relatedModule.'.mtitle') </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            @if($mod->options['can_create'])
                            <li><a id="btnNuevo" type="button" class="btn bg-blue" href="{{ route($relatedModule.'.create', array('autocreate' => 0))}}"><i class="glyphicon glyphicon-plus"></i> @lang('buttons.new')</a></li>
                            @endif
                            <li><a id="{{$relatedModule}}btnPdf" type="button" class="btn bg-theme-primary" href="{{ route($relatedModule.'.listToPdf')}}"> @lang('buttons.pdf')</a></li>
                            <li><a id="{{$relatedModule}}btnExportar" type="button" class="btn bg-theme-primary" href="{{ route($relatedModule.'.export')}}"> @lang('buttons.export')</a></li>
                            <li><a class="btn bg-slate" data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    {!! Html::divCommonSearch( $relatedModule, $module ) !!}
                    <!-- table-responsive -->
                    {!! Html::myDataTable($relatedModule, $related_field) !!}
                </div>
            </div>
            <?php $cont++; ?>
        @endforeach
    @endif
@endif
@stop