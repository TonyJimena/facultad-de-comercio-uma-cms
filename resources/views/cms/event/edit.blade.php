@extends('edit') 
<!-- Page content -->
@section('edit-master')
<div class="row">
    <div class="col-md-12">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang('global.event')</legend>
            <div class="form-group">
                <div class="col-sm-2">
                    <label>@lang($module . '.date')</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i> 
                        </span>
                        @php
                            $date = $item->date ?: \Carbon\Carbon::now()->format('d/m/Y H:i:s');
                        @endphp
                        <input type="text" value="{{$date}}" class="form-control datetimepicker margin-top-none" id="date" name="date" readonly="">
                    </div>
                </div>
                {!! Form::myTextField($item, 'address', $item->getTable(), $errors, array( 'cols' => 3 )) !!}
                {!! Form::myTextField($item, 'google_maps_url', $item->getTable(), $errors, array( 'cols' => 3 )) !!}
                {!! Form::myTextField($item, 'number_attendees', $item->getTable(), $errors, array( 'cols' => 1 )) !!}
                {!! Form::mySelectField($item, 'event_state_id', $module, $eventState, $errors, array('id' => 'event_state_id', 'class' => 'form-control error', 'cols' => 3 ) ) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextField($item, 'title', $item->getTable(), $errors, array( 'cols' => 12 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextArea($item, 'description', $module, $errors, array( 'cols' => 12 )) !!}
            </div>
        </fieldset>
    </div>
</div>
@stop
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript" src="{{ res('/assets/js/plugins/pickers/anytime.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".datetimepicker").AnyTime_picker({
            format: "%d/%m/%Y %H:%i:%s",
            labelDayOfMonth: 'Día del mes',
            labelHour: 'Hora',
            labelMinute: 'Minutos',
            labelMonth: 'Mes',
            labelSecond: 'Segundos',
            labelTitle: 'Selecciona día y hora',
            labelYear: 'Año',
            dayAbbreviations: ['Dom', 'Lun', 'Mart', 'Miérc', 'Juev', 'Vier', 'Sáb'],
            monthAbbreviations: ['En', 'Febr', 'Mzo', 'Abr', 'My', 'Jun', 'Jul', 'Ag', 'Sept', 'Oct', 'Nov', 'Dic']
        });
    });
</script>
@stop