@extends('layouts.default')
<!-- Page content -->
@section('content')
<div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-theme-secondary"><i class="{{ $iconHeader }} module-{{ $module }}"></i> {{ strtoupper(trans($module.'.mtitle')) }}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                  @php
                  $camelizeModule = Stringy\StaticStringy::upperCamelize($module);
                  $class = '\\App\\Models\\' . $camelizeModule;
                  if(class_exists($class))$mod = new $class();
                  @endphp
                  <ul class="icons-list">
                    <li><a id="{{$module}}btnPdf" type="button" class="btn bg-theme-primary btn-xs" href="{{ route($module.'.listToPdf')}}"> @lang('buttons.pdf')</a></li>
                    <li><a id="{{$module}}btnExportar" type="button" class="btn bg-theme-primary btn-xs" href="{{ route($module.'.export')}}"> @lang('buttons.export')</a></li>
                    @if($mod->options['can_create'] || $mod->options['can_send'])
                    <li style="margin-left: 40px;"><a id="btnNuevo" type="button" class="btn bg-blue-400 btn-xs" href="{{ route($module.'.create', array('autocreate' => 0))}}"> @lang('buttons.new')</a></li>
                    @endif
                    @if(isset($mod) && $mod->options['seo'] && $mod->relationsGrid)
                    <li><a class="btn bg-slate btn-xs" data-action="collapse"></a></li>
                    @endif
                  </ul>
                </div>
            </div>

            <div class="panel-body">
            {!! Html::myDataTable($module) !!}
          </div>
          </div>
    <!-- Modal delete confirmation -->
    <div aria-hidden="true" role="dialog" tabindex="-1" id="alertModal" class="modal fade" style="display: none;">
      <div class="modal-dialog modal-dialog-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title text-center">@lang('buttons.delete-title')</h4>
          </div>
          <div class="modal-body dialog-center">
            <p class="margin-bottom-lg"> @lang('buttons.delete-confirmation') </p>
            <div class="form-group text-center">
              <button data-dismiss="modal" class="btn btn-success btn-gradient margin-right-sm delRow" action="{{ route($module.'.destroy', 'i') }}" type="button"><i class="fa fa-check"></i> @lang('buttons.accept')</button>
              <button data-dismiss="modal" class="btn btn-danger btn-gradient" type="button"><i class="fa fa-warning"></i> @lang('buttons.cancel')</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal add form -->
    <div aria-hidden="true" role="dialog" tabindex="-1" id="formModal" class="modal fade" style="display: none;">
    </div>
@stop
