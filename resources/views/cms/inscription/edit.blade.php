@extends('edit') 
<!-- Page content -->
@section('edit-master')
@php
    $readonly = $item->user_id ? true : false;
@endphp
@if($item->id && !$item->user_id)
<div class="row">
<div style="float: right;">
    <a class="btn btn-sm bg-theme-primary" href="{{route('inscription.user_create', ['id' => $item->id])}}">@lang('inscription.create_user')</a>
</div>
</div>
@endif
<div class="row">
    <div class="col-md-6">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang('global.user_data')</legend>
            <div class="form-group">
                {!! Form::myTextField($item, 'name', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item, 'email', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextField($item, 'position', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item, 'phone_number', $item->getTable(), $errors, $readonly ? array( 'cols' => 3, 'readonly' => true ) : array( 'cols' => 3 )) !!}
                {!! Form::myTextField($item, 'telephone_number', $item->getTable(), $errors, $readonly ? array( 'cols' => 3, 'readonly' => true ) : array( 'cols' => 3 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextArea($item, 'reason_registration', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
                {!! Form::myTextArea($item, 'contribution', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextField($item, 'linkedin_url', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item, 'facebook_url', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextField($item, 'instagram_url', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item, 'twitter_url', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
            </div>
        </fieldset>
    </div>
    <div class="col-md-6">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang('global.company')</legend>
            <div class="form-group">
                {!! Form::myTextField($item, 'company_name', $item->getTable(), $errors, $readonly ? array( 'cols' => 4, 'readonly' => true ) : array( 'cols' => 4 )) !!}
                {!! Form::myTextField($item, 'number_centers', $item->getTable(), $errors, $readonly ? array( 'cols' => 2, 'readonly' => true ) : array( 'cols' => 2 )) !!}
                {!! Form::myTextField($item, 'number_employees', $item->getTable(), $errors, $readonly ? array( 'cols' => 2, 'readonly' => true ) : array( 'cols' => 2 )) !!}
                {!! Form::myTextField($item, 'web', $item->getTable(), $errors, $readonly ? array( 'cols' => 4, 'readonly' => true ) : array( 'cols' => 4 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextField($item, 'address', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item, 'partner', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
            </div>
            <div class="form-group">
                {!! Form::mySelectField($item, 'sector_id', $module, $sector, $errors, $readonly ? array( 'id' => 'sector_id', 'class' => 'form-control error', 'cols' => 6, 'disabled' => true ) : array('id' => 'sector_id', 'class' => 'form-control error', 'cols' => 6 ) ) !!}
                {!! Form::myTextField($item, 'annual_billing', $item->getTable(), $errors, $readonly ? array( 'cols' => 6, 'readonly' => true ) : array( 'cols' => 6 )) !!}
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label class="" for="google_maps_url">@lang('inscription.google_maps_url')</label>
                    <input type="text" value="{{$item->google_maps_url}}" id="google_maps_url" name="google_maps_url" placeholder="{{trans('inscription.google_maps_url')}}" class="form-control error" {{$readonly ? 'readonly' : ''}}>
                    @if($item->google_maps_url)
                    <a href="{{$item->google_maps_url}}" target="_blank"><i class="fa fa-eye"> Ver dirección</i></a>
                    @endif
                </div>
            </div>
            <div class="form-group">
                {!! Form::myTextArea($item, 'services', $item->getTable(), $errors, $readonly ? array( 'cols' => 12, 'readonly' => true ) : array( 'cols' => 12 )) !!}
            </div>
        </fieldset>
    </div>
</div>

@stop
@section('scripts')
<script type="text/javascript">
    var readonly = @json($readonly);
    if(readonly) {
        $('#btnGuardar').remove();
        $('#btnApply').remove();
    }
</script>
@stop