@if (!Request::ajax())
<!DOCTYPE html>
<html lang="es">
    <head>

        <base href="{{ Config::get('app.url_base') }}">

        <script type="text/javascript">
            BASE_URL = '{{ Config::get('app.url_base') }}';
        </script>

        <!-- Basic Page Needs -->
        <meta charset="utf-8" />
        <title> {{ isset($title)?$title . ' - ' . env('APP_NAME', 'Solbyte CMS') : env('APP_NAME', 'Solbyte CMS') }}</title>
        <meta name="keywords" lang="es" content="">
        <meta name="description" lang="es" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Clickjacking Defense -->

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
        <!-- Font CSS  -->

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ res('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">    
    <link href="{{ res('/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ res('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ res('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ res('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ res('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Your Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ res('/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ res('/css/pages.css') }}">

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ res('/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/core/libraries/jquery_ui/full.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ res('/assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>    
    <script type="text/javascript" src="{{ res('/assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/uploaders/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>    
    <script type="text/javascript" src="{{ res('/assets/js/plugins/editors/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/pickers/pickadate/picker.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/pickers/pickadate/picker.date.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/pickers/pickadate/picker.time.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/pickers/pickadate/legacy.js') }}"></script>   
    <script type="text/javascript" src="{{ res('/assets/js/plugins/pickers/pickadate/translations/es_ES.js') }}"></script> 
    <script type="text/javascript" src="{{ res('/assets/js/plugins/forms/tags/tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/forms/tags/tokenfield.min.js') }}"></script>
    <script type="text/javascript" src="{{ res('/assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <!-- /theme JS files -->
    <!-- Set js config variables -->

    <script type="text/javascript">
    /**
    * Set ajax call to use CSRF protection
    */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var js_lang    = "{{ (App::getLocale() == '')?'es': App::getLocale() }}";
    @if(isset($module) && $module)
    var js_module  = @json($module);
    var js_class = @json('\\App\\Models\\' . Stringy\StaticStringy::upperCamelize($module));
    @endif
    <?php
        $fun = function ($val){
            return (isset($val['name'])?"'".$val['name']."'":"'".$val."'");
        };
    ?>
    var js_columns = "{{ empty($columns) ? 'id,name' : implode(',',array_map($fun, $columns)) }}";
    var langs = [{!! implode(',',array_map($fun, array_column(App\Models\Language::getAvailable()->toArray(), 'code'))) !!}];
    </script>    
    <script type="text/javascript" src="{{ res('/js/lib/helper.js?002') }}"></script>
    <script type="text/javascript" src="{{ res('/js/lib/custom.js?002') }}"></script>
    <script type="text/javascript" src="{{ res('/js/lib/common.js?002') }}"></script>

    @if (isset($module) && file_exists(public_path("res/js/$module.js")))
    <script type="text/javascript" src="{{ res("/js/$module.js") }}"></script>
    @endif
    <!-- Module custom js -->
    @yield('javascripts')
    <!-- Module custom js end -->
        <script type="text/javascript">
            window.tax = {{ App\Helpers\Helper::getTax() }};
        </script>
    </head>
    @php
        if(isset($body_classes)){
            $classBody = $body_classes;
        }elseif(!Auth::check()){
            $classBody = 'login-container bg-white';
        }else{
            $classBody = '';
        }
    @endphp

    <body class="{{ $classBody }}">
        <!-- header -->

        @section('header')
            @if(Auth::check())
                @include('layouts.header')
            @endif
        @show

        <!-- main -->

        <div class="content" id="main">
            <!-- sidebar -->
@endif
            <!-- content -->

            @yield('content')

<script type="text/javascript">
    initInputs(document);
</script>       
@if (!Request::ajax())
        <!--footer-->
        <!--@yield('footer')-->
        @section('footer')
            @include('layouts.footer')
        @show
        </div>
        <!-- Module custom js -->
        @yield('javascripts')
        <!-- Module custom js end -->
        @if (Session::get('flash_message'))
            <?php
                //dd(\Session::all());
                $messageTitle = Session::get('flash_message_title');
                $messageTitle = empty($messageTitle) ? trans('global.notification') : $messageTitle;
                $messageClass = Session::get('flash_message_classes'); 
                Session::forget('flash_message_classes');                    
                $messageClass = empty($messageClass) ? 'bg-primary' : $messageClass;
            ?>
        <script>$( document ).ready(function() {showMessage("{{ $messageTitle }}", "{{ Session::get('flash_message') }}", 3000, "{{ $messageClass }}")});; </script>
        @endif
        @yield('scripts')
    </body>
</html>
@else
@endif

