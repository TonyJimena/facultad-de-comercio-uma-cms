	<div class="navbar navbar-default header-highlight">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ url('/cms') }}" style="display: flex;">
				<img src="{{ res('/assets/images/menu.png') }}" alt="">
				<p style="color: #fff; margin-left: 5px;">CÍCULO EMPRESARIAL </br> <strong>DE MÁLAGA</strong></p>
			</a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li class="dropdown dropdown-user">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<img src="{{ res('/assets/images/user_logo.png') }}" alt="">
							<span>
							@if(Auth::guard('web')->user())
								{{Auth::guard('web')->user()->username}}
							@elseif(Auth::user())
								{{Auth::user()->name}}
							@endif
							</span>
							<i class="caret"></i>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							@if(!Auth::guard('web')->user())
								<li><a href="{{ route('user_cms.edit', ['id' => Auth::user()->id]) }}"><i class="icon-user-plus"></i> @lang('session.my_profile')</a></li>
							@endif
							<li><a href="{{ route('session.destroy') }}"><i class="icon-switch2"></i> @lang('session.logout')</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">					

					@include('layouts.nav')

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				@if(env('HAS_BREADCRUMB'))
				@php
				$dashboard = \App\Models\Module::where('slug', 'dashboard')->first();
				$icon = $dashboard->type_icon . ' ' . $dashboard->icon;
				$icon = $icon?: 'icon-home2';
				$urlDashboard = $dashboard->slug;
				$nameRoute = Route::currentRouteName();
				$subNameRoute = substr($nameRoute, strpos($nameRoute, '.') + 1, strlen($nameRoute));
				@endphp
				<div class="page-header page-header-default">
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li>
								<a href="{{ $urlDashboard? url(env('APP_PREFIX_CMS').'/'.$urlDashboard) : route('home') }}">
									<i class="{{$icon}} position-left"></i>
									@if($nameRoute == $dashboard->name)
									{{ $dashboard->description }}
									@endif
								</a>
							</li>
							@if(isset($module))
								@php
								$mod = \App\Models\Module::where('name', $module)->first();
								@endphp
								@if($mod)
									<li>
										<a href="{{ url(env('APP_PREFIX_CMS').'/'.$mod->slug) }}">
											<span>{{$mod->description}}</span>
										</a>
									</li>
									<li>
										@switch($subNameRoute)
											@case('edit')
													@lang('global.edit')
												@break
											@case('create')
													@lang('global.create')
												@break
										@endswitch
									</li>
								@endif
							@endif
							<!--<li><a href="datatable_advanced.html">Datatables</a></li>
							<li class="active">Advanced</li>-->
						</ul>
					</div>
				</div>
				@endif
				<!-- /page header -->
