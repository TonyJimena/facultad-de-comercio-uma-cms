<!-- Start: Header -->
<header class="navbar navbar-fixed-top">
  <div class="pull-left"> <a class="navbar-brand" href="{{ Config::get('app.url_base') }}">

    <div class="navbar-logo">
      @if( Auth::id() )
        <img src="{{ res( Auth::user()->company_logo_path ) }}" class="img-responsive" alt="logo"/>
      @endif

    </div>
    </a> </div>
  <div class="pull-center">
    @if (Auth::check())
    <?php // $profile = Auth::user()->profile->all();
    ?>
        @if (!empty($profile['modules']))
            @foreach ($profile['modules'] as $moduleitem)
              <button rel="{{ Route::getRoutes()->hasNamedRoute($moduleitem['name'].'.index')?route($moduleitem['name'].'.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa module-{{ $moduleitem['name'] }}"></i> </button>
            @endforeach
        @else
            @if (Acl::can('Customers'))
              <button title="Clientes" rel="{{ Route::getRoutes()->hasNamedRoute('customers.index')?route('customers.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa module-customers"></i> </button>
            @endif

            @if (Acl::can('Providers'))
              <button title="Proveedores" rel="{{ Route::getRoutes()->hasNamedRoute('providers.index')?route('providers.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-truck"></i> </button>
            @endif

            @if (Acl::can('Engineeringprojects') || Acl::can('Ocasurprojects'))
              @if( Auth::user()->company_id == 1 )
                  <button title="Proyectos Ingeniería" rel="{{ Route::getRoutes()->hasNamedRoute('engineeringprojects.index')?route('engineeringprojects.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-briefcase module-engineeringprojects"></i> </button>
              @else
                <button title="Proyectos Ocasur" rel="{{ Route::getRoutes()->hasNamedRoute('ocasurprojects.index')?route('ocasurprojects.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-briefcase module-ocasurprojects"></i> </button>
              @endif
            @endif

            @if (Acl::can('Presupuestos'))
              <button title="Presupuestos"  rel="{{ Route::getRoutes()->hasNamedRoute('presupuestos.index')?route('presupuestos.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-book"></i> </button>
            @endif

            @if (Acl::can('Facturas'))
              <button title="Facturación" rel="{{ Route::getRoutes()->hasNamedRoute('facturas.index')?route('facturas.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-file-text"></i> </button>
            @endif

            @if (Acl::can('IncomingInvoices'))
              <button title="Facturas recibidas" rel="{{ Route::getRoutes()->hasNamedRoute('incoming_invoices.index')?route('incoming_invoices.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-file-text-o"></i> </button>
            @endif

            @if (Acl::can('PartidasPers'))
              <button title="Partidas Pers." rel="{{ Route::getRoutes()->hasNamedRoute('partidas_pers.index')?route('partidas_pers.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-table"></i> </button>
            @endif

            @if (Acl::can('PaymentMethods'))
              <button title="Formas de Pago" rel="{{ Route::getRoutes()->hasNamedRoute('payment_methods.index')?route('payment_methods.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-credit-card"></i> </button>
            @endif

            @if (Acl::can('BankAccounts'))
              <button title="Cuentas Bancarias" rel="{{ Route::getRoutes()->hasNamedRoute('bank_accounts.index')?route('bank_accounts.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button">
                <span class="glyphicons glyphicons-bank"></span>
              </button>
            @endif

            <!--<button title="Gestión Documental" style="opacity: 0.5" rel="/" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-folder"></i> </button>-->
            <!--<button title="Pagos/Cobros" style="opacity: 0.5" rel="/" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-money"></i> </button>-->
            <!-- <button title="Tiendas" rel="{{ Route::getRoutes()->hasNamedRoute('stores.index')?route('stores.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-home"></i> </button> -->
            @if (Acl::can('Employees'))
              <button title="Personal" rel="{{ Route::getRoutes()->hasNamedRoute('employees.index')?route('employees.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa  fa-male module-employees"></i> </button>
            @endif
            
            <!-- <button title="Compras" style="opacity: 0.5" rel="/" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-shopping-cart"></i> </button> -->
            <!-- <button title="Movimientos" rel="{{ Route::getRoutes()->hasNamedRoute('movements.index')?route('movements.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-exchange"></i> </button> -->
            <!-- <button title="Ticket" style="opacity: 0.5" rel="/" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-ticket"></i> </button> -->

            <!--<button title="Partidas" style="opacity: 0.5" rel="/" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-tasks"></i> </button>-->
            @if (Acl::can('User'))
              <button title="Usuarios y Perfiles" rel="{{ Route::getRoutes()->hasNamedRoute('user.index')?route('user.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa module-users"></i> </button>
            @endif

            @if (Acl::can('MasterData'))
              <button title="Datos Maestros" rel="{{ Route::getRoutes()->hasNamedRoute('master-data.index')?route('master-data.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-info module-master-data"></i> </button>
            @endif
            @if (Acl::can('Products'))
              <button title="Productos" rel="{{ Route::getRoutes()->hasNamedRoute('products.index')?route('products.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-th"></i> </button>
            @endif

            @if (Acl::can('ProductsCategories'))
              <button title="Categorías" rel="{{ Route::getRoutes()->hasNamedRoute('products_categories.index')?route('products_categories.index'):'' }}" data-toggle="dropdown" class="btn btn-sm dropdown-toggle module" type="button"> <i class="fa fa-th2"></i> </button>
            @endif
        @endif
    @endif
  </div>
  <div class="pull-right header-btns">
    @if (Auth::check())
    <div class="btn-group user-menu">
      <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicons glyphicons-user"></span><b> {{ Auth::user()->username }}</b> </button>

      <ul class="dropdown-menu checkbox-persist" role="menu">
        <li class="menu-arrow">
          <div class="menu-arrow-up"></div>
        </li>
        <li class="dropdown-header">Tu Cuenta <span class="pull-right glyphicons glyphicons-user"></span></li>
        <li>
          <ul class="dropdown-items">
<!--
            <li>
              <a href="{{ route('customers.index') }}" class="item-message"><div class="item-icon"><i class="fa module-customers"></i> </div>@lang('modules.customers')</a>
            </li>
            <li>
              <a href="{{ route('providers.index') }}" class="item-message"><div class="item-icon"><i class="fa module-providers"></i> </div>@lang('modules.providers')</a>
            </li>
-->
            <li>
              <a class="item-message" href="{{ route('account.change-password') }}"><div class="item-icon"><i class="fa fa-unlock-alt"></i></div>@lang('session.change_password')</a>
            </li>
          </ul>
        </li>
        <li class="dropdown-footer">
          <a href="{{ route('session.destroy') }}"><i class="fa fa-sign-out"></i> @lang('session.logout')</a>
        </li>
      </ul>
    </div>
    @else
        <li {{ (Request::is('login') ? 'class="active"' : '') }}><a href="{{ route('session.create') }}">Sign in</a></li>
        <li {{ (Request::is('signup') ? 'class="active"' : '') }}><a href="{{ route('account.create') }}">Sign up</a></li>
    @endif
  </div>
</header>
