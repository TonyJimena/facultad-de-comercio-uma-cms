@extends('edit')
<!-- Page content -->
@section('edit-master')
    <div class="form-group">
        {!! Form::myTextField($item, 'title', $module, $errors, array( 'cols' => 4, 'maxlength' => 100)) !!}
        {!! Form::myTextField($item, 'short_description', $module, $errors, array( 'cols' => 4, 'maxlength' => 100)) !!}
        {!! Form::myTextField($item, 'description', $module, $errors, array( 'cols' => 4, 'maxlength' => 100)) !!}
        {!! Form::myTextField($item, 'url', $module, $errors, array( 'cols' => 4, 'maxlength' => 100)) !!}
        {!! Form::myCheckBox($item, 'active', $module, $errors, array( 'cols' => 4, 'maxlength' => 100)) !!}
    </div>
@stop
