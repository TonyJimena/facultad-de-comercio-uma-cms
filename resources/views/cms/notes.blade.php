<div class="col-sm-12">
    <div class="controls">
    {{ Form::hidden('relation_notes_noteable_id', (!empty($id))?$id:'' ) }}
    {{ Form::hidden('relation_notes_noteable_type', (!empty($type))?$type:'' ) }}
    {{ Form::hidden('relation_notes_id', (!empty($item->id))?$item->id:'' ) }}
    {{ Form::label('relation_notes_note', trans($module.'.notes'), array('class' => 'control-label')) }}
    {{ Form::textarea('relation_notes_note', (!empty($item->note))?$item->note:'', array('class' => 'form-control', 'rows' => '4') ) }}
    {{ $errors->first('relation_notes_note', '<span class="help-block">:message</span>') }}
    </div>
</div>
