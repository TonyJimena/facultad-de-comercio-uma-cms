@extends('edit') 
<!-- Page content -->
@section('edit-master')
<div class="row">
    <div class="col-md-12">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang('global.notification')</legend>
            <div class="form-group">
                {!! Form::myTextField($item, 'title', $item->getTable(), $errors, array( 'cols' => 12 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextArea($item, 'description', $module, $errors, array( 'cols' => 12 )) !!}
            </div>
            <div class="form-group">
                <strong>Puedes incluir iconos en los textos de la notificación</strong> <a class="btn" href="https://getemoji.com/" target="_blank">Ver iconos</a>
            </div>
        </fieldset>
    </div>
</div>
@stop