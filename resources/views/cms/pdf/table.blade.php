<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
    <head>
        <title>PDF</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style type="text/css">
            table {
                background-color:#444;
                font-size: 14px;
            }
            table th {
                background-color: #444;
                color: #fff;
                font-weight: normal;
            }
            table tr {
                background-color: #fff;
            }
            .text-right {
                text-align: right;
            }
        </style>
    </head>
    <body bgcolor="#ffffff" vlink="blue" link="blue">
        <h1>{{{ $title }}}</h1>
        <div class="text-right">
            <img src="{{ res('/assets/images/logo_lateral.png') }}" alt="{{env('APP_NAME')}}" id="loginLogo" style="width: 20%;">
        </div>
        <br>
        <table cellpadding="4">
            <thead>
                <tr>
                    @foreach ($headers as $header)
                        <th>{{{ $header }}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $items)
                    <tr>
                        @foreach ($items as $item)
                            <td>{{{ $item }}}</td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
