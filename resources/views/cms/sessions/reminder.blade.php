@extends('layouts/default')

@section('content')
<div class="page-header">
    <h2 class="sub-header">@lang('sessions.reminder')</h2>
</div>

<div class="row">
{{ Form::open(array( 'action' => 'password.sendreminder', 'class' => 'form-horizontal') ) }}
    <div class="control-group{{ $errors->first('email', ' error') }}">
        {{ Form::label('email', trans('sessions.email'), array('class' => 'control-label')) }}
        <div class="controls">
    	   {{ Form::text('email') }}
    	   {{ $errors->first('email', '<span class="help-block">:message</span>') }}
    	</div>
    </div>
	<hr/>
	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn">@lang('sessions.reminder')</button>
		</div>
	</div>
{{ Form::close() }}
</div>
@stop