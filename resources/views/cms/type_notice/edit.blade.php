@extends('edit')
<!-- Page content -->
@section('edit-master')
    <div class="form-group">
        {!! Form::myTextField($item, 'name', $module, $errors, array( 'cols' => 4, 'maxlength' => 100)) !!}
        {!! Form::myTextField($item, 'label', $module, $errors, array( 'cols' => 4, 'maxlength' => 100)) !!}
        {!! Form::myTextField($item, 'description', $module, $errors, array( 'cols' => 4, 'maxlength' => 100)) !!}
    </div>
@stop
