@extends('edit')
<!-- Page content -->
@section('edit-master')
<div class="row">
    <div class="col-md-4">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang('global.user')</legend>
            <div class="form-group">
                {!! Form::myCheckBox($item, 'active', $module, $errors, array( 'cols' => 2 ) ) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextField($item, 'username', $item->getTable(), $errors, array( 'cols' => 6 )) !!}
                {!! Form::myPasswordField($item, 'password', $item->getTable(), $errors, array( 'cols' => 6 )) !!}
            </div>
            {{-- <div class="form-group">
                {!! Form::myDate($item, 'renovation_date', $item->getTable(), $errors, array( 'cols' => 12 )) !!}
            </div> --}}
        </fieldset>
    </div>
    <div class="col-md-8">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang('global.user_data')</legend>
            <div class="form-group">
                {!! Form::myTextField($item->userData, 'name', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item->userData, 'email', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 6 )) !!}
            </div>
            {{-- <div class="form-group">
                {!! Form::myTextField($item->userData, 'position', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item->userData, 'phone_number', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 3 )) !!}
                {!! Form::myTextField($item->userData, 'telephone_number', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 3 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextArea($item->userData, 'reason_registration', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 6 )) !!}
                {!! Form::myTextArea($item->userData, 'contribution', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 6 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextField($item->userData, 'linkedin_url', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item->userData, 'facebook_url', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 6 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextField($item->userData, 'instagram_url', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item->userData, 'twitter_url', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 6 )) !!}
            </div> --}}
        </fieldset>
    </div>
    {{-- <div class="col-md-12">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang('global.company')</legend>
            <div class="form-group">
                {!! Form::myTextField($item->company, 'name', isset($item->company) ? $item->company->getTable() : $item->getTable(), $errors, array( 'cols' => 4, 'id' => 'company_name', 'name' => 'company_name' )) !!}
                {!! Form::myTextField($item->company, 'number_centers', isset($item->company) ? $item->company->getTable() : $item->getTable(), $errors, array( 'cols' => 2 )) !!}
                {!! Form::myTextField($item->company, 'number_employees', isset($item->company) ? $item->company->getTable() : $item->getTable(), $errors, array( 'cols' => 2 )) !!}
                {!! Form::myTextField($item->company, 'annual_billing', isset($item->company) ? $item->company->getTable() : $item->getTable(), $errors, array( 'cols' => 2 )) !!}
                {!! Form::myTextField($item->company, 'web', isset($item->company) ? $item->company->getTable() : $item->getTable(), $errors, array( 'cols' => 2 )) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextField($item->company, 'address', isset($item->company) ? $item->company->getTable() : $item->getTable(), $errors, array( 'cols' => 4 )) !!}
                <div class="col-sm-4">
                    <label class="" for="google_maps_url">@lang('company.google_maps_url')</label>
                    <input type="text" value="{{isset($item->company) ? $item->company->google_maps_url : ''}}" id="google_maps_url" name="google_maps_url" placeholder="{{trans('company.google_maps_url')}}" class="form-control error">
                    @if(isset($item->company) && $item->company->google_maps_url)
                    <a href="{{$item->company->google_maps_url}}" target="_blank"><i class="fa fa-eye"> Ver dirección</i></a>
                    @endif
                </div>
                {!! Form::myTextField($item->userData, 'partner', isset($item->userData) ? $item->userData->getTable() : $item->getTable(), $errors, array( 'cols' => 4 )) !!}
            </div>
            <div class="form-group">
                {!! Form::mySelectField($item->company, 'sector_id', isset($item->company) ? $item->company->getTable() : $item->getTable(), $sectors, $errors, array('id' => 'sector_id', 'class' => 'form-control error', 'cols' => 6 ) ) !!}
            </div>
            <div class="form-group">
                {!! Form::myTextArea($item->company, 'services', isset($item->company) ? $item->company->getTable() : $item->getTable(), $errors, array( 'cols' => 12 )) !!}
            </div>
        </fieldset>
    </div> --}}
</div>

{{-- @php
    $files = $item->userData->files;
@endphp
@if($files->toArray())
    @include('user.files', ['files' => $files])
@endif --}}

@stop
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.pickadate').pickadate({
            format: 'dd/mm/yyyy',
            selectYears: true,
            selectMonths: true,
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Cerrar',
        });
        $(document).on('click', '.showpass', function() {
            var type = $('#password').attr('type');
            var newType;

            if(type == 'password') {
                newType = 'text';
                $(this).removeClass('fa-eye').addClass('fa-eye-slash');
            }else{
                newType = 'password';
                $(this).removeClass('fa-eye-slash').addClass('fa-eye');
            }

            $('#password').attr('type', newType);
        });
    });
</script>
@stop
