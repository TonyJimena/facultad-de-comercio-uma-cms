<?php

/**
 * Ping
 */
Route::get('/ping', function () {
    return response('', 200);
});

/**
 * Sector
 */

 // Route::get('/sectors', 'SectorController@index');
/**
 * File Type
 */
 // Route::get('/types-files', 'FileTypeController@index');
/**
 * Event State
 */
 // Route::get('/events-status', 'EventStateController@index');

// /**
//  * Tablón de anuncios
//  */
// Route::get('/notices', 'NoticeController@index');

// Route::post('/notices', 'NoticeController@store');

// Route::put('/notices/{id}', 'NoticeController@update');

// Route::delete('/notices/{id}', 'NoticeController@deleteNotice');

/**
 * Register
 */
Route::post('/users', 'UserController@store');

/**
 * Login
 */
Route::post('/login', 'AuthController@login');
/**
 * Reset password
 */
Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail');
/**
 * Directory
 */
// Route::get('/directories', 'UserController@index');
// Route::get('/directories/{id}', 'UserController@show');
/**
 * Devices
 */
// Route::post('/devices', 'DeviceController@store');
/**
 * Files User Data
 */
 // Route::get('/profile/files-user-data/{id}/preview', 'FileUserDataController@previewUserDataFile')->name('api.files_user_data.preview');

Route::group(['middleware' => ['auth:sanctum', 'actived-user']], function () {

    /**
     * Enlaces de interés
     */

    Route::get('/links', 'LinkController@index');

    Route::post('/links', 'LinkController@store');

    Route::put('/links/{id}', 'LinkController@update');

    /**
     * Noticias
     */
    Route::get('/news', 'NewController@index');

    Route::post('/news', 'NewController@store');

    Route::put('/news/{id}', 'NewController@update');

    /**
     * Tablón de anuncios
     */
    Route::get('/notices', 'NoticeController@index');

    Route::post('/notices', 'NoticeController@storeNotice');

    Route::put('/notices/{id}', 'NoticeController@update');

    Route::delete('/notices/{id}', 'NoticeController@deleteNotice');

    /**
     * User
     */
    Route::get('/user', 'UserController@showAuthenticatedUser');
    Route::put('/user', 'UserController@updateAuthenticatedUser');
    /**
     * Files User Data
     */
    // Route::get('/profile/files-user-data/{userId}', 'FileUserDataController@getUserDataFiles');
    // Route::get('/profile/files-user-data/', 'FileUserDataController@getUserDataFilesAuthenticatedUser');
    // Route::post('/profile/files-user-data', 'FileUserDataController@storeUserDataFile');
    // Route::put('/profile/files-user-data/{id}', 'FileUserDataController@updateUserDataFile');
    //Route::get('/profile/files-user-data/{id}/download', 'FileUserDataController@downloadUserDataFile');
    // Route::delete('/profile/files-user-data/{id}', 'FileUserDataController@deleteUserDataFile');
    /**
     * Devices
     */
   //  Route::put('/devices/{uuid}', 'DeviceController@update');
    /**
     * Event
     */
    // Route::get('/events', 'EventController@index');
    // Route::get('/events/{id}', 'EventController@show');
    // Route::get('/events/{id}/assist', 'EventController@assist');
    // Route::get('/events/{id}/leave', 'EventController@leave');
    // Route::get('/profile/events', 'EventController@getEventsAuthenticateUser');
    /**
     * Logout
     */
    Route::get('/logout', 'AuthController@logout');
});
